﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HaveakPro.Models;
using System.Transactions;
using PagedList;
using HaveakPro.Models.Methods;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.UI;

namespace HaveakPro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_Egitim)]
    public class EgitimIslemlerController : Controller
    {

        HaveakEntities Entity;
        public EgitimIslemlerController(HaveakEntities _Entity)
        {
            Entity = _Entity;
            Authorizing.SecilenModul = "PANEL";
        }

        #region Kategori İşlemleri
        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public async Task<ActionResult> EgitimKategori()
        {
            var data = await Entity.Bolums.Where(t => t.IsDelete == false && t.IsActive == true).ToListAsync();
            return View(data);
        }
        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        [HttpPost]
        public async Task<ActionResult> KategoriEkle(string KategoriAdi)
        {

            Bolum blm = new Bolum();
            blm.IsActive = true;
            blm.IsDelete = false;
            blm.BolumAdi = KategoriAdi;
            Entity.Bolums.Add(blm);
            await Entity.SaveChangesAsync();
            return RedirectToAction("EgitimKategori", "EgitimIslemler", new { area = "Panel" });
        }
        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public async Task<JsonResult> KategoriGuncelle(int id, string KategoriAdi)
        {
            bool durum = false;
            try
            {
                Bolum blm = await Entity.Bolums.Where(t => t.ID == id).FirstOrDefaultAsync();
                blm.BolumAdi = KategoriAdi;
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Bölüm İşlemler
        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public async Task<ActionResult> UniteModul()
        {
            var data = await Entity.UniteModuls.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            return View(data);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        [HttpPost]
        public async Task<ActionResult> ModulEkle(string Modul)
        {
            UniteModul tip = new UniteModul();
            tip.IsActive = true;
            tip.IsDelete = false;
            tip.Modul = Modul;
            Entity.UniteModuls.Add(tip);
            await Entity.SaveChangesAsync();
            return RedirectToAction("UniteModul", "EgitimIslemler", new { area = "Panel" });
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public async Task<JsonResult> ModulGuncelle(int ID, string Modul)
        {
            bool durum = false;
            try
            {
                var data = await Entity.UniteModuls.Where(r => r.ID == ID).FirstOrDefaultAsync();
                data.Modul = Modul;
                await Entity.SaveChangesAsync();
                durum = false;

            }
            catch (Exception)
            {
                durum = false;
            }
            return Json(durum, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public async Task<JsonResult> ModulSilme(int ID)
        {
            bool durum = false;
            try
            {
                var data = await Entity.UniteModuls.Where(r => r.ID == ID).FirstOrDefaultAsync();
                data.IsActive = false;
                data.IsDelete = true;
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch (Exception)
            {
                durum = false;
            }
            return Json(durum, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Ünite İşlemler

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public async Task<ActionResult> Unite()
        {
            var Detay = await Entity.Unites.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            ViewBag.Moduller = await Entity.UniteModuls.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            return View(Detay);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        [HttpPost]
        public async Task<ActionResult> UniteEkle(string UniteAdi, int Modul_Ref)
        {
            Unite tip = new Unite();
            tip.IsActive = true;
            tip.IsDelete = false;
            tip.UniteAdi = UniteAdi;
            tip.Modul_Ref = Modul_Ref;
            Entity.Unites.Add(tip);
            await Entity.SaveChangesAsync();
            return RedirectToAction("Unite", "EgitimIslemler", new { area = "PANEL" });
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public JsonResult UniteGuncelle(int ID, string UniteAdi, int Modul_Ref, int Sira)
        {
            bool durum = false;
            try
            {
                var data = Entity.Unites.Where(r => r.ID == ID).FirstOrDefault();
                data.UniteAdi = UniteAdi;
                data.Modul_Ref = Modul_Ref;
                data.Sira = Sira;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public async Task<JsonResult> UniteSilme(int ID)
        {
            bool durum = false;
            try
            {
                var data = await Entity.Unites.Where(r => r.ID == ID).FirstOrDefaultAsync();
                data.IsActive = false;
                data.IsDelete = true;
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch (Exception)
            {
                durum = false;
            }
            return Json(durum, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region KursBölümler
        public async Task<JsonResult> SaveKursModul(int Bolum_Ref, string UniteModul_Ref)
        {
            string[] idd = UniteModul_Ref.Split(',');

            bool kaydet = false;
            try
            {
                foreach (var item in idd.Where(t => t.Length > 0))
                {
                    KursModuller mdl = new KursModuller();
                    mdl.Bolum_Ref = Bolum_Ref;
                    mdl.UniteModule_Ref = int.Parse(item);
                    Entity.KursModullers.Add(mdl);
                }
                await Entity.SaveChangesAsync();
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }
            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("BolumeModulAtama", "EgitimIslemler", new { area = "Panel" });
        }
        public async Task<ActionResult> KategoriyeModulAtama()
        {
            ViewBag.Bolumler = await Entity.Bolums.Where(r => r.IsActive == true && r.IsDelete == false).ToListAsync();
            //ViewBag.Moduller = Entity.UniteModuls.Where(t => t.IsDelete == false && t.IsActive == true).ToList();
            var data = await Entity.KursModullers.Where(r => r.UniteModul.IsActive == true && r.UniteModul.IsDelete == false && r.Bolum.IsActive == true && r.Bolum.IsDelete == false).ToListAsync();
            return View(data);
        }
        public async Task<JsonResult> DeleteKursModul(int ID)
        {
            bool durum = false;
            try
            {
                KursModuller mdl = await Entity.KursModullers.Where(t => t.ID == ID).FirstOrDefaultAsync();
                Entity.KursModullers.Remove(mdl);
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> SeciliBolumeAtanmamisKurslar(int ID)
        {
            List<int> idControl = await Entity.KursModullers.Where(t => t.Bolum_Ref == ID).Select(y => y.UniteModule_Ref.Value).ToListAsync();
            var mdl = from itemz in Entity.UniteModuls where !idControl.Contains(itemz.ID) && itemz.IsActive == true && itemz.IsDelete == false select new { itemz.ID, itemz.Modul };
            return Json(mdl, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Sınıf İşlemler
        public ActionResult Sinif()
        {
            return View();
        }

        public async Task<ActionResult> _SinifListesi()
        {
            List<Sinif> siniflar = await Entity.Sinifs.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            return PartialView("_SinifListe", siniflar);
        }


        [HttpPost]
        public async Task<ActionResult> SinifEkle(string SinifAdi)
        {
            var control = await Entity.Sinifs.Where(t => t.SinifAdi == SinifAdi).CountAsync();
            if (control == 0)
            {
                Sinif tip = new Sinif();
                tip.IsActive = true;
                tip.IsDelete = false;
                tip.SinifAdi = SinifAdi;
                Entity.Sinifs.Add(tip);
                await Entity.SaveChangesAsync();
            }
            return RedirectToAction("Sinif", "EgitimIslemler", new { area = "Panel" });
            
        }

        public async Task<JsonResult> SinifGuncelle(int ID, string SinifAdi)
        {
            bool durum = false;
            try
            {
                var data = await Entity.Sinifs.Where(r => r.ID == ID).FirstOrDefaultAsync();
                if (data != null)
                {
                    data.SinifAdi = SinifAdi;
                    await Entity.SaveChangesAsync();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SinifSil(int ID)
        {
            bool durum = false;
            try
            {
                var data = await Entity.Sinifs.Where(r => r.ID == ID).FirstOrDefaultAsync();
                data.IsActive = false;
                data.IsDelete = true;
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch (Exception)
            {
                durum = false;
            }
            return Json(durum, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SinifOgrenciAtama
        public async Task<ActionResult> EgitimBolumAtama()
        {
            List<Bolum> tbolum = await Entity.Bolums.Where(t => t.IsDelete == false && t.IsActive == true && t.KursModullers.Count > 0).ToListAsync();
            List<Sinif> tsinif = await Entity.Sinifs.Where(r => r.IsActive == true && r.IsDelete == false).ToListAsync();
            List<Kullanici> Tdata = await Entity.Kullanicis.Where(e => e.IsActive == true && e.IsDelete == false).ToListAsync();            
            ViewBag.Bolum = tbolum;
            ViewBag.Sinif = tsinif;
            //var ogrenciler = Entity.Kullanicis.Where(e => e.IsActive == true && e.IsDelete == false && e.Kull_Rolleri.Select(r => r.Kull_Rol_Adi.Moduller_Ref).FirstOrDefault() != 1).Distinct().ToList();
            return View(Tdata);
        }

        public ActionResult _EgitimOgrenciler(int? page)
        {
            int pageSize = 12;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<Kullanici> ogrenciler = Entity.Kullanicis.OrderByDescending(m => m.ID).Where(r => r.IsActive == true && r.IsDelete == false).ToPagedList(pageIndex, pageSize); ;
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            return PartialView("_EgitimOgrenciListe", ogrenciler);
        }

        public async Task<JsonResult> SinifOgrenciKaydet(int[] Bolum_Ref, int[] Personel_Ref, int Sinif_Ref, DateTime BaslamaTarihi, DateTime BitisTarihi)
        {
            bool durum = false;
            try
            {
                foreach (var itemp in Personel_Ref)
                {
                    var eskiSinifKontrol = await Entity.Sinif_Ogrenci.Where(e => e.Personel_Ref == itemp).ToListAsync();
                    if (eskiSinifKontrol.Count > 0)
                    {
                        foreach (var item in eskiSinifKontrol)
                        {
                            item.BitisTarihi = DateTime.Today;
                        }
                        await Entity.SaveChangesAsync();
                    }
                    var kontrol = await Entity.Sinif_Ogrenci.Where(r => r.Sinif_Ref == Sinif_Ref && r.Personel_Ref == itemp && r.Bitti == false).FirstOrDefaultAsync();
                    if (kontrol == null)
                    {

                        //TransactionScope scp = new TransactionScope();
                        //{
                            Sinif_Ogrenci ogr = new Sinif_Ogrenci()
                            {
                                Personel_Ref = itemp,
                                Sinif_Ref = Sinif_Ref,
                                Basladi = true,
                                Bitti = false,
                                BaslamaTarihi = BaslamaTarihi,
                                BitisTarihi = BitisTarihi
                            };
                            Entity.Sinif_Ogrenci.Add(ogr);
                            await Entity.SaveChangesAsync();
                            AtananBolumler blm = null;
                            foreach (var itembb in Bolum_Ref)
                            {
                                blm = new AtananBolumler();
                                blm.Bolum_Ref = itembb;
                                blm.Sinif_Ogrenci_Ref = ogr.ID;
                                Entity.AtananBolumlers.Add(blm);
                            }
                            await Entity.SaveChangesAsync();
                            //scp.Complete();
                        //}
                        //scp.Dispose();

                    }
                    else
                    {
                        //TransactionScope scp = new TransactionScope();
                        //{

                            kontrol.BaslamaTarihi = BaslamaTarihi;
                            kontrol.BitisTarihi = BitisTarihi;                           

                            AtananBolumler blm;
                            foreach (var itembb in Bolum_Ref)
                            {
                                var bolumKontrol = await Entity.AtananBolumlers.Where(r => r.Sinif_Ogrenci_Ref == kontrol.ID && r.Bolum_Ref == itembb).FirstOrDefaultAsync();
                                if (bolumKontrol == null)
                                {
                                    blm = new AtananBolumler();
                                    blm.Bolum_Ref = itembb;
                                    blm.Sinif_Ogrenci_Ref = kontrol.ID;
                                    Entity.AtananBolumlers.Add(blm);                                   
                                }                                                              
                            }
                            await Entity.SaveChangesAsync();
                            //scp.Complete();
                        //}
                        //scp.Dispose();
                    }
                }
                durum = true;
            }
            catch(Exception ex)
            {
                durum = false;
            }

            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);


            //return RedirectToAction("EgitimBolumAtama", "EgitimIslemler", new { area = "Panel" });
        }
        public async Task<ActionResult> _SinifData(int sinifId)
        {           
            List<Sinif_Ogrenci> data = await Entity.Sinif_Ogrenci.Where(r => r.Sinif_Ref == sinifId && r.Bitti == false).ToListAsync();
            return PartialView("_SinifOgrenciler", data);
        }
        public async Task<JsonResult> SinifOgrenciGuncelle(int id, int Personel_Ref, int Sinif_Ref, string BaslamaTarihi, string BitisTarihi, int[] Bolum_Ref, int Bitti)
        {
            bool kaydet = false;
            try
            {
                var data = await Entity.Sinif_Ogrenci.Where(t => t.ID == id).FirstOrDefaultAsync();
                data.BaslamaTarihi = Convert.ToDateTime(BaslamaTarihi);
                data.BitisTarihi = Convert.ToDateTime(BitisTarihi);
                if (Bitti == 1)
                {
                    data.Bitti = true;
                }
                await Entity.SaveChangesAsync();

                var kontrol = await Entity.AtananBolumlers.Where(t => t.Sinif_Ogrenci_Ref == id).ToListAsync();
                foreach (var items in kontrol)
                {
                    Entity.AtananBolumlers.Remove(items);
                }
                if (kontrol.Count != 0)
                {
                    await Entity.SaveChangesAsync();
                }


                foreach (var iteme in Bolum_Ref)
                {
                    AtananBolumler blm = new AtananBolumler();
                    blm.Bolum_Ref = iteme;
                    blm.Sinif_Ogrenci_Ref = id;
                    Entity.AtananBolumlers.Add(blm);
                }
                await Entity.SaveChangesAsync();
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }

            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Eğitim arşiv İşlemleri

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public async Task<ActionResult> Index()
        {
            var list = await Entity.Egitim_Arsivi.Where(t => t.Unite.IsActive == true && t.Unite.IsDelete == false && t.IsActive == true && t.IsDelete == false && t.Unite.UniteModul.IsActive == true && t.Unite.UniteModul.IsDelete == false).ToListAsync();
            ViewBag.UnitList =await Entity.UniteModuls.Where(r => r.IsActive == true && r.IsDelete == false).ToListAsync();
            return View(list);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public async Task<JsonResult> GetUnitList(int bolumRef)
        {
            var unitList = await Entity.Unites.Where(r => r.IsActive == true && r.IsDelete == false && r.UniteModul.KursModullers.Select(g => g.Bolum_Ref).FirstOrDefault() == bolumRef).Select(y => new { y.ID, y.UniteAdi }).ToListAsync();
            return Json(unitList, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        [HttpPost, ValidateInput(false)]
        public async Task<ActionResult> EgitimArsivEkle(string Baslik, string Icerik, int Unite_Ref, HttpPostedFileBase Video)
        {

            Egitim_Arsivi detail = new Egitim_Arsivi();
            detail.IsActive = true;
            detail.IsDelete = false;
            detail.Baslik = Baslik;
            detail.Icerik = Icerik;
            detail.Unite_Ref = Unite_Ref;
            detail.Video = savedocument(Video, 2, "");
            Entity.Egitim_Arsivi.Add(detail);
            await Entity.SaveChangesAsync();
            return RedirectToAction("Index", "EgitimIslemler", new { area = "Panel" });
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        [ValidateInput(false)]
        public JsonResult EgitimArsivGuncelle(int id, string Baslik, string editor1, int Unite_Ref, HttpPostedFileBase Video)
        {
            bool durum = false;
            try
            {
                Egitim_Arsivi detail = Entity.Egitim_Arsivi.Where(r => r.ID == id).FirstOrDefault();
                detail.Baslik = Baslik;
                detail.Icerik = editor1;
                detail.Unite_Ref = Unite_Ref;
                if (Video != null)
                {
                    detail.Video = savedocument(Video, 2, detail.Video);
                }
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public async Task<JsonResult> EgitimArsivSilme(int ID)
        {
            bool durum = false;
            try
            {
                var data = await Entity.Egitim_Arsivi.Where(r => r.ID == ID).FirstOrDefaultAsync();
                data.IsActive = false;
                data.IsDelete = true;
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch (Exception)
            {
                durum = false;
            }
            return Json(durum, "application/data", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        public async Task<ActionResult> _EgitimData(int id)
        {
            var uniteList = await Entity.Unites.Where(e => e.UniteModul.KursModullers.Select(t => t.Bolum_Ref).FirstOrDefault() == id).Select(t => t.ID).ToListAsync();
            List<Egitim_Arsivi> dataAll = new List<Egitim_Arsivi>();
            foreach (var item in uniteList)
            {
                Egitim_Arsivi data = await Entity.Egitim_Arsivi.Where(t => t.IsActive == true && t.IsDelete == false && t.Unite_Ref == item).FirstOrDefaultAsync();
                dataAll.Add(data);
            }
            return PartialView("_EgitimArsivi", dataAll);
        }
        #endregion

        #region Global İşlemler
        protected string savedocument(HttpPostedFileBase document, int type, string gelen)
        {
            if (document != null)
            {
                FileInfo f = new FileInfo(document.FileName);
                string Fname = Guid.NewGuid() + f.Name;
                string path = "";
                if (type == 1)
                {
                    var silDosya = Server.MapPath("~/" + gelen);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    path = "DocumentFiles/" + Fname;
                }
                else
                {
                    var silDosya = Server.MapPath("~/" + gelen);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    path = "DocumentFiles/videos/" + Fname;
                }
                document.SaveAs(Server.MapPath("~/" + path));
                return path;
            }
            else
            {
                return "";
            }
        }


        #endregion
    }
}
