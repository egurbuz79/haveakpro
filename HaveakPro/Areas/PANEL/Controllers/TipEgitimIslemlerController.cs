﻿using HaveakPro.Areas.PANEL.Models;
using HaveakPro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Web.UI;

namespace HaveakPro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.SINAVLAR_OgrenciGirisi + "," + UserRoleName.EGITIM_OgrenciGirisi + "," + UserRoleName.GUVENLIK_OgrenciGirisi + "," + UserRoleName.ADMIN_BagajTehlikelimadde + "," + UserRoleName.ADMIN_Sinav + "," + UserRoleName.ADMIN_PersonelKullanici + "," + UserRoleName.ADMIN_BagajTehlikelimadde + "," + UserRoleName.ADMIN_Egitim)]
    public class TipEgitimIslemlerController : Controller
    {
        //
        // GET: /PANEL/TipEgitimIslemler/

        HaveakEntities Entity;
        public TipEgitimIslemlerController(HaveakEntities _Entity)
        {
            Entity = _Entity;          
        }

        public async Task<ActionResult> Index()
        {
            var data = await Entity.TipEgitimSinavlars.Where(r => r.IsActive == true && r.IsDelete == false).ToListAsync();
            return View(data);
        }

        public async Task<JsonResult> TipEgitimGuncelle(int id, string TipEgitimNo, string SoruSayisi, int Sure, string Aciklama, bool Yayin)
        {
            bool durum = false;
            try
            {
                var data = await Entity.TipEgitimSinavlars.Where(y => y.ID == id).FirstOrDefaultAsync();
                if (data != null)
                {
                    data.TipEgitimNo = TipEgitimNo;
                    data.SoruSayisi = int.Parse(SoruSayisi);
                    data.Sure = Sure;
                    data.Aciklama = Aciklama;
                    data.Yayinda = Yayin;
                    await Entity.SaveChangesAsync();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> TipEgitimSilme(int id)
        {
            bool durum = false;
            try
            {
                var durumKontrol = await Entity.TipEgitimSinavlars.Where(t => t.IsActive == true && t.IsDelete == false && t.ID == id).FirstOrDefaultAsync();
                if (durumKontrol != null)
                {
                    durumKontrol.IsActive = false;
                    durumKontrol.IsDelete = true;
                    await Entity.SaveChangesAsync();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> TipEgitimKaydet(string TipEgitimNo, int SoruSayisi, int Sure, string Aciklama, string Yayin)
        {
            var kontrol = await Entity.TipEgitimSinavlars.Where(y => y.TipEgitimNo == TipEgitimNo && y.IsDelete == false && y.IsActive == true).CountAsync();
            if (kontrol == 0)
            {
                TipEgitimSinavlar data = new TipEgitimSinavlar();
                data.IsActive = true;
                data.IsDelete = false;
                data.TipEgitimNo = TipEgitimNo;
                data.SoruSayisi = SoruSayisi;
                data.Sure = Sure;
                data.Aciklama = Aciklama;
                data.Yayinda = Yayin == "on" ? true : false;
                Entity.TipEgitimSinavlars.Add(data);
                await Entity.SaveChangesAsync();
            }
            return RedirectToAction("Index", "TipEgitimIslemler", new { area = "Panel" });
        }

        public async Task<ActionResult> TipEgitimSorular(int id)
        {
            //ViewData["Unite"] = Entity.Unites.Where(r => r.IsActive == true).ToList();
            //yukarıda kurs 3 ve kurs 11 ya otomatik seçilecek yada başka bi şekilde çağrılacak!

            //Not: Enumlar DB deki ID ye göre numara almalı!
            ViewBag.TipEgitimSorular = await Entity.UniteSorulars.Where(t => t.SinavTipi_Ref == (int)Enumlar.SoruTipleri.TIP_EGITIM && t.IsActive == true && t.IsDelete == false).ToListAsync();
            var data = await Entity.TipEgitimSinavlars.Where(t => t.ID == id && t.IsActive == true && t.IsDelete == false).FirstOrDefaultAsync();
            return View(data);
        }

        public async Task<JsonResult> TipEgitimSoruKaydet(int TipEgitimSinavlar_Ref, int Unite_Sorular_Ref)
        {
            bool durum = false;
            try
            {
                var kontrol = await Entity.TipEgitim_Sorular.Where(r => r.TipEgitSinavlar_Ref == TipEgitimSinavlar_Ref && r.Unite_Sorular_Ref == Unite_Sorular_Ref && r.IsActive == true && r.IsDelete == false).FirstOrDefaultAsync();
                if (kontrol == null)
                {
                    var data = new TipEgitim_Sorular();
                    data.IsActive = true;
                    data.IsDelete = false;
                    data.TipEgitSinavlar_Ref = TipEgitimSinavlar_Ref;
                    data.Unite_Sorular_Ref = Unite_Sorular_Ref;
                    Entity.TipEgitim_Sorular.Add(data);
                    await Entity.SaveChangesAsync();
                }
                else
                {
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    await Entity.SaveChangesAsync();
                }
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }


        public async Task<JsonResult> TanimliEgitimSoruGuncelleTip(int id)
        {
            bool durum = false;
            try
            {
                var data = await Entity.TipEgitim_Sorular.Where(t => t.ID == id).FirstOrDefaultAsync();
                data.IsActive = false;
                data.IsDelete = true;
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        

    }
}
