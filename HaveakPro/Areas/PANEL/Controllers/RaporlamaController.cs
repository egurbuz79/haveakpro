﻿using HaveakPro.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Data.Entity;
using System.Web.UI;
using HaveakPro.Areas.PANEL.Models;

namespace HaveakPro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_PersonelKullanici)]
    public class RaporlamaController : Controller
    {
        HaveakEntities db;
        public RaporlamaController(HaveakEntities _db)
        {
            db = _db;
        }

        public JsonResult getPersonel(int param)
        {
            List<sp_Personel_Result> dty = db.sp_Personel(param).ToList();
            return Json(dty, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getPersonelSinavlar(int param)
        {
            List<sp_PersonelSinavlar_Result> dty = db.sp_PersonelSinavlar(param).ToList();
            return Json(dty, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getPersonelTestler(int param)
        {
            List<sp_PersonelTestler_Result> dty = db.sp_PersonelTestler(param).ToList();
            return Json(dty, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getPersonelTip(int param)
        {
            List<sp_Personel_TIP_Result> dty = db.sp_Personel_TIP(param).ToList();
            return Json(dty, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getPersonelToplamSurec(int param)
        {
            List<sp_ToplamSurecler_Result> dty = db.sp_ToplamSurecler(param).ToList();
            return Json(dty, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getPersonelGenelToplam(int param)
        {
            string dty = db.sp_GenelToplamSurecler(param).FirstOrDefault() == null ? "00:00:00" : db.sp_GenelToplamSurecler(param).FirstOrDefault().Value.ToString();
            return Json(dty, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getPersonelSinifToplamSurec(int param, string sinif)
        {
            sp_SinifToplamSurecler_Result dty = db.sp_SinifToplamSurecler(param, sinif).FirstOrDefault();
            return Json(dty, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getMaddeSinavBasari(int param)
        {
            List<DuzenekOranlar> oranlar = new List<DuzenekOranlar>();

            #region Uygulama Sınav ORanlar

            //var personelSinavSorular = db.UniteSorulars.Where(e => e.STM_Ref != null && e.IsActive == true && e.IsDelete == false && e.SinavTipi_Ref == 1
            //    && e.Sinav_Sorular.Any(q => q.IsActive == true && q.IsDelete == false
            //    && q.Sinavlar.Personel_Sinavlar.Any(r => r.Sinif_Ogrenci.Personel_Ref == param && r.IsActive == true && r.IsDelete == false && r.SinavAciklama==true))).ToList(); // Uygulama Personelin gördüğü sorular

            Nullable<int> genelSinavSilah = db.PersonelToplamSinavSoruMaddeDagilim(param).Where(w => w.Isim == "Silah").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
            Nullable<int> genelSinavSilahDogru = db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(param).Where(r => r.Isim == "Silah").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
            double oran = 0;
            try
            {
                oran = ((Convert.ToDouble(genelSinavSilahDogru) / Convert.ToDouble(genelSinavSilah)) * 100);
                if (double.IsNaN(oran))
                {
                    oran = 0;
                }
                oran = Math.Round(oran, 2);
            }
            catch
            {
                oran = 0;
            }
            oranlar.Add(new DuzenekOranlar { madde = "Silah", oran = oran, sorusayisi = genelSinavSilah.Value, dogrusayisi = genelSinavSilahDogru.Value, sinavturu = "UYGULAMA Sınavlar" });

            Nullable<int> genelSinavDuzenek = db.PersonelToplamSinavSoruMaddeDagilim(param).Where(w => w.Isim == "Düzenek").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
            Nullable<int> genelSinavDuzenekDogru = db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(param).Where(r => r.Isim == "Düzenek").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
            double oranDuz = 0;
            try
            {
                oranDuz = ((Convert.ToDouble(genelSinavDuzenekDogru) / Convert.ToDouble(genelSinavDuzenek)) * 100);
                if (double.IsNaN(oranDuz))
                {
                    oranDuz = 0;
                }
                oranDuz = Math.Round(oranDuz, 2);
            }
            catch
            {
                oranDuz = 0;
            }
            oranlar.Add(new DuzenekOranlar { madde = "Düzenek", oran = oranDuz, sorusayisi = genelSinavDuzenek.Value, dogrusayisi = genelSinavDuzenekDogru.Value, sinavturu = " " });

            Nullable<int> genelSinavKesici = db.PersonelToplamSinavSoruMaddeDagilim(param).Where(w => w.Isim == "Kesici Delici").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
            Nullable<int> genelSinavKesiciDogru = db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(param).Where(r => r.Isim == "Kesici Delici").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
            double oranKes = 0;
            try
            {
                oranKes = ((Convert.ToDouble(genelSinavKesiciDogru) / Convert.ToDouble(genelSinavKesici)) * 100);
                if (double.IsNaN(oranKes))
                {
                    oranKes = 0;
                }
                oranKes = Math.Round(oranKes, 2);
            }
            catch
            {
                oranKes = 0;
            }
            oranlar.Add(new DuzenekOranlar { madde = "Kesici Delici", oran = oranKes, sorusayisi = genelSinavKesici.Value, dogrusayisi = genelSinavKesiciDogru.Value, sinavturu = " " });

            Nullable<int> genelSinavKargo = db.PersonelToplamSinavSoruMaddeDagilim(param).Where(w => w.Isim == "Düzenek (Kargo EDS)").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
            Nullable<int> genelSinavKargoDogru = db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(param).Where(r => r.Isim == "Düzenek (Kargo EDS)").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
            double oranKar = 0;
            try
            {
                oranKar = ((Convert.ToDouble(genelSinavKargoDogru) / Convert.ToDouble(genelSinavKargo)) * 100);
                if (double.IsNaN(oranKar))
                {
                    oranKar = 0;
                }
                oranKar = Math.Round(oranKar, 2);
            }
            catch
            {
                oranKar = 0;
            }
            oranlar.Add(new DuzenekOranlar { madde = "Düzenek (Kargo EDS)", oran = oranKar, sorusayisi = genelSinavKargo.Value, dogrusayisi = genelSinavKargoDogru.Value, sinavturu = " " });

            Nullable<int> genelSinavDiger = db.PersonelToplamSinavSoruMaddeDagilim(param).Where(w => w.Isim == "Diğerleri").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
            Nullable<int> genelSinavDigerDogru = db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(param).Where(r => r.Isim == "Diğerleri").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
            double oranDig = 0;
            try
            {
                oranDig = ((Convert.ToDouble(genelSinavDigerDogru) / Convert.ToDouble(genelSinavDiger)) * 100);
                if (double.IsNaN(oranDig))
                {
                    oranDig = 0;
                }
                oranDig = Math.Round(oranDig, 2);
            }
            catch
            {
                oranDig = 0;
            }
            oranlar.Add(new DuzenekOranlar { madde = "Diğerleri", oran = oranDig, sorusayisi = genelSinavDiger.Value, dogrusayisi = genelSinavDigerDogru.Value, sinavturu = " " });
            #endregion

            //#region GİS Sınavlar
            //var personelSinavSorularTip = db.UniteSorulars.Where(e => e.STM_Ref != null && e.IsActive == true && e.IsDelete == false && e.SBMaddeResimler.SBTM_Ref == 4
            //         && e.Tip_Sorular.Any(q => q.IsActive == true && q.IsDelete == false
            //         && q.TipCevaplars.Any(r => r.Personel_TIP.Personel_Ref == param))).ToList(); //  GİS Personelin gördüğü sorular

            //var genelSinavSilahTip = personelSinavSorular.Where(w => w.SBMaddeResimler.SBTM_Ref == 1).Count();
            //var genelSinavSilahDogruTip = personelSinavSorular.Where(q => q.SBMaddeResimler.SBTM_Ref == 1 && q.Sinav_Sorular.Any(u => u.SinavCevaplars.Any(t => t.DogruCevap == true))).Count();
            //double oranTip = 0;
            //try
            //{
            //    oranTip = ((Convert.ToDouble(genelSinavSilahDogruTip) / Convert.ToDouble(genelSinavSilahTip)) * 100);
            //    if (double.IsNaN(oranTip))
            //    {
            //        oranTip = 0;
            //    }
            //    oranTip = Math.Round(oranTip, 2);
            //}
            //catch
            //{
            //    oranTip = 0;
            //}
            //oranlar.Add(new DuzenekOranlar { madde = "Silah", oran = oranTip, sorusayisi = genelSinavSilahTip, dogrusayisi = genelSinavSilahDogruTip, sinavturu = "GİS Sınavlar" });

            //var genelSinavDuzenekTip = personelSinavSorular.Where(w => w.SBMaddeResimler.SBTM_Ref == 3).Count();
            //var genelSinavDuzenekDogruTip = personelSinavSorular.Where(q => q.SBMaddeResimler.SBTM_Ref == 3 && q.Sinav_Sorular.Any(u => u.SinavCevaplars.Any(t => t.DogruCevap == true))).Count();
            //double oranDuzTip = 0;
            //try
            //{
            //    oranDuzTip = ((Convert.ToDouble(genelSinavDuzenekDogruTip) / Convert.ToDouble(genelSinavDuzenekTip)) * 100);
            //    if (double.IsNaN(oranDuzTip))
            //    {
            //        oranDuzTip = 0;
            //    }
            //    oranDuzTip = Math.Round(oranDuzTip, 2);
            //}
            //catch
            //{
            //    oranDuz = 0;
            //}
            //oranlar.Add(new DuzenekOranlar { madde = "Düzenek", oran = oranDuzTip, sorusayisi = genelSinavDuzenekTip, dogrusayisi = genelSinavDuzenekDogruTip, sinavturu = " " });

            //var genelSinavKesiciTip = personelSinavSorular.Where(w => w.SBMaddeResimler.SBTM_Ref == 2).Count();
            //var genelSinavKesiciDogruTip = personelSinavSorular.Where(q => q.SBMaddeResimler.SBTM_Ref == 2 && q.Sinav_Sorular.Any(u => u.SinavCevaplars.Any(t => t.DogruCevap == true))).Count();
            //double oranKesTip = 0;
            //try
            //{
            //    oranKesTip = ((Convert.ToDouble(genelSinavKesiciDogruTip) / Convert.ToDouble(genelSinavKesiciTip)) * 100);
            //    if (double.IsNaN(oranKesTip))
            //    {
            //        oranKesTip = 0;
            //    }
            //    oranKesTip = Math.Round(oranKesTip, 2);
            //}
            //catch
            //{
            //    oranKesTip = 0;
            //}
            //oranlar.Add(new DuzenekOranlar { madde = "Kesici-Delici", oran = oranKesTip, sorusayisi = genelSinavKesiciTip, dogrusayisi = genelSinavKesiciDogruTip });

            //var genelSinavKargoTip = personelSinavSorular.Where(w => w.SBMaddeResimler.SBTM_Ref == 6).Count();
            //var genelSinavKargoDogruTip = personelSinavSorular.Where(q => q.SBMaddeResimler.SBTM_Ref == 6 && q.Sinav_Sorular.Any(u => u.SinavCevaplars.Any(t => t.DogruCevap == true))).Count();
            //double oranKarTip = 0;
            //try
            //{
            //    oranKarTip = ((Convert.ToDouble(genelSinavKargoDogruTip) / Convert.ToDouble(genelSinavKargoTip)) * 100);
            //    if (double.IsNaN(oranKarTip))
            //    {
            //        oranKarTip = 0;
            //    }
            //    oranKarTip = Math.Round(oranKarTip, 2);
            //}
            //catch
            //{
            //    oranKarTip = 0;
            //}
            //oranlar.Add(new DuzenekOranlar { madde = "Düzenek (Kargo-EDS)", oran = oranKarTip, sorusayisi = genelSinavKargoTip, dogrusayisi = genelSinavKargoDogruTip, sinavturu = " " });

            //var genelSinavDigerTip = personelSinavSorular.Where(w => w.SBMaddeResimler.SBTM_Ref == 7).Count();
            //var genelSinavDigerDogruTip = personelSinavSorular.Where(q => q.SBMaddeResimler.SBTM_Ref == 7 && q.Sinav_Sorular.Any(u => u.SinavCevaplars.Any(t => t.DogruCevap == true))).Count();
            //double oranDigTip = 0;
            //try
            //{
            //    oranDigTip = ((Convert.ToDouble(genelSinavDigerDogruTip) / Convert.ToDouble(genelSinavDigerTip)) * 100);
            //    if (double.IsNaN(oranDigTip))
            //    {
            //        oranDigTip = 0;
            //    }
            //    oranDigTip = Math.Round(oranDigTip, 2);
            //}
            //catch
            //{
            //    oranDigTip = 0;
            //}
            //oranlar.Add(new DuzenekOranlar { madde = "Diğerleri", oran = oranDigTip, sorusayisi = genelSinavDigerTip, dogrusayisi = genelSinavDigerDogruTip, sinavturu = " " });

            //#endregion

            return Json(oranlar, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Raporlar()
        {
            var siniflar = db.Sinifs.Where(e => !e.IsDelete && e.IsActive).ToList();
            return View(siniflar);
        }

        public JsonResult GenelToplamRaporlar()
        {
            var raporData = new GenelRaporlama(db).GenelDataCikti();
            return Json(raporData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getSinifPersoneller(int sinif_ref)
        {
            List<Personel_Sinavlar> data = db.Personel_Sinavlar.Where(t => t.IsActive == true && t.Sinif_Ogrenci.Sinif_Ref == sinif_ref && t.Bitti == 1).ToList();
            ViewBag.Data = db;
            return PartialView("_sinifOgrenciListe", data);
        }


        //[HttpPost]
        //[ValidateInput(false)]
        //public ActionResult ExportPdf()
        //{
        //    using (MemoryStream stream = new System.IO.MemoryStream())
        //    {
        //        string gridHtml = System.IO.File.ReadAllText(Server.MapPath("~/Shared/ReportViews/PRapor.html"), Encoding.UTF8);
        //        StringReader sr = new StringReader(gridHtml);
        //        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);

        //        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
        //        pdfDoc.Open();
        //        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
        //        pdfDoc.Close();
        //        return File(stream.ToArray(), "application/pdf", "Grid.pdf");
        //    }
        //}

    }

    public class DuzenekOranlar
    {
        public string sinavturu { get; set; }
        public string madde { get; set; }
        public double oran { get; set; }
        public int sorusayisi { get; set; }
        public int dogrusayisi { get; set; }
    }
}

