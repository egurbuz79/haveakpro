﻿using HaveakPro.Models;
using HaveakPro.Models.Methods;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Web.UI;

namespace HaveakPro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_BagajTehlikelimadde)]
    public class BagajIslemlerController : Controller
    {
        
        HaveakEntities Entity;
        public BagajIslemlerController(HaveakEntities _Entity)
        {
            Entity = _Entity;            
            Authorizing.SecilenModul = "PANEL";
        }
             
        #region ABagaj
        public ActionResult Bagajlar(int? page)
        {
            int pageSize = 24;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<Bagajlar> listb = Entity.Bagajlars.OrderBy(m => m.ID).Where(t => t.IsActive == true && t.IsDelete == false).ToPagedList(pageIndex, pageSize);
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            return View(listb);
        }

        public ActionResult BagajAra(int? page, string search)
        {
            int pageSize = 24;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<Bagajlar> listb = Entity.Bagajlars.OrderBy(m => m.ID).Where(t => t.Isim.Contains(search) && t.IsActive == true && t.IsDelete == false).ToPagedList(pageIndex, pageSize);
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            return View("Bagajlar", listb);
        }

        [HttpPost]
        public ActionResult SaveBagaj(HttpPostedFileBase[] Resim)
        {
            foreach (var item in Resim)
            {
                var bgj = new Bagajlar();
                bgj.IsActive = true;
                bgj.IsDelete = false;
                bgj.Isim = item.FileName.Split('.')[0];
                bgj.Resim = savedocumentBg(item);
                Entity.Bagajlars.Add(bgj);
                Entity.SaveChanges();
            }
            return RedirectToAction("Bagajlar", "BagajIslemler", new { area = "PANEL" });
        }

        public JsonResult DeleteBagaj(int ID)
        {
            bool durum = false;
            var bgj = Entity.Bagajlars.Where(t => t.ID == ID).FirstOrDefault();
            try
            {
                bgj.IsActive = false;
                bgj.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateBagaj(int ID, HttpPostedFileBase Resim)
        {
            bool durum = false;
            var bgj = Entity.Bagajlars.Where(t => t.ID == ID).FirstOrDefault();
            try
            {
                bgj.Isim = Resim.FileName.Split('.')[0];
                if (Resim != null)
                {
                    var silDosya = Server.MapPath("~/" + bgj.Resim);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    bgj.Resim = savedocumentBg(Resim);
                }
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateBagajAlt(int ID, HttpPostedFileBase Resim2)
        {
            bool durum = false;
            var bgj = Entity.Bagajlars.Where(t => t.ID == ID).FirstOrDefault();
            try
            {
                if (Resim2 != null)
                {
                    var silDosya = Server.MapPath("~/" + bgj.Resim2);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    bgj.Resim2 = savedocumentBg(Resim2);
                }
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        protected string savedocumentBg(HttpPostedFileBase document)
        {
            if (document != null)
            {
                FileInfo f = new FileInfo(document.FileName);
                string Fname = Guid.NewGuid() + f.Name;
                string path = "DocumentFiles/xray/" + Fname;
                document.SaveAs(Server.MapPath("~/" + path));
                return path;
            }
            else
            {
                return "";
            }

        }
        #endregion

        #region HBagaj
        public ActionResult HBagajlar(int? page)
        {
            int pageSize = 24;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<HBagajlar> list = Entity.HBagajlars.Where(t => t.IsActive == true && t.IsDelete == false).OrderByDescending(m => m.ID).ToPagedList(pageIndex, pageSize);
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            return View(list);
        }

        public ActionResult HBagajAra(int? page, string search)
        {
            int pageSize = 24;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<HBagajlar> listb = Entity.HBagajlars.OrderByDescending(m => m.ID).Where(t => t.Isim.Contains(search) && t.IsActive == true && t.IsDelete == false).ToPagedList(pageIndex, pageSize);
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            return View("HBagajlar", listb);
        }

        [HttpPost]
        public ActionResult SaveHBagaj(HttpPostedFileBase[] Resim)
        {
            foreach (var item in Resim)
            {
                var bgj = new HBagajlar();
                bgj.IsActive = true;
                bgj.IsDelete = false;
                bgj.Isim = item.FileName.Split('.')[0];
                bgj.Resim = savedocumentHiBg(item);
                Entity.HBagajlars.Add(bgj);
            }
            Entity.SaveChanges();
            return RedirectToAction("HBagajlar", "BagajIslemler", new { area = "PANEL" });
        }

        public JsonResult DeleteHBagaj(int ID)
        {
            bool durum = false;
            var bgj = Entity.HBagajlars.Where(t => t.ID == ID).FirstOrDefault();
            try
            {
                bgj.IsActive = false;
                bgj.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateHBagaj(int ID, HttpPostedFileBase Resim)
        {
            bool durum = false;
            var bgj = Entity.HBagajlars.Where(t => t.ID == ID).FirstOrDefault();
            try
            {
                bgj.Isim = Resim.FileName.Split('.')[0];
                if (Resim != null)
                {
                    var silDosya = Server.MapPath("~/" + bgj.Resim);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    bgj.Resim = savedocumentHiBg(Resim);
                }
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        protected string savedocumentHiBg(HttpPostedFileBase document)
        {
            if (document != null)
            {
                FileInfo f = new FileInfo(document.FileName);
                string Fname = Guid.NewGuid() + f.Name;
                string path = "DocumentFiles/hxray/" + Fname;
                document.SaveAs(Server.MapPath("~/" + path));
                return path;
            }
            else
            {
                return "";
            }

        }
        #endregion
        #region SBagaj
        public ActionResult SBagajlar(int? page, int? Skategori_Ref)
        {
            int pageSize = 24;
            int pageIndex = 1;
            int kategoriIndex = Skategori_Ref == null ? 0 : Skategori_Ref.Value;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            IPagedList<BagajLst> listb;
            if (kategoriIndex != 0)
            {
                listb = Entity.SBagajlars.OrderBy(m => m.ID).Where(t => t.IsActive == true && t.IsDelete == false && t.SKategori_Ref == kategoriIndex).Select(e => new BagajLst { ID = e.ID, Isim = e.Isim, Resim = e.Resim, SKategori_Ref = e.SKategori_Ref, KategoriAdi = e.SBagajKategori.Isim }).ToPagedList(pageIndex, pageSize);
            }
            else
            {
                listb = Entity.SBagajlars.OrderBy(m => m.ID).Where(t => t.IsActive == true && t.IsDelete == false).Select(e => new BagajLst { ID = e.ID, Isim = e.Isim, Resim = e.Resim, SKategori_Ref = e.SKategori_Ref, KategoriAdi = e.SBagajKategori.Isim }).ToPagedList(pageIndex, pageSize);
            }
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.SKategori = Entity.SBagajKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            ViewBag.RecordNo = recordNo;
            ViewBag.Kategori_Ref = kategoriIndex;
            return View(listb);
        }

        public ActionResult SBagajAra(int? page, string search)
        {
            int pageSize = 24;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<SBagajlar> listb = Entity.SBagajlars.OrderBy(m => m.ID).Where(t => t.Isim.Contains(search) && t.IsActive == true && t.IsDelete == false).ToPagedList(pageIndex, pageSize);
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.SKategori = Entity.SBagajKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            ViewBag.RecordNo = recordNo;
            return View("SBagajlar", listb);
        }

        [HttpPost]
        public ActionResult SaveSBagaj(HttpPostedFileBase[] Resim, int Skategori_Ref)
        {
            foreach (var item in Resim)
            {
                var bgj = new SBagajlar();
                bgj.IsActive = true;
                bgj.IsDelete = false;
                bgj.Isim = item.FileName.Split('.')[0];
                bgj.SKategori_Ref = Skategori_Ref;
                bgj.Resim = savedocumentBg(item);
                Entity.SBagajlars.Add(bgj);
            }
            Entity.SaveChanges();
            return RedirectToAction("SBagajlar", "BagajIslemler", new { area = "PANEL" });
        }

        public JsonResult DeleteSBagaj(int ID)
        {
            bool durum = false;
            var bgj = Entity.SBagajlars.Where(t => t.ID == ID).FirstOrDefault();
            try
            {
                bgj.IsActive = false;
                bgj.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateSBagaj(int ID, string Isim, HttpPostedFileBase Resim, int Skategori_Ref)
        {
            bool durum = false;
            var bgj = Entity.SBagajlars.Where(t => t.ID == ID).FirstOrDefault();
            try
            {
                bgj.Isim = Isim;
                bgj.SKategori_Ref = Skategori_Ref;
                if (Resim != null)
                {
                    var silDosya = Server.MapPath("~/" + bgj.Resim);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    bgj.Resim = savedocumentBg(Resim);
                }
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Sınav Kategori
        public ActionResult SKategori()
        {
            var data = Entity.SBagajKategoris.Where(y => y.IsActive == true && y.IsDelete == false).ToList();
            return View(data);
        }

        [HttpPost]
        public ActionResult SkategoriEkle(string Isim)
        {
            var control = Entity.SBagajKategoris.Where(t => t.Isim == Isim).Count();
            if (control == 0)
            {
                var data = new SBagajKategori();
                data.IsActive = true;
                data.IsDelete = false;
                data.Isim = Isim;
                Entity.SBagajKategoris.Add(data);
                Entity.SaveChanges();

            }
            return RedirectToAction("SKategori", "BagajIslemler", new { area = "PANEL" });
        }

        public ActionResult SKategoriGuncelle(int ID, string isim)
        {
            bool durum = false;
            try
            {
                var data = Entity.SBagajKategoris.Where(t => t.ID == ID).FirstOrDefault();
                data.Isim = isim;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SKategoriSil(int ID)
        {
            bool durum = false;
            try
            {
                var data = Entity.SBagajKategoris.Where(y => y.ID == ID).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SBTMadde Baslik
        public async Task<ActionResult> SBTMBaslik()
        {
            var data = await Entity.SBTehlikeliMKategoris.Where(y => y.IsActive == true && y.IsDelete == false).ToListAsync();
            return View(data);
        }

        [HttpPost]
        public ActionResult SBTMBaslikEkle(string Isim)
        {
            var control = Entity.SBTehlikeliMKategoris.Where(t => t.Isim == Isim).Count();
            if (control == 0)
            {
                var data = new SBTehlikeliMKategori();
                data.IsActive = true;
                data.IsDelete = false;
                data.Isim = Isim;
                Entity.SBTehlikeliMKategoris.Add(data);
                Entity.SaveChanges();

            }
            return RedirectToAction("SBTMBaslik", "BagajIslemler", new { area = "PANEL" });
        }

        public ActionResult SBTMBaslikGuncelle(int ID, string isim)
        {
            bool durum = false;
            try
            {
                var data = Entity.SBTehlikeliMKategoris.Where(t => t.ID == ID).FirstOrDefault();
                data.Isim = isim;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SBTMBaslikSil(int ID)
        {
            bool durum = false;
            try
            {
                var data = Entity.SBTehlikeliMKategoris.Where(y => y.ID == ID).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SBTMadde Resimler
        public ActionResult SBTMResimler()
        {
            //IPagedList<SBMaddeResimler> listb = null;
            ViewBag.SBTMBaslik = Entity.SBTehlikeliMKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            //ViewBag.RecordNo = 0;
            return View();
        }

        [HttpPost]
        public ActionResult SBTMResimEkle(int SBTM_Ref, HttpPostedFileBase[] Resim)
        {
            foreach (var item in Resim)
            {
                var data = new SBMaddeResimler();
                data.IsActive = true;
                data.IsDelete = false;
                data.SBTM_Ref = SBTM_Ref;
                data.Tarih = DateTime.Now.Date;
                data.Resim = savedocumentSb(item);
                Entity.SBMaddeResimlers.Add(data);
                Entity.SaveChanges();
            }
            return RedirectToAction("SBTMResimler", "BagajIslemler", new { area = "PANEL" });
        }

        public ActionResult SBTMResimGuncelle(int ID, int SBTM_Ref, HttpPostedFileBase Resim)
        {
            bool durum = false;
            try
            {
                var data = Entity.SBMaddeResimlers.Where(t => t.ID == ID).FirstOrDefault();
                data.SBTM_Ref = SBTM_Ref;
                if (Resim != null)
                {
                    var silDosya = Server.MapPath("~/" + data.Resim);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    data.Resim = savedocumentSb(Resim);
                    data.Tarih = DateTime.Now.Date;
                }
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SBTMResimSil(int ID)
        {
            bool durum = false;
            try
            {
                var data = Entity.SBMaddeResimlers.Where(y => y.ID == ID).FirstOrDefault();
                data.IsActive = false;
                data.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }


        public async Task<ActionResult> _MaddeResimGetir(List<int> IDs)
        {
            List<SBMaddeResimler> bgjlar = await Entity.SBMaddeResimlers.Where(item => IDs.Contains(item.ID)).ToListAsync();
            return PartialView("_ResimKategori", bgjlar);
        }

        public ActionResult _MaddeAltResimGetir(int Id)
        {
            List<SBMaddeResimler> bgjlar = Entity.SBMaddeResimlers.Where(i => i.IsActive == true && i.IsDelete == false && i.AltResimRef == Id).ToList();
            ViewBag.Entity = Entity;
            return PartialView("_AltKategoriResimler", bgjlar);
        }


        public async Task<JsonResult> MaddeResimGrupla(List<STMResimKategori> data)
        {
            bool durum = false;
            int seciliId = data.Where(w => w.statu == true).Select(q => q.id).FirstOrDefault();
            SBMaddeResimler anabgj = await Entity.SBMaddeResimlers.Where(t => t.ID == seciliId).FirstOrDefaultAsync();
            anabgj.AltResimRef = 0;
            Entity.SaveChanges();
            foreach (var item in data)
            {
                if (item.id != seciliId)
                {
                    SBMaddeResimler guncelleAlt = await Entity.SBMaddeResimlers.Where(w => w.ID == item.id).FirstOrDefaultAsync();
                    guncelleAlt.AltResimRef = seciliId;
                }
            }
            try
            {
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GrupSil(int Id)
        {
            bool durum = false;
            var data = await Entity.SBMaddeResimlers.Where(e => e.ID == Id).FirstOrDefaultAsync();
            if (data.AltResimRef == 0)
            {
                var altResimKkontrol =await Entity.SBMaddeResimlers.Where(q => q.AltResimRef == Id).CountAsync();
                if (altResimKkontrol != 0)
                {
                    durum = false;
                }
                else
                {
                    data.AltResimRef = null;
                    await Entity.SaveChangesAsync();
                    durum = true;
                }
            }
            else
            {
                data.AltResimRef = null;
                await Entity.SaveChangesAsync();
                durum = true;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);

        }

        public JsonResult UpdateMaddeler(int ID, HttpPostedFileBase Resim)
        {
            bool durum = false;
            var madde = Entity.SBMaddeResimlers.Where(t => t.ID == ID).FirstOrDefault();
            try
            {
                if (Resim != null)
                {
                    var silDosya = Server.MapPath("~/" + madde.Resim);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    madde.Resim = savedocumentSb(Resim);
                }
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> SBTMResimlerGetir(int? SBTM_Ref)
        {
            //int pageSize = 48;
            //int pageIndex = 1;
            //int recordNo = 0;
            //pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            List<SBMaddeResimler> Detay = await Entity.SBMaddeResimlers.OrderBy(t => t.ID).Where(t => t.IsActive == true && t.IsDelete == false && t.SBTM_Ref == SBTM_Ref && (t.AltResimRef == 0 || t.AltResimRef == null)).ToListAsync();
            //if (pageIndex != 1)
            //{
            //    recordNo = ((pageIndex - 1) * pageSize);
            //}
            ViewBag.GelenKatID = SBTM_Ref;
            //ViewBag.RecordNo = recordNo;
            ViewBag.Entity = Entity;
            ViewBag.SBTMBaslik =await Entity.SBTehlikeliMKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            //return PartialView("_SBTMResimlerData", Detay);
            return View("SBTMResimler", Detay);
        }



        protected string savedocumentSb(HttpPostedFileBase document)
        {
            if (document != null)
            {
                FileInfo f = new FileInfo(document.FileName);
                string Fname = Guid.NewGuid() + f.Name;
                string path = "DocumentFiles/xray/" + Fname;
                document.SaveAs(Server.MapPath("~/" + path));
                return path;
            }
            else
            {
                return "";
            }

        }
        #endregion

      
    }
    public class BagajLst
    {
        public int ID { get; set; }
        public string Isim { get; set; }
        public string Resim { get; set; }
        public int SKategori_Ref { get; set; }
        public string KategoriAdi { get; set; }
    }

    public class STMResimKategori
    {
        public int id { get; set; }
        public bool statu { get; set; }
    }
}
