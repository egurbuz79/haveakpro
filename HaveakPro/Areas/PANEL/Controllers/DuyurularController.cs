﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HaveakPro.Models;
using HaveakPro.Models.Methods;
using System.Threading.Tasks;
using System.Data.Entity;
using HaveakPro.Models.Interface;
using System.Web.UI;

namespace HaveakPro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_BagajTehlikelimadde + "," + UserRoleName.ADMIN_Egitim + "," + UserRoleName.ADMIN_PersonelKullanici + "," + UserRoleName.ADMIN_Sinav)]
    public class DuyurularController : Controller
    {
       

        HaveakEntities Entity;
        I_UserModel user;
        public DuyurularController(HaveakEntities _Entity, I_UserModel _user)
        {
            Entity = _Entity;
            user = _user;
            Authorizing.SecilenModul = "PANEL";
        }

        public async Task<ActionResult> Index()
        {
            List<Duyurular> data = await Entity.Duyurulars.Where(t => t.IsDelete == false).ToListAsync();
            ViewBag.Id = user.getAutorizeUser().Id;
            return View(data);
        }
        [HttpPost]
        public async Task<ActionResult> DuyuruYayinla(string Baslik, string Duyuru, int Tipi, int Oncelik)
        {
            Duyurular detay = new Duyurular();
            detay.Baslik = Baslik;
            detay.Duyuru = Duyuru;
            detay.IlanTarihi = DateTime.Now;
            detay.DuyuruTarihi = DateTime.Now;
            detay.Tipi = Tipi;
            detay.Oncelik = Oncelik;
            detay.IsActive = true;
            detay.IsDelete = false;           
            Entity.Duyurulars.Add(detay);
            await Entity.SaveChangesAsync();
            return RedirectToAction("Index", "Duyurular", new { area = "Panel" });
        }

        public async Task<JsonResult> DuyuruGuncelle(int id, int tip, int Oncelik, string Baslik, string Duyuru, bool Active)
        {
            bool result = false;
            try
            {
                var data = Entity.Duyurulars.Where(r => r.ID == id).FirstOrDefault();
                data.IsActive = Active;
                data.Baslik = Baslik;
                data.Duyuru = Duyuru;
                data.DuyuruTarihi = DateTime.Now.Date;
                data.Tipi = tip;
                data.Oncelik = Oncelik;
                await Entity.SaveChangesAsync();
                result = true;
            }
            catch
            {
                result = false;
            }
            return Json(result, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DuyuruSil(int id)
        {
            bool result = false;
            try
            {
                var data = await Entity.Duyurulars.Where(r => r.ID == id).FirstOrDefaultAsync();
                data.IsDelete = true;
                await Entity.SaveChangesAsync();
                result = true;
            }
            catch
            {
                result = false;
            }
            return Json(result, "application/json", JsonRequestBehavior.AllowGet);
        }

       
    }
}
