﻿using HaveakPro.Models;
using HaveakPro.Models.Interface;
using HaveakPro.Models.Methods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Data.Entity;
using System.Web.UI;

namespace HaveakPro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.SINAVLAR_OgrenciGirisi + "," + UserRoleName.EGITIM_OgrenciGirisi + "," + UserRoleName.GUVENLIK_OgrenciGirisi + "," + UserRoleName.ADMIN_BagajTehlikelimadde + "," + UserRoleName.ADMIN_Sinav + "," + UserRoleName.ADMIN_PersonelKullanici + "," + UserRoleName.ADMIN_BagajTehlikelimadde + "," + UserRoleName.ADMIN_Egitim)]
    public class HomeController : Controller
    {
        HaveakEntities Entity;
        I_UserModel user;
        public HomeController(HaveakEntities _Entity, I_UserModel _user)
        {
            Entity = _Entity;
            user = _user;
        }      

        public HomeController()
        {
            Authorizing.SecilenModul = "PANEL";

        }

        private DateTime bugun = DateTime.Now;
        private int gun = DateTime.Now.Day;
        private int ay = DateTime.Now.Month;
        private int yil = DateTime.Now.Year;
       

        [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_BagajTehlikelimadde + "," + UserRoleName.ADMIN_Egitim + "," + UserRoleName.ADMIN_PersonelKullanici + "," + UserRoleName.ADMIN_Sinav)]
        public async Task<ActionResult> Index()
        {
            int userRef = user.getAutorizeUser().User_Ref;
            ViewBag.Sinavlar = await Entity.Personel_Sinavlar.Where(t => t.SinavAciklama == true && t.IsDelete == false).Select(r => r.Durum).ToListAsync();
            var sorular = await Entity.SinavTipis.ToListAsync();
            var listSoru = sorular.Where(t => t.UniteSorulars.Where(r => r.IsActive == true && r.IsDelete == false).Count() > 0).Select(q => q.UniteSorulars).ToList();

            List<SinavDurum> datac = new List<SinavDurum>();
            foreach (var items in listSoru)
            {
                var datax = items.GroupBy(t => new { t.SinavTipi.Baslik, t.SinavTipi_Ref }).Select(q => new SinavDurum { Baslik = q.Key.Baslik, sinavRef = q.Count() }).FirstOrDefault();
                datac.Add(datax);
            }
            ViewBag.Sorular = datac;
            Loglama log = await Entity.Loglamas.OrderByDescending(w=>w.ID).Where(w =>
                 w.Kullanici_Ref == userRef &&
                 w.GirisTarihi.Day == gun &&
                 w.GirisTarihi.Month == ay &&
                 w.GirisTarihi.Year == yil).FirstOrDefaultAsync();

            Loglama logdetay = new Loglama();
            if (log == null || (log != null && log.CikisSaati != null))
            {
                logdetay.IsActive = true;
                logdetay.IsDelete = false;
                logdetay.Kullanici_Ref = userRef;
                logdetay.GirisTarihi = bugun;
                logdetay.YapilanIs = "Admin İşlem";
                Entity.Loglamas.Add(logdetay);
                await Entity.SaveChangesAsync();
            }

            ViewBag.Astro = await Entity.Bagajlars.Where(t => t.IsActive == true && t.IsDelete == false).Select(r => r.ID).CountAsync();
            ViewBag.Hieman = await Entity.HBagajlars.Where(t => t.IsActive == true && t.IsDelete == false).Select(r => r.ID).CountAsync();
            ViewBag.SBagaj = await Entity.SBagajlars.Where(t => t.IsActive == true && t.IsDelete == false).Select(r => r.ID).CountAsync();
            ViewBag.Madde = await Entity.Maddelers.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            ViewBag.TMaddeler = await Entity.SBMaddeResimlers.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();

            ViewBag.TMKategori = await  Entity.TehlikeliMaddeKategoris.Where(e => e.IsActive == true && e.IsDelete == false).ToListAsync();
            ViewBag.STMKategori = await Entity.SBTehlikeliMKategoris.Where(e => e.IsActive == true && e.IsDelete == false).ToListAsync();

            ViewBag.TumKullanici = await Entity.Kullanicis.Where(t => t.IsActive == true && t.IsDelete == false).CountAsync();

            List<Kullanici> uyeler = await Entity.Kullanicis.Where(r => r.Sistemde == true && r.Songiristarihi.Value == bugun.Date).ToListAsync();
            ViewBag.AktifUyeler = uyeler;   
            return View();
        }

        #region Resim Tip İşlemler
        //[Authorize(Roles = UserRoleName.PANEL_AdminGiris)]
        //public ActionResult ResimTip()
        //{
        //    var Detay = Entity.ResimTipis.ToList();
        //    return View(Detay);
        //}
        //[HttpPost]
        //public ActionResult SaveResimTip(string ResimTip)
        //{
        //    ResimTipi tip = new ResimTipi();
        //    tip.IsActive = true;
        //    tip.IsDelete = false;
        //    tip.TipAdi = ResimTip;
        //    Entity.ResimTipis.Add(tip);
        //    Entity.SaveChanges();
        //    return RedirectToAction("ResimTip", "Home", new { area = "Panel" });
        //}

        //public ActionResult UpdateResimTip(int ID, string ResimTipUp, bool IsActive, bool IsDelete)
        //{
        //    string result = "";
        //    try
        //    {
        //        var data = Entity.ResimTipis.Where(r => r.ID == ID).FirstOrDefault();
        //        data.TipAdi = ResimTipUp;
        //        data.IsActive = IsActive;
        //        data.IsDelete = IsDelete;
        //        Entity.SaveChanges();
        //        result = "[{\"durum\":true}]";
        //    }
        //    catch (Exception)
        //    {
        //        result = "[{\"durum\":false}]";
        //    }
        //    ContentResult cr = new ContentResult();
        //    cr.ContentEncoding = System.Text.Encoding.UTF8;
        //    cr.ContentType = "application/json";
        //    cr.Content = result;
        //    return cr;
        //}
        #endregion

        public async Task<JsonResult> SenMassageUser(int Personel_Ref, string Icerik, int Alici_Ref)
        {
            int kullaniciID = await Entity.Kullanicis.Where(y => y.Personel_Ref == Personel_Ref).Select(r => r.ID).FirstOrDefaultAsync();
            var control = await Entity.Kull_Rolleri.Where(t => t.User_Ref == kullaniciID).ToListAsync();
            bool result = false;
            try
            {
                var data = new Mesajlar();
                data.IsActive = true;
                data.IsDelete = false;
                data.Alici_Ref = Alici_Ref;
                data.Icerik = Icerik;
                data.Tarih = DateTime.Now;
                Entity.Mesajlars.Add(data);
                await Entity.SaveChangesAsync();
                result = true;
            }
            catch
            {
                result = false;
            }

            return Json(result, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        
    }
    public class SinavDurum
    {
        public int sinavRef { get; set; }
        public string Baslik { get; set; }
    }

    public class TMKategoriler
    {
        public string Kategori { get; set; }
        public int Adet { get; set; }
    }
}
