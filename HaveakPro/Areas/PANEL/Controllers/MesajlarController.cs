﻿using HaveakPro.Models;
using HaveakPro.Models.Methods;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Data.Entity;
using System.Web.UI;

namespace HaveakPro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.SINAVLAR_OgrenciGirisi + "," + UserRoleName.EGITIM_OgrenciGirisi + "," + UserRoleName.GUVENLIK_OgrenciGirisi + "," + UserRoleName.ADMIN_BagajTehlikelimadde + "," + UserRoleName.ADMIN_Sinav + "," + UserRoleName.ADMIN_PersonelKullanici + "," + UserRoleName.ADMIN_BagajTehlikelimadde + "," + UserRoleName.ADMIN_Egitim)]
    public class MesajlarController : Controller
    {
        HaveakEntities Entity;
        public MesajlarController(HaveakEntities _Entity)
        {
            Entity = _Entity;
            Authorizing.SecilenModul = "PANEL";
        }

        public async Task<ActionResult> Index()
        {
            var list = await Entity.Mesajlars.ToListAsync();
            ViewBag.Personel = await Entity.Personels.Where(r => r.IsActive == true && r.IsDelete == false).ToListAsync();
            return View(list);
        }

        [HttpPost]
        public async Task<ActionResult> SendMessage(int Personel_Ref, string Icerik)
        {
            var mesaj = new Mesajlar();
            mesaj.IsActive = true;
            mesaj.IsDelete = false;
            //mesaj.Personel_Ref = Personel_Ref;
            mesaj.Icerik = Icerik;           
            mesaj.Tarih = DateTime.Now;
            Entity.Mesajlars.Add(mesaj);
            await Entity.SaveChangesAsync();
            return RedirectToAction("Index", "Mesajlar", new { area="Panel" });
        }

      

    }
}
