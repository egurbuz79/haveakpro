﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HaveakPro.Models;
//using HaveakPro.Areas.PANEL.Models;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using HaveakPro.Areas.PANEL.Models;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Threading;
using PagedList;
using HaveakPro.Models.Methods;
using System.Web.UI;
using System.Data.Entity.Infrastructure;
using HaveakPro.Models.Interface;

namespace HaveakPro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize(Roles = UserRoleName.ADMIN_Sinav + "," + UserRoleName.PANEL_AdminGiris)]
    public class SinavIslemlerController : Controller
    {
        HaveakEntities Entity;
        I_UserModel user;
        public SinavIslemlerController(HaveakEntities _Entity, I_UserModel _user)
        {
            Entity = _Entity; 
            user = _user;
            Authorizing.SecilenModul = "PANEL";
        }

        public async Task<ActionResult> YeniSoruTanimlama()
        {
            ViewBag.Uniteler = await Entity.Unites.Where(r => r.IsActive == true && r.IsDelete == false).ToListAsync();
            ViewBag.UniteModuller = await Entity.UniteModuls.Where(r => r.IsActive == true && r.IsDelete == false).ToListAsync();
            ViewBag.STMKategori = await Entity.SBTehlikeliMKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            ViewBag.SBagajKategori = await Entity.SBagajKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            ViewBag.Sorukategori = await Entity.SoruKategoris.ToListAsync();
            ViewBag.SinavTipi = await Entity.SinavTipis.ToListAsync();
            return View();
        }

        public async Task<ActionResult> _KategoriBagajlar(int kategori_Ref)
        {
            var data = await Entity.SBagajlars.Where(t => t.SKategori_Ref == kategori_Ref && t.IsActive == true && t.IsDelete == false).ToListAsync();
            return PartialView("_KategoriSBagajlar", data);
        }

        public ActionResult _KategoriMaddeler(int madde_Ref)
        {
            var data = Entity.SBMaddeResimlers.Where(t => t.SBTM_Ref == madde_Ref && t.IsActive == true && t.IsDelete == false).ToList();
            ViewBag.Entity = Entity;
            return PartialView("_KategoriSMaddeler", data);
        }


        public async Task<JsonResult> SoruMaddeKontrol(int bagajRef, int maddeRef)
        {
            var kontrol = await Entity.UniteSorulars.Where(t => t.SBagajlar_Ref == bagajRef && t.STM_Ref == maddeRef).CountAsync();
            return Json(kontrol, JsonRequestBehavior.AllowGet);

        }

        public async Task<ActionResult> SinavCevaplar(int id)
        {
            var pcevap = await Entity.SinavCevaplars.Where(y => y.Personel_Sinavlar_Ref == id).ToListAsync();
            return View(pcevap);
        }

        public async Task<ActionResult> SoruGuncelle(int id)
        {
            ViewBag.UniteModuller = await Entity.UniteModuls.Where(r => r.IsActive == true && r.IsDelete == false).ToListAsync();
            ViewBag.STMKategori = await Entity.SBTehlikeliMKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            ViewBag.SBagajKategori = await Entity.SBagajKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            ViewBag.SoruKategori = await Entity.SoruKategoris.ToListAsync();
            //ViewBag.SinavSorular = Entity.BaslikMaddes.Where(t => t.KategoriMadde_Ref == 1007).ToList();
            ViewBag.SinavTipi = await Entity.SinavTipis.ToListAsync();
            var data = await Entity.UniteSorulars.Where(g => g.ID == id && g.IsActive == true).FirstOrDefaultAsync();
            return View(data);
        }

        public string ServerTime()
        {
            return DateTime.Now.ToString();
        }

        [HttpPost]
        public async Task<ActionResult> SoruKaydet(int SinavTipi_Ref, int SoruKategori_Ref, int? Unite_Ref, string Soru, List<string> SoruSikki, List<string> CevapIcerik, List<bool> DogruSecenek,
                                     int? SBagajlarData, int? STMData, string XKordinat, string YKordinat, List<string> AltSoruSikki, List<string> AltCevapIcerik,
                                     List<bool> AltDogruSecenek, decimal? TMOlcek, decimal? TMVektor, decimal? TMSaydamlik)
        {

            UniteSorular soruDetay = new UniteSorular();
            soruDetay.IsActive = true;
            soruDetay.IsDelete = false;
            if (SBagajlarData != null)
            {
                soruDetay.SBagajlar_Ref = SBagajlarData.Value;
            }
            if (STMData != null)
            {
                soruDetay.STM_Ref = STMData;
            }
            soruDetay.Soru = Soru;
            soruDetay.Unite_Ref = Unite_Ref.Value;
            soruDetay.SinavTipi_Ref = SinavTipi_Ref;
            if (XKordinat != "" && YKordinat != "")
            {
                soruDetay.XKordinat = Convert.ToDecimal(XKordinat.Replace('.', ','));
                soruDetay.YKordinat = Convert.ToDecimal(YKordinat.Replace('.', ','));
                soruDetay.TMOlcek = TMOlcek;
                soruDetay.TMVektor = TMVektor;
                soruDetay.TMSaydamlik = TMSaydamlik;
            }
            soruDetay.SoruKategori_Ref = SoruKategori_Ref;
            Entity.UniteSorulars.Add(soruDetay);
            await Entity.SaveChangesAsync();

            if (SoruSikki != null)
            {
                var tSoruSikki = SoruSikki.Select((z, Index) => new { Name = z, Ix = Index });
                var tCevapIcerik = CevapIcerik.Select((z, Index) => new { Name = z, Ix = Index });
                var tDogruSecenek = DogruSecenek.Select((z, Index) => new { Name = z, Ix = Index });
                var Sorular = from SoruSikkix in tSoruSikki
                              join CevapIcerikx in tCevapIcerik on SoruSikkix.Ix equals CevapIcerikx.Ix
                              join DogruSecenekx in tDogruSecenek on SoruSikkix.Ix equals DogruSecenekx.Ix
                              select new SoruSiklari
                              {
                                  IsActive = true,
                                  IsDelete = false,
                                  CevapIcerik = CevapIcerikx.Name,
                                  UniteSorular_Ref = soruDetay.ID,
                                  DogruSecenek = DogruSecenekx.Name,
                                  SoruSikki = SoruSikkix.Name
                              };
                foreach (var itemSoru in Sorular)
                {
                    Entity.SoruSiklaris.Add(itemSoru);
                }
                await Entity.SaveChangesAsync();

                if (AltSoruSikki != null)
                {
                    var tAltSoruSikki = AltSoruSikki.Select((z, Index) => new { Name = z, Ix = Index });
                    var tAltCevapIcerik = AltCevapIcerik.Select((z, Index) => new { Name = z, Ix = Index });
                    var tAltDogruSecenek = AltDogruSecenek.Select((z, Index) => new { Name = z, Ix = Index });
                    var AltSorular = from AltSoruSikkix in tAltSoruSikki
                                     join AltCevapIcerikx in tAltCevapIcerik on AltSoruSikkix.Ix equals AltCevapIcerikx.Ix
                                     join AltDogruSecenekx in tAltDogruSecenek on AltSoruSikkix.Ix equals AltDogruSecenekx.Ix
                                     select new AltSiklar
                                     {
                                         IsActive = true,
                                         IsDelete = false,
                                         AltCevapIcerik = AltCevapIcerikx.Name,
                                         UniteSorular_Ref = soruDetay.ID,
                                         AltDogruSecenek = AltDogruSecenekx.Name,
                                         AltSoruSikki = AltSoruSikkix.Name
                                     };
                    foreach (var itemAltSoru in AltSorular)
                    {
                        Entity.AltSiklars.Add(itemAltSoru);
                    }
                    await Entity.SaveChangesAsync();
                }
            }

            return RedirectToAction("YeniSoruTanimlama", "SinavIslemler", new { area = "Panel" });
        }

        //public ActionResult SoruGuncelle(int ID)
        //{
        //    var Data = Entity.UniteSorulars.Where(t => t.ID == ID).First();
        //    Data.IsActive = false;
        //    Data.IsDelete = true;
        //    Entity.SaveChanges();
        //    return RedirectToAction("TanimliSorular", "SinavIslemler", new { area = "Panel" });
        //    //return View(Data);
        //}

        public async Task<JsonResult> SoruAktifPasif(int ID, bool aktif)
        {
            var data = await Entity.UniteSorulars.Where(t => t.ID == ID).FirstOrDefaultAsync();
            data.IsActive = aktif;
            await Entity.SaveChangesAsync();
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult SoruGuncelleGenelUyg(int ID, int Unite_Ref, string Soru, int rm_Id, int? XBaslangic, int? XBitis, int? YBaslangic, int? YBitis)
        //{
        //    string kaydet = "";
        //    UniteSorular soruDetay = Entity.UniteSorulars.Where(r => r.ID == ID).First();        
        //    soruDetay.BaslikMadde_Ref = rm_Id;            
        //    soruDetay.Soru = Soru;
        //    soruDetay.Unite_Ref = Unite_Ref;
        //    soruDetay.XBaslangic = XBaslangic;
        //    soruDetay.XBitis = XBitis;
        //    soruDetay.YBaslangic = YBaslangic;
        //    soruDetay.YBitis = YBitis;         
        //    try
        //    {
        //        Entity.SaveChanges();  
        //        kaydet = "Güncelleme Başarılı";
        //    }
        //    catch
        //    {
        //        kaydet = "Güncelem Sırasında Bir hata oluştu tekrar deneyiniz! (Genel)";
        //    }
        //    Entity.SaveChanges();

        //    return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        //}

        public async Task<JsonResult> SoruGuncelleGenel(int ID, int SinavTipi_Ref, int SoruKategori_Ref, int? Unite_Ref, string Soru, int? SBagajlar_Ref, int? STM_Ref, string XKordinat, string YKordinat, decimal? TMOlcek, decimal? TMVektor, decimal? TMSaydamlik)
        {
            bool kaydet = false;
            UniteSorular soruDetay = await Entity.UniteSorulars.Where(r => r.ID == ID).FirstOrDefaultAsync();
            soruDetay.Soru = Soru;
            soruDetay.Unite_Ref = Unite_Ref == null ? 0 : Unite_Ref.Value;
            soruDetay.SoruKategori_Ref = SoruKategori_Ref;
            if (SinavTipi_Ref == (int)HaveakPro.Areas.PANEL.Models.Enumlar.SoruTipleri.UYGULAMA || SinavTipi_Ref == (int)HaveakPro.Areas.PANEL.Models.Enumlar.SoruTipleri.TIP || SinavTipi_Ref == (int)HaveakPro.Areas.PANEL.Models.Enumlar.SoruTipleri.TIP_EGITIM)
            {
                soruDetay.SBagajlar_Ref = SBagajlar_Ref;
                if (STM_Ref != 0)
                {
                    soruDetay.STM_Ref = STM_Ref;
                }
                soruDetay.XKordinat = Convert.ToDecimal(XKordinat.Replace('.', ','));
                soruDetay.YKordinat = Convert.ToDecimal(YKordinat.Replace('.', ','));
                soruDetay.TMOlcek = TMOlcek;
                soruDetay.TMVektor = TMVektor;
                soruDetay.TMSaydamlik = TMSaydamlik;
            }
            try
            {
                Entity.SaveChanges();
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }
            await Entity.SaveChangesAsync();
            return Json(kaydet, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SoruGuncelleSiklar(int ID, string SoruSikki, string CevapIcerik, bool DogruSecenek)
        {
            bool kaydet = false;
            SoruSiklari detay = await Entity.SoruSiklaris.Where(t => t.ID == ID).FirstOrDefaultAsync();
            detay.CevapIcerik = CevapIcerik;
            detay.DogruSecenek = DogruSecenek;
            detay.SoruSikki = SoruSikki;
            await Entity.SaveChangesAsync();
            try
            {
                List<int> idx = await Entity.SoruSiklaris.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref && t.IsActive == true).Select(y => y.ID).ToListAsync();
                foreach (var item in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    SoruSiklari detay2 = await Entity.SoruSiklaris.Where(t => t.ID == item.value).FirstOrDefaultAsync();
                    detay2.DogruSecenek = false;
                    if (detay2.ID == detay.ID)
                    {
                        detay2.DogruSecenek = DogruSecenek;
                    }
                    await Entity.SaveChangesAsync();
                }
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }
            return Json(kaydet, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SoruEkleSiklar(int UniteSorular_Ref, string SoruSikki, string CevapIcerik, bool DogruSecenek)
        {
            bool kaydet = false;
            SoruSiklari detay = new SoruSiklari();
            detay.IsActive = true;
            detay.IsDelete = false;
            detay.UniteSorular_Ref = UniteSorular_Ref;
            detay.CevapIcerik = CevapIcerik;
            detay.DogruSecenek = DogruSecenek;
            detay.SoruSikki = SoruSikki;
            try
            {
                Entity.SoruSiklaris.Add(detay);
                await Entity.SaveChangesAsync();
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }
            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SoruSiklarSil(int ID)
        {
            string kaydet = "";
            SoruSiklari detay = Entity.SoruSiklaris.Where(t => t.ID == ID).First();
            detay.IsActive = false;
            detay.IsDelete = true;
            detay.DogruSecenek = false;
            try
            {
                await Entity.SaveChangesAsync();
                string[] siklar = { "a)", "b)", "c)", "d)", "e)", "f)", "g)" };
                List<int> idx = await Entity.SoruSiklaris.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref && t.IsActive == true && t.IsDelete == false).Select(y => y.ID).ToListAsync();
                foreach (var item in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    SoruSiklari detay2 = await Entity.SoruSiklaris.Where(t => t.ID == item.value && t.IsActive == true && t.IsDelete == false).FirstOrDefaultAsync();
                    if (detay2 != null)
                    {
                        detay2.DogruSecenek = false;
                        if (item.Index == 0)
                        {
                            detay2.DogruSecenek = true;
                        }
                        await Entity.SaveChangesAsync();
                    }

                }
                foreach (var itemCevap in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    SoruSiklari detay2Cevap = await Entity.SoruSiklaris.Where(t => t.ID == itemCevap.value && t.IsActive == true && t.IsDelete == false).FirstOrDefaultAsync();
                    if (detay2Cevap != null)
                    {
                        detay2Cevap.SoruSikki = siklar[itemCevap.Index];
                        await Entity.SaveChangesAsync();
                    }
                }

                kaydet = "Soru Sikki Başarı ile güncellendi";
            }
            catch
            {
                kaydet = "Güncelleme Sırasında bir hata oluştu  tekrar deneyiniz! (Soru Şıkkı)";
            }
            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SoruEkleAltSiklar(int UniteSorular_Ref, string AltSoruSikki, string AltCevapIcerik, bool AltDogruSecenek)
        {
            bool kaydet = false;
            AltSiklar detay = new AltSiklar();
            detay.IsActive = true;
            detay.IsDelete = false;
            detay.UniteSorular_Ref = UniteSorular_Ref;
            detay.AltCevapIcerik = AltCevapIcerik;
            detay.AltDogruSecenek = AltDogruSecenek;
            detay.AltSoruSikki = AltSoruSikki;
            try
            {
                Entity.AltSiklars.Add(detay);
                await Entity.SaveChangesAsync();
                kaydet = true;
                //kaydet += detay.ID;
                //kaydet += ",Alt Soru Sikki Başarı ile Eklendi";
            }
            catch
            {
                kaydet = false;
            }


            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SoruGuncelleAltSiklar(int ID, string AltSoruSikki, string AltCevapIcerik, bool AltDogruSecenek)
        {
            string kaydet = "";
            AltSiklar detay = Entity.AltSiklars.Where(t => t.ID == ID).First();
            detay.AltCevapIcerik = AltCevapIcerik;
            detay.AltDogruSecenek = AltDogruSecenek;
            detay.AltSoruSikki = AltSoruSikki;
            try
            {
                await Entity.SaveChangesAsync();

                List<int> idx = await Entity.AltSiklars.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref).Select(y => y.ID).ToListAsync();
                foreach (var item in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    AltSiklar detay2 = await Entity.AltSiklars.Where(t => t.ID == item.value).FirstOrDefaultAsync();
                    detay2.AltDogruSecenek = false;
                    if (detay2.ID == detay.ID)
                    {
                        detay2.AltDogruSecenek = AltDogruSecenek;
                    }
                    Entity.SaveChanges();
                }
                kaydet = "Soru Sikki Başarı ile güncellendi";
            }
            catch
            {
                kaydet = "Güncelleme Sırasında bir hata oluştu  tekrar deneyiniz! (Soru Şıkkı)";
            }

            return Json(kaydet, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> AltSoruSikkiGuncelle(int ID)
        {
            bool kaydet = false;
            AltSiklar detay = await Entity.AltSiklars.Where(t => t.ID == ID && t.IsActive == true && t.IsDelete == false).FirstOrDefaultAsync();
            try
            {
                var detay2 = await Entity.AltSiklars.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref && t.IsActive == true && t.IsDelete == false).ToListAsync();
                foreach (var item in detay2)
                {
                    item.AltDogruSecenek = false;
                    await Entity.SaveChangesAsync();
                }
                detay.AltDogruSecenek = true;
                await Entity.SaveChangesAsync();
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }
            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SoruSikkiGuncelle(int ID)
        {
            bool kaydet = false;
            SoruSiklari detay = await Entity.SoruSiklaris.Where(t => t.ID == ID).FirstOrDefaultAsync();
            try
            {
                var detay2 = await Entity.SoruSiklaris.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref && t.IsActive == true && t.IsDelete == false).ToListAsync();
                foreach (var item in detay2)
                {
                    item.DogruSecenek = false;
                    await Entity.SaveChangesAsync();
                }
                detay.DogruSecenek = true;
                await Entity.SaveChangesAsync();
                kaydet = true;
            }
            catch
            {
                kaydet = false;
            }
            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SinavSilme(int sinavID)
        {
            bool durum = false;
            try
            {
                var durumKontrol = await Entity.Sinavlars.Where(t => t.IsActive == true && t.IsDelete == false && t.ID == sinavID && t.Personel_Sinavlar.Count == 0).FirstOrDefaultAsync();
                if (durumKontrol != null)
                {
                    durumKontrol.IsActive = false;
                    durumKontrol.IsDelete = true;
                    await Entity.SaveChangesAsync();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SoruAltSiklarSil(int ID)
        {
            bool kaydet = false;
            AltSiklar detay = await Entity.AltSiklars.Where(t => t.ID == ID).FirstOrDefaultAsync();
            detay.IsActive = false;
            detay.IsDelete = true;
            try
            {
                await Entity.SaveChangesAsync();
                string[] siklar = { "a)", "b)", "c)", "d)", "e)", "f)", "g)" };
                List<int> idx = await Entity.AltSiklars.Where(t => t.UniteSorular_Ref == detay.UniteSorular_Ref && t.IsActive == true && t.IsDelete == false).Select(y => y.ID).ToListAsync();
                foreach (var item in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    AltSiklar detay2 = await Entity.AltSiklars.Where(t => t.ID == item.value).FirstOrDefaultAsync();
                    detay2.AltDogruSecenek = false;
                    if (item.Index == 0)
                    {
                        detay2.AltDogruSecenek = false;
                        await Entity.SaveChangesAsync();
                    }
                    detay2.AltSoruSikki = siklar[item.Index];
                    await Entity.SaveChangesAsync();
                }

                foreach (var itemCevap in idx.Select((r, v) => new { value = r, Index = v }))
                {
                    AltSiklar detay2Cevap = await Entity.AltSiklars.Where(t => t.ID == itemCevap.value && t.IsActive == true && t.IsDelete == false).FirstOrDefaultAsync();
                    if (detay2Cevap != null)
                    {
                        detay2Cevap.AltSoruSikki = siklar[itemCevap.Index];
                        await Entity.SaveChangesAsync();
                    }
                }
                kaydet = true;

            }
            catch
            {
                kaydet = false;
            }


            return Json(kaydet, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TanimliSorular(int? page, int? soruTip, int? sBagajKategori, string aranan, int? soruNo)
        {
            ViewBag.SinavTipi = Entity.SinavTipis.ToList();
            ViewBag.SBagajKategori = Entity.SBagajKategoris.Where(e=>e.IsActive && !e.IsDelete).ToList();

            soruTip = soruTip.HasValue ? Convert.ToInt32(soruTip) : -1;
            sBagajKategori = sBagajKategori.HasValue ? Convert.ToInt32(sBagajKategori) : 0;

            ViewBag.Sinavlar = Entity.Sinavlars.Where(t => t.IsActive == true).ToList();
            ViewBag.Uniteler = Entity.Unites.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            IPagedList<UniteSorular> list = null;
            int pageSize = 24;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IQueryable<UniteSorular> listGnl = Entity.UniteSorulars.OrderBy(m => m.ID).Where(t => t.IsActive == true && t.IsDelete == false);
            if (!string.IsNullOrEmpty(aranan))
            {
                listGnl = listGnl.Where(r => r.Soru.Contains(aranan));
            }
            if (soruTip != -1)
            {
                if (sBagajKategori == 0)
                {
                    list = listGnl.Where(t => t.SinavTipi_Ref == soruTip).ToPagedList(pageIndex, pageSize);
                }
                else
                {
                    list = listGnl.Where(t => t.SinavTipi_Ref == soruTip && t.SBagajlar.SKategori_Ref == sBagajKategori).ToPagedList(pageIndex, pageSize);
                }
            }
            else
            {
                if (sBagajKategori == 0)
                {
                    list = listGnl.ToPagedList(pageIndex, pageSize);
                }
                else
                {
                    list = listGnl.Where(q => q.SBagajlar.SKategori_Ref == sBagajKategori).ToPagedList(pageIndex, pageSize);
                }
            }
            if (soruNo != null)
            {
                list = listGnl.Where(t => t.ID == soruNo).ToPagedList(pageIndex, pageSize);
            }
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            ViewBag.SinavTipDn = soruTip;
            ViewBag.SBagajKategoriDn = sBagajKategori;
            ViewBag.ArananDn = aranan;
            ViewBag.SoruNo = soruNo;
            ViewBag.Roller = user.getAutorizeUser().Roller;
            return View(list);
        }


        public async Task<ActionResult> TSoruGuncelle(int id)
        {
            ViewBag.Uniteler = await Entity.Unites.Where(r => r.IsActive == true && r.IsDelete == false).ToListAsync();
            UniteSorular data = await Entity.UniteSorulars.Where(t => t.ID == id).FirstOrDefaultAsync();
            //ViewBag.usiklar = Entity.SoruSiklaris.Where(r => r.UniteSorular_Ref == data.ID && r.IsActive == true).Select(t => new SoruSiklariLocal { ID = t.ID, SoruSikki = t.SoruSikki, CevapIcerik = t.CevapIcerik, DogruSecenek = t.DogruSecenek }).ToList();
            //ViewBag.altSiklar = Entity.AltSiklars.Where(t => t.UniteSorular_Ref == data.ID && t.IsActive == true).ToList();

            return View(data);
        }

        public async Task<ActionResult> SoruIcerik(int id)
        {
            ViewBag.list = await Entity.UniteSorulars.Where(t => t.Unite_Ref == id).Select(r => new { r.ID, r.IsActive, r.SinavTipi_Ref, r.Soru }).ToListAsync();
            return View();

        }

        #region Sınav Tanımlama
        public ActionResult Sinavlar()
        {
            var data = Entity.Sinavlars.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            return View(data);
        }

        public async Task<JsonResult> SinavlarGuncelle(int id, string SinavNo, string SoruSayisi, string Aciklama, byte SinavTipi)
        {
            bool durum = false;
            try
            {
                var data = await Entity.Sinavlars.Where(t => t.ID == id).FirstOrDefaultAsync();
                data.SinavNo = SinavNo;
                data.SoruSayisi = int.Parse(SoruSayisi);
                data.Aciklama = Aciklama;
                data.SinavTipi = SinavTipi;
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SinavlarSil(int id)
        {
            bool durum = false;
            try
            {
                var data = await Entity.Sinavlars.Where(t => t.ID == id).FirstOrDefaultAsync();
                data.IsActive = false;
                data.IsDelete = true;
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> SinavKaydet(string SinavNo, int SoruSayisi, string Aciklama)
        {
            var kontrol = await Entity.Sinavlars.Where(y => y.SinavNo == SinavNo).CountAsync();
            if (kontrol == 0)
            {
                Sinavlar data = new Sinavlar();
                data.IsActive = true;
                data.IsDelete = false;
                data.SinavNo = SinavNo;
                data.SoruSayisi = SoruSayisi;
                data.Aciklama = Aciklama;
                Entity.Sinavlars.Add(data);
                await Entity.SaveChangesAsync();
            }
            return RedirectToAction("Sinavlar", "SinavIslemler", new { area = "Panel" });
        }
        #endregion

        public ActionResult SinavSorular(int id)
        {
            ViewData["SinavTipi"] = Entity.SinavTipis.ToList();
            ViewData["Unite"] = Entity.Unites.Include("UniteSorulars").Where(r => r.IsActive == true).ToList();
            var sinavlar = Entity.Sinavlars.Where(t => t.ID == id).Select(r => new { r.ID, r.SinavNo }).FirstOrDefault();
            ViewBag.SinavNo = sinavlar.SinavNo;
            ViewBag.SinavID = sinavlar.ID;
            var sinavSorular = Entity.Sinav_Sorular.Where(t => t.Sinavlar_Ref == id && t.IsDelete == false).ToList();
            return View(sinavSorular);
        }

        public ActionResult SinavCevaplama(int? sinavID)
        {
            int sinavIndex = sinavID.HasValue ? Convert.ToInt32(sinavID) : 0;
            ViewBag.Sinavlar = Entity.Sinavlars.Where(t => t.IsActive == true).ToList();
            DateTime Bugun = DateTime.Now.Date;
            List<Personel_Sinavlar> data = Entity.Personel_Sinavlar.Where(t => t.IsActive == true && t.Sinavlar_Ref == sinavIndex && ((t.SinavTarihi <= Bugun && t.Bitti == 1))).ToList();
            ViewBag.SinavID = sinavIndex;
            return View(data);
        }

        public ActionResult _USorular(int? page, int id, int tip, int sinavno)
        {

            //int pageSize = 100;
            //int pageIndex = 1;
            //int recordNo = 0;
            //SinavSoruOranlama orn = new SinavSoruOranlama(Entity);
            //pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            //IQueryable<UniteSorular> data = Entity.UniteSorulars.OrderBy(m => m.ID).Where(t => t.IsActive == true && t.IsDelete == false && t.Unite_Ref==id && orn.SoruOranGetir(t.ID) < 90);
            //IQueryable<sp_UniteSorular_Result> data = Entity.sp_UniteSorular(id, 1);

            //List<UniteSorularPage> soruData = new List<UniteSorularPage>();

            //if (tip == (int)Enumlar.SoruTipleri.TEST)
            //{
            //    var lst = data.Where(t => t.SinavTipi_Ref == tip || t.SinavTipi_Ref == (int)Enumlar.SoruTipleri.TEORIK).ToList();
            //    for (int i = 0; i < lst.Count; i++)
            //    {
            //        var oranadet = orn.SoruOranAdetGetir(lst[i].ID);
            //        UniteSorularPage soru = new UniteSorularPage();
            //        soru.UniteSoru = lst[i];
            //        soru.bulunmaOran = oranadet.oran;
            //        soru.bulunmaAdet = oranadet.adet;
            //        soruData.Add(soru);
            //    }              
            //}
            //else
            //{
            IObjectContextAdapter dbcontextadapter = (IObjectContextAdapter)Entity;
            dbcontextadapter.ObjectContext.CommandTimeout = 300;
            List<sp_UniteSorular_Result1> list = Entity.sp_UniteSorular(id, tip).ToList();
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    var oranadet = orn.SoruOranAdetGetir(lst[i].ID);
            //    if (oranadet.oran < 90)
            //    {
            //        UniteSorularPage soru = new UniteSorularPage();
            //        soru.UniteSoru = lst[i];
            //        soru.bulunmaOran = oranadet.oran;
            //        soru.bulunmaAdet = oranadet.adet;
            //        soruData.Add(soru);
            //    }
            //}              
            //}
            //list = lst.ToPagedList(pageIndex, pageSize);
            //if (pageIndex != 1)
            //{
            //    recordNo = ((pageIndex - 1) * pageSize);
            //}
            //ViewBag.RecordNo = recordNo;
            ViewBag.Sinavno = sinavno;
            ViewBag.Tip = tip;
            return PartialView("_UniteSorular", list);
        }

        public async Task<JsonResult> SPKaydetData(List<String> SOgrenciID, int SinavID, string SinavTarih, string BaslamaSaati, string BitisSaati)
        {
            string durum = "Kayıt ekleme Hatalı";
            foreach (var item in SOgrenciID)
            {
                int idp = int.Parse(item);
                var controldd = await Entity.Personel_Sinavlar.Where(r => r.Sinif_Ogrenci_Ref == idp && r.Sinavlar_Ref == SinavID).CountAsync();
                if (controldd == 0)
                {
                    double surex = TimeSpan.Parse(BitisSaati).TotalMinutes - TimeSpan.Parse(BaslamaSaati).TotalMinutes;
                    int zaman = Convert.ToInt32(surex);
                    var data = new Personel_Sinavlar();
                    data.IsActive = true;
                    data.IsDelete = false;
                    data.Sinif_Ogrenci_Ref = idp;
                    data.Sinavlar_Ref = SinavID;
                    data.SinavAciklama = false;
                    data.SinavNotuUygulama = 0;
                    data.SinavNotuTeorik = 0;
                    data.Durum = "AÇIKLANMADI";
                    data.DogruSayisiUygulama = 0;
                    data.YanlisSayisiTeorik = 0;
                    data.UygulamaBos = 0;
                    data.YanlisSayisiUgulama = 0;
                    data.DogruSayisiTeorik = 0;
                    data.TeorikBos = 0;
                    data.SinavTarihi = Convert.ToDateTime(SinavTarih);
                    data.BaslamaSaati = TimeSpan.Parse(BaslamaSaati);
                    data.BitisSaati = TimeSpan.Parse(BitisSaati);
                    data.SinavSuresi = zaman;
                    data.Basladi = 0;
                    data.Bitti = 0;
                    data.GirisSaati = null;
                    data.CikisSaati = null;
                    Entity.Personel_Sinavlar.Add(data);
                    await Entity.SaveChangesAsync();
                    durum = "ekleme İşlemi Gerçekleşti";
                }
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SinavSoruKaydet(int SinavTipi_Ref, int UniteSorular_Ref, int Sinavlar_Ref)
        {
            bool durum = false;
            try
            {
                var data = new Sinav_Sorular();
                data.IsActive = true;
                data.IsDelete = false;
                data.Sinavlar_Ref = Sinavlar_Ref;
                data.SinavTipi_Ref = SinavTipi_Ref;
                data.UniteSorular_Ref = UniteSorular_Ref;
                Entity.Sinav_Sorular.Add(data);
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }


            //var tSinavTipi_Ref = SinavTipi_Ref.Select((t, Index) => new { Name = t, Ix = Index });
            //var tUniteSorular_Ref = UniteSorular_Ref.Select((t, Index) => new { Name = t, Ix = Index });
            //var tSinavlar_Ref = Sinavlar_Ref.Select((t, Index) => new { Name = t, Ix = Index });

            //var subDetail = from SinavTipifx in tSinavTipi_Ref
            //                join UniteSorulartx in tUniteSorular_Ref on SinavTipifx.Ix equals UniteSorulartx.Ix
            //                join Sinavlarfx in tSinavlar_Ref on SinavTipifx.Ix equals Sinavlarfx.Ix

            //                    select new Sinav_Sorular
            //                    {
            //                        IsActive = true,
            //                        IsDelete = false,
            //                         Sinavlar_Ref = Sinavlarfx.Name,
            //                         SinavTipi_Ref = SinavTipifx.Name,
            //                         UniteSorular_Ref = UniteSorulartx.Name
            //                    };
            //    foreach (var itemSinav in subDetail)
            //    {
            //        Entity.Sinav_Sorular.Add(itemSinav);                   
            //    }
            //    Entity.SaveChanges();

            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }

        public async Task<ActionResult> PersonelSinav()
        {
            ViewBag.Sinif = await Entity.Sinifs.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            ViewBag.SinifOgrenci = await Entity.Sinif_Ogrenci.ToListAsync();
            ViewBag.Sinavlar = await Entity.Sinavlars.Where(y => y.IsActive == true).ToListAsync();
            return View();
        }

        public async Task<JsonResult> SPsil(int id)
        {
            string durum = "";
            try
            {
                var data = await Entity.Personel_Sinavlar.Where(t => t.ID == id).FirstOrDefaultAsync();
                bool kontrol = data.Basladi == 1 ? false : true;
                if (kontrol)
                {
                    data.IsActive = false;
                    data.IsDelete = true;
                    await Entity.SaveChangesAsync();
                    durum = "true";
                }
            }
            catch
            {
                durum = "false";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> TanimliSoruGuncelle(int id)
        {
            bool durum = false;
            try
            {
                var data = await Entity.Sinav_Sorular.Where(t => t.ID == id).FirstOrDefaultAsync();
                data.IsActive = false;
                data.IsDelete = true;
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SinavSonuclandirma(List<String> postData)
        {
            bool durum = false;
            try
            {
                foreach (var item in postData)
                {
                    List<SinavSonuclari> cevaplar = new List<SinavSonuclari>();
                    int id = int.Parse(item);
                    int sinavEtiket = await Entity.Personel_Sinavlar.Where(t => t.ID == id && t.IsActive == true && t.IsDelete == false).Select(y => y.Sinavlar_Ref).FirstOrDefaultAsync();
                    var sinavSorular = await Entity.Sinav_Sorular.Where(r => r.Sinavlar_Ref == sinavEtiket && r.IsActive == true && r.IsDelete == false).ToListAsync();
                    foreach (var item2 in sinavSorular)
                    {
                        var cevapkontrol = item2.SinavCevaplars.Where(r => r.Personel_Sinavlar_Ref == id).Select(t => new { t.Netice, t.Sınav_Sorular_Ref }).FirstOrDefault();

                        string cevabi = "";
                        if (cevapkontrol == null)
                        {
                            cevabi = "YOK";
                        }
                        else
                        {
                            if (cevapkontrol.Netice == true)
                            {
                                cevabi = "True";
                            }
                            else
                            {
                                cevabi = "False";
                            }
                        }
                        SinavSonuclari datax = new SinavSonuclari();
                        datax.PerID = id;
                        datax.SoruID = item2.ID;
                        datax.Cevap = cevabi;
                        datax.SoruTipi = item2.SinavTipi_Ref;
                        cevaplar.Add(datax);
                    }

                    int TeorikDogruSayisi = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "True").Count();
                    int TeorikYanlisSayisi = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "False").Count();
                    int TeorikBos = cevaplar.Where(t => t.SoruTipi == 2 && t.Cevap == "YOK").Count();

                    int UygulamaDogruSayisi = cevaplar.Where(t => t.SoruTipi == 1 && t.Cevap == "True").Count();
                    int UygulamaYanlisSayisi = cevaplar.Where(t => t.SoruTipi == 1 && t.Cevap == "False").Count();
                    int UygulamaBos = cevaplar.Where(t => t.SoruTipi == 1 && t.Cevap == "YOK").Count();

                    decimal TamPuan = 100;

                    int ToplamSoruUygulama = sinavSorular.Where(y => y.SinavTipi_Ref == 1).Count();
                    int ToplamSoruTeorik = sinavSorular.Where(y => y.SinavTipi_Ref == 2).Count();
                    decimal soruPuanUygulama = 0;
                    decimal soruPuanTeorik = 0;
                    if (ToplamSoruUygulama != 0)
                    {
                        soruPuanUygulama = TamPuan / ToplamSoruUygulama;
                    }
                    if (ToplamSoruTeorik != 0)
                    {
                        soruPuanTeorik = TamPuan / ToplamSoruTeorik;
                    }


                    decimal SinavNotuUygulama = soruPuanUygulama * UygulamaDogruSayisi;
                    decimal SinavNotuTeorik = soruPuanTeorik * TeorikDogruSayisi;

                    string Netice = "";
                    int uygulamaSoruVami = sinavSorular.Where(e => e.SinavTipi_Ref == (int)Enumlar.SoruTipleri.UYGULAMA).Count();
                    int teorikSoruVarmi = sinavSorular.Where(e => e.SinavTipi_Ref == (int)Enumlar.SoruTipleri.TEORIK).Count();

                    if (uygulamaSoruVami > 0 && teorikSoruVarmi > 0)
                    {
                        if (SinavNotuUygulama >= 75 && SinavNotuTeorik >= 75)
                        {
                            Netice = "GEÇTİ";
                        }
                        else
                        {
                            Netice = "KALDI";
                        }
                    }
                    else if (uygulamaSoruVami > 0 && teorikSoruVarmi == 0)
                    {
                        if (SinavNotuUygulama >= 75)
                        {
                            Netice = "GEÇTİ";
                        }
                        else
                        {
                            Netice = "KALDI";
                        }
                    }
                    else if (uygulamaSoruVami == 0 && teorikSoruVarmi > 0)
                    {
                        if (SinavNotuTeorik >= 75)
                        {
                            Netice = "GEÇTİ";
                        }
                        else
                        {
                            Netice = "KALDI";
                        }
                    }



                    var perSinavIsleme = await Entity.Personel_Sinavlar.Where(t => t.ID == id).FirstOrDefaultAsync();
                    perSinavIsleme.DogruSayisiUygulama = UygulamaDogruSayisi;
                    perSinavIsleme.DogruSayisiTeorik = TeorikDogruSayisi;
                    perSinavIsleme.YanlisSayisiUgulama = UygulamaYanlisSayisi;
                    perSinavIsleme.YanlisSayisiTeorik = TeorikYanlisSayisi;
                    perSinavIsleme.SinavNotuUygulama = SinavNotuUygulama;
                    perSinavIsleme.SinavNotuTeorik = SinavNotuTeorik;
                    perSinavIsleme.UygulamaBos = UygulamaBos;
                    perSinavIsleme.TeorikBos = TeorikBos;
                    perSinavIsleme.Durum = Netice;
                    await Entity.SaveChangesAsync();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DuyuruGonder()
        {
            bool durum = false;
            try
            {
                var data = await Entity.Personel_Sinavlar.Where(t => t.Durum != "AÇIKLANMADI").ToListAsync();
                foreach (var item in data)
                {
                    item.SinavAciklama = true;
                    item.DuyuruTarihi = DateTime.Now.Date;
                }
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }

            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> _SinavaAtananPersonel(int id)
        {
            var data = await Entity.Personel_Sinavlar.Where(e => e.IsActive == true && e.IsDelete == false && e.Sinavlar_Ref == id).ToListAsync();
            return PartialView("_PersonelSinavlar", data);
        }

        public async Task<ActionResult> _SinifOgrenci(int sinifID)
        {
            DateTime zaman = DateTime.Now.Date;
            var list = await Entity.Sinif_Ogrenci.Where(t => t.Sinif_Ref == sinifID && t.BitisTarihi >= zaman).ToListAsync();
            //var sonlst = list.Where(t=>t.Personel.Kullanicis.Where(r=>r.BitisTarihi.Value)).FirstOrDefault();
            //foreach (var item in list)
            //{                
            //    int kontrol = item.Personel.Kullanicis.Where(y=>y.BitisTarihi >= zaman).Count();
            //    if (kontrol != 0)
            //    {

            //    }
            //}


            return PartialView("_SinifOgrenciler", list);
        }

        public async Task<JsonResult> AtanmisPersonelCikarma(int sinavID)
        {
            var list = await Entity.Personel_Sinavlar.Where(t => t.Sinavlar_Ref == sinavID && t.IsActive == true).Select(y => y.Sinif_Ogrenci.Personel_Ref).ToListAsync();
            return Json(list, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        protected string savedocument(HttpPostedFileBase document)
        {
            if (document != null)
            {
                FileInfo f = new FileInfo(document.FileName);
                string Fname = Guid.NewGuid() + f.Name;
                string path = "Areas/Sinavlar/Images/" + Fname;
                document.SaveAs(Server.MapPath("~/" + path));
                return path;
            }
            else
            {
                return "";
            }

        }



        #region Sınav Tipi İşlemleri

        public async Task<ActionResult> SinavTip()
        {
            var data = await Entity.SinavTipis.ToListAsync();
            return View(data);
        }
        [HttpPost]
        public async Task<ActionResult> SinavTipiEkle(string Baslik)
        {
            SinavTipi blm = new SinavTipi();
            blm.Baslik = Baslik;
            Entity.SinavTipis.Add(blm);
            await Entity.SaveChangesAsync();
            return RedirectToAction("SinavTip", "SinavIslemler", new { area = "Panel" });
        }

        public async Task<JsonResult> SinavTipiGuncelle(int id, string Baslik)
        {
            bool durum = false;
            try
            {
                SinavTipi blm = await Entity.SinavTipis.Where(t => t.ID == id).FirstOrDefaultAsync();
                blm.Baslik = Baslik;
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Test Tanımlama
        public async Task<ActionResult> Testler()
        {
            ViewBag.Moduller = await Entity.UniteModuls.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            var data = await Entity.Testlers.Where(r => r.IsActive == true && r.IsDelete == false).ToListAsync();
            return View(data);
        }

        public async Task<JsonResult> TestlerGuncelle(int id, string TestNo, string SoruSayisi, int Sure, string Aciklama, int UniteModul_Ref)
        {
            bool durum = false;
            try
            {
                var kontrol = await Entity.Testlers.Where(y => y.TestNo == TestNo && y.UniteModul_Ref == UniteModul_Ref).CountAsync();
                if (kontrol == 0)
                {
                    var data = await Entity.Testlers.Where(t => t.ID == id).FirstOrDefaultAsync();
                    data.TestNo = TestNo;
                    data.SoruSayisi = int.Parse(SoruSayisi);
                    data.Sure = Sure;
                    data.Aciklama = Aciklama;
                    data.UniteModul_Ref = UniteModul_Ref;
                    await Entity.SaveChangesAsync();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> TestSilme(int id)
        {
            bool durum = false;
            try
            {
                var durumKontrol = await Entity.Testlers.Where(t => t.IsActive == true && t.IsDelete == false && t.ID == id && t.Personel_Testler.Count == 0).FirstOrDefaultAsync();
                if (durumKontrol != null)
                {
                    durumKontrol.IsActive = false;
                    durumKontrol.IsDelete = true;
                    await Entity.SaveChangesAsync();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> TestKaydet(string TestNo, int SoruSayisi, int Sure, string Aciklama, int UniteModul_Ref)
        {
            var kontrol = await Entity.Testlers.Where(y => y.TestNo == TestNo && y.UniteModul_Ref == UniteModul_Ref).CountAsync();
            if (kontrol == 0)
            {
                Testler data = new Testler();
                data.IsActive = true;
                data.IsDelete = false;
                data.TestNo = TestNo;
                data.SoruSayisi = SoruSayisi;
                data.Sure = Sure;
                data.Aciklama = Aciklama;
                data.UniteModul_Ref = UniteModul_Ref;
                Entity.Testlers.Add(data);
                await Entity.SaveChangesAsync();
            }
            return RedirectToAction("Testler", "SinavIslemler", new { area = "Panel" });
        }

        public async Task<ActionResult> TestSorular(int id, int UniteModul_Ref)
        {
            ViewData["Unite"] = await Entity.Unites.Where(r => r.IsActive == true && r.Modul_Ref == UniteModul_Ref).ToListAsync();
            ViewBag.UniteModulRef = UniteModul_Ref;
            var data = await Entity.Testlers.Where(t => t.ID == id).FirstOrDefaultAsync();
            return View(data);
        }

        public async Task<JsonResult> TestKursModulUniteler(int id)
        {
            var data = await Entity.Unites.Where(t => t.IsActive == true && t.IsDelete == false && t.Modul_Ref == id).Select(r => new { r.ID, r.UniteAdi }).ToListAsync();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> TestSoruKaydet(int Test_Ref, int UniteSorular_Ref)
        {
            bool durum = false;
            try
            {
                var kontrol = await Entity.Test_Sorular.Where(r => r.Testler_Ref == Test_Ref && r.UniteSorular_Ref == UniteSorular_Ref).FirstOrDefaultAsync();
                if (kontrol == null)
                {
                    var data = new Test_Sorular();
                    data.IsActive = true;
                    data.IsDelete = false;
                    data.Testler_Ref = Test_Ref;
                    data.UniteSorular_Ref = UniteSorular_Ref;
                    Entity.Test_Sorular.Add(data);
                    await Entity.SaveChangesAsync();
                }
                else
                {
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    await Entity.SaveChangesAsync();
                }
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }


        public async Task<JsonResult> TanimliSoruGuncelleTest(int id)
        {
            bool durum = false;
            try
            {
                var data = await Entity.Test_Sorular.Where(t => t.ID == id).FirstOrDefaultAsync();
                data.IsActive = false;
                data.IsDelete = true;
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }

    public class SinavSonuclari : IDisposable
    {
        public int PerID { get; set; }
        public int SoruID { get; set; }
        public string Cevap { get; set; }
        public int SoruTipi { get; set; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public class SoruSiklariLocal : IDisposable
    {

        public int ID { get; set; }
        public string SoruSikki { get; set; }
        public string CevapIcerik { get; set; }
        public bool DogruSecenek { get; set; }


        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public class ResimIslem : IDisposable
    {
        public int? ID { get; set; }
        public string Baslik { get; set; }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

}
