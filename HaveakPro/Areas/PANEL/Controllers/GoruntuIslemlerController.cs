﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HaveakPro.Models;
using PagedList;
using HaveakPro.Models.Methods;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Web.UI;

namespace HaveakPro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_BagajTehlikelimadde)]
    public class GoruntuIslemlerController : Controller
    {
        //
        // GET: /Panel/GoruntuIslemler/
        HaveakEntities Entity;
        public GoruntuIslemlerController(HaveakEntities _Entity)
        {
            Entity = _Entity;
            Authorizing.SecilenModul = "PANEL";
        }

        #region Kategori İşlemler


        public ActionResult TehlikeliMaddeKategori(int? page)
        {
            int pageSize = 100;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<TehlikeliMaddeKategori> listb = Entity.TehlikeliMaddeKategoris.OrderByDescending(m => m.ID).Where(t => t.IsActive == true && t.IsDelete == false).ToPagedList(pageIndex, pageSize);
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            return View(listb);
        }


        [HttpPost]
        public async Task<ActionResult> SaveTMKategori(string Baslik, HttpPostedFileBase Resim)
        {
            TehlikeliMaddeKategori tip = new TehlikeliMaddeKategori();
            tip.IsActive = true;
            tip.IsDelete = false;
            tip.Baslik = Baslik;
            tip.Resim = savedocument(Resim);
            Entity.TehlikeliMaddeKategoris.Add(tip);
            await Entity.SaveChangesAsync();
            return RedirectToAction("TehlikeliMaddeKategori", "GoruntuIslemler", new { area = "Panel" });
        }

        public async Task<JsonResult> UpdateTMKategori(int ID, string Baslik, HttpPostedFileBase Resim)
        {
            bool result = false;
            try
            {
                TehlikeliMaddeKategori data = await Entity.TehlikeliMaddeKategoris.Where(r => r.ID == ID).FirstOrDefaultAsync();
                data.Baslik = Baslik;
                if (Resim != null)
                {
                    var silDosya = Server.MapPath("~/" + data.Resim);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    data.Resim = savedocument(Resim);
                }
                await Entity.SaveChangesAsync();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Kategori Madde İşlemler
        public async Task<ActionResult> Maddeler()
        {
            IPagedList<Maddeler> Detay = null;
            ViewBag.RecordNo = 0;
            ViewBag.TMkategori = await Entity.TehlikeliMaddeKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
            return View(Detay);
        }

        public ActionResult MaddelerAra(int? page, string search)
        {
            int pageSize = 100;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<Maddeler> listb = Entity.Maddelers.OrderBy(m => m.ID).Where(t => t.ResimAd.Contains(search) && t.IsActive == true && t.IsDelete == false).ToPagedList(pageIndex, pageSize);
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            return View("Maddeler", listb);
        }

        public async Task<JsonResult> DeleteMaddeler(int ID)
        {
            bool durum = false;
            var bgj = await Entity.Maddelers.Where(t => t.ID == ID).FirstOrDefaultAsync();
            try
            {
                bgj.IsActive = false;
                bgj.IsDelete = true;
                Entity.SaveChanges();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> _MaddeResimGetir(List<int> IDs)
        {
            List<Maddeler> bgjlar = await Entity.Maddelers.Where(item => IDs.Contains(item.ID)).ToListAsync();
            return PartialView("_ResimKategori", bgjlar);
        }

        public async Task<ActionResult> _MaddeAltResimGetir(int Id)
        {
            List<Maddeler> bgjlar = await Entity.Maddelers.Where(i=> i.AltResimRef == Id).ToListAsync();
            return PartialView("_AltKategoriResimler", bgjlar);
        }


        public async Task<JsonResult> MaddeResimGrupla(List<ResimKategori> data)
        {
            bool durum = false;
            int seciliId = data.Where(w => w.statu == true).Select(q => q.id).FirstOrDefault();
            Maddeler anabgj = await Entity.Maddelers.Where(t => t.ID == seciliId).FirstOrDefaultAsync();
            anabgj.AltResimRef = 0;
            await Entity.SaveChangesAsync();
            foreach (var item in data)
            {
                if (item.id != seciliId)
                {
                    Maddeler guncelleAlt = await Entity.Maddelers.Where(w => w.ID == item.id).FirstOrDefaultAsync();
                    guncelleAlt.AltResimRef = seciliId;
                }
            }
            try
            {
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> UpdateMaddeler(int ID, HttpPostedFileBase Resim)
        {
            bool durum = false;
            var madde = await Entity.Maddelers.Where(t => t.ID == ID).FirstOrDefaultAsync();
            try
            {
                madde.ResimAd = Resim.FileName.Split('.')[0];
                if (Resim != null)
                {
                    var silDosya = Server.MapPath("~/" + madde.Resim);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    madde.Resim = savedocument(Resim);
                }
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult KategoriMaddelerGetir(int? page, int? KatID)
        {
            int pageSize = 48;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<Maddeler> Detay = Entity.Maddelers.OrderBy(t => t.ID).Where(t => t.IsActive == true && t.IsDelete == false && t.TehlikeliMadde_Ref == KatID && (t.AltResimRef == 0 || t.AltResimRef == null)).ToPagedList(pageIndex, pageSize);
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.GelenKatID = KatID;
            ViewBag.RecordNo = recordNo;
            ViewBag.TMkategori = Entity.TehlikeliMaddeKategoris.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            return View("Maddeler", Detay);
        }

        public async Task<JsonResult> GrupSil(int Id)
        {
            bool durum = false;
            var data = await Entity.Maddelers.Where(e => e.ID == Id).FirstOrDefaultAsync();
            if (data.AltResimRef == 0)
            {
                var altResimKkontrol = await Entity.Maddelers.Where(q => q.AltResimRef == Id).CountAsync();
                if (altResimKkontrol != 0)
                {
                    durum = false;
                }
                else
                {
                    data.AltResimRef = null;
                    await Entity.SaveChangesAsync();
                    durum = true;
                }
            }
            else
            {
                data.AltResimRef = null;
                await Entity.SaveChangesAsync();
                durum = true;
            }
            return Json(durum, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public async Task<ActionResult> SaveMaddeler(HttpPostedFileBase[] Resim, int TehlikeliMadde_Ref)
        {
            foreach (var item in Resim)
            {
                Maddeler tip = new Maddeler();
                tip.IsActive = true;
                tip.IsDelete = false;
                tip.ResimAd = item.FileName.Split('.')[0];
                tip.Resim = savedocument(item);
                tip.TehlikeliMadde_Ref = TehlikeliMadde_Ref;
                Entity.Maddelers.Add(tip);
            }
            await Entity.SaveChangesAsync();
            return RedirectToAction("KategoriMaddelerGetir", "GoruntuIslemler", new { area = "Panel", page = 1, KatID = TehlikeliMadde_Ref });
        }

        #endregion

        //protected string savedocumentTm(HttpPostedFileBase document, string folderName)
        //{
        //    if (document != null)
        //    {
        //        if (!Directory.Exists(Server.MapPath("~/DocumentFiles/imagebank/" + folderName)))
        //        {
        //            Directory.CreateDirectory(Server.MapPath("~/DocumentFiles/imagebank/" + folderName));
        //        }
        //        FileInfo f = new FileInfo(document.FileName);
        //        string Fname = Guid.NewGuid() + f.Name;
        //        string path = "DocumentFiles/imagebank/" + folderName + "/" + Fname;
        //        //f.Directory.Create();
        //        document.SaveAs(Server.MapPath("~/" + path));
        //        return path;
        //    }
        //    else
        //    {
        //        return "";
        //    }

        //}

        protected string savedocument(HttpPostedFileBase document)
        {
            if (document != null)
            {
                FileInfo f = new FileInfo(document.FileName);
                string Fname = Guid.NewGuid() + f.Name;
                string path = "DocumentFiles/imagebank/" + Fname;
                f.Directory.Create();
                document.SaveAs(Server.MapPath("~/" + path));
                return path;
            }
            else
            {
                return "";
            }

        }
               
    }

    public class ResimKategori
    {
        public int id { get; set; }
        public bool statu { get; set; }
    }
}
