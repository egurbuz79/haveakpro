﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HaveakPro.Models;
using PagedList;
using HaveakPro.Models.Methods;
using System.Threading.Tasks;
using System.Data.Entity;
using HaveakPro.Models.Interface;
using System.Web.UI;

namespace HaveakPro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_PersonelKullanici)]
    public class PersonelIslemlerController : Controller
    {
        HaveakEntities Entity;
        I_UserModel user;
        public PersonelIslemlerController(HaveakEntities _Entity, I_UserModel _user)
        {
            Entity = _Entity;
            user = _user;
            Authorizing.SecilenModul = "PANEL";
        }


        public ActionResult PersonelListe(int? page)
        {
            int pageSize = 12;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            IPagedList<Personel> data = Entity.Personels.OrderByDescending(m => m.ID).Where(r => r.IsActive == true && r.IsDelete == false).ToPagedList(pageIndex, pageSize);

            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            ViewBag.KisiAra = "";
            return View(data);
        }

        public ActionResult PersonelAra(int? page, string adsoyad)
        {
            int pageSize = 12;
            int pageIndex = 1;
            int recordNo = 0;

            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<Personel> data = data = Entity.Personels.OrderByDescending(m => m.ID).Where(r => (r.Adi.Contains(adsoyad) || r.Soyadi.Contains(adsoyad)) && r.IsActive == true && r.IsDelete == false).ToPagedList(pageIndex, pageSize);

            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            ViewBag.KisiAra = adsoyad;
            return View("PersonelListe", data);
        }

        public ActionResult PersonelIslem(int? id)
        {
            int dx = id == null ? 0 : id.Value;
            return View(dx);
        }


        [HttpPost]
        public async Task<ActionResult> PersonelEkle(string Adi, string Soyadi, string TCNo, string IsTelefon, string EvTelefon, string CepTelefon, string Email, string Adresi, string Cinsiyet, string DogumTarihi, HttpPostedFileBase Resim)
        {
            try
            {
                Personel detay = new Personel();
                detay.IsActive = true;
                detay.IsDelete = false;
                detay.Adi = Adi;
                detay.Soyadi = Soyadi;
                detay.TCNo = TCNo;
                detay.IsTelefon = IsTelefon;
                detay.EvTelefon = EvTelefon;
                detay.CepTelefon = CepTelefon;
                detay.Email = Email;
                detay.Adresi = Adresi;
                detay.Cinsiyet = Cinsiyet;
                detay.DogumTarihi = Convert.ToDateTime(DogumTarihi);
                if (Resim != null)
                {
                    detay.Resim = savedocument(Resim);
                }
                Entity.Personels.Add(detay);
                await Entity.SaveChangesAsync();
            }
            catch
            {

            }
            return RedirectToAction("PersonelListe", "PersonelIslemler", new { area = "Panel" });
        }

        public async Task<JsonResult> PersonelGuncelle(int ID, string Adi, string Soyadi, string TCNo, string IsTelefon, string EvTelefon, string CepTelefon, string Email, string Adresi, string Cinsiyet, DateTime DogumTarihi, HttpPostedFileBase Resim)
        {
            bool result = false;
            try
            {
                Personel detay = Entity.Personels.Where(r => r.ID == ID).FirstOrDefault();
                detay.Adi = Adi;
                detay.Soyadi = Soyadi;
                detay.TCNo = TCNo;
                detay.IsTelefon = IsTelefon;
                detay.EvTelefon = EvTelefon;
                detay.CepTelefon = CepTelefon;
                detay.Email = Email;
                detay.Adresi = Adresi;
                detay.Cinsiyet = Cinsiyet;
                detay.DogumTarihi = DogumTarihi;
                if (Resim != null)
                {
                    var silDosya = Server.MapPath("~/DocumentFiles/Personel/" + detay.Resim);
                    if (System.IO.File.Exists(silDosya))
                    {
                        System.IO.File.Delete(silDosya);
                    }
                    detay.Resim = savedocument(Resim);
                }
                await Entity.SaveChangesAsync();
                result = true;
            }
            catch
            {
                result = false;
            }
            return Json(result, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        protected string savedocument(HttpPostedFileBase document)
        {
            if (document != null)
            {
                FileInfo f = new FileInfo(document.FileName);
                string Fname = Guid.NewGuid().ToString().Split('-')[0] + f.Name;
                document.SaveAs(Server.MapPath("~/DocumentFiles/Personel/" + Fname));
                return Fname;
            }
            else
            {
                return "";
            }

        }

        public async Task<ActionResult> KullaniciRoller(int? page, string adi, string soyadi)
        {
            int pageSize = 42;
            int pageIndex = 1;
            int recordNo = 0;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            IPagedList<Kull_Rolleri> detail = null;
            if (!user.getAutorizeUser().Roller.Contains(UserRoleName.PANEL_AdminGiris))
            {
                detail = Entity.Kull_Rolleri.OrderBy(m => m.Kullanici.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false && (r.Kullanici.Personel.Adi.Contains(adi) && r.Kullanici.Personel.Soyadi.Contains(soyadi) && (!r.Kull_Rol_Adi.Name.Contains("ADMIN") || !r.Kull_Rol_Adi.Name.Contains("admin")))).ToPagedList(pageIndex, pageSize);
            }
            else
            {
                detail = Entity.Kull_Rolleri.OrderBy(m => m.Kullanici.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false && (r.Kullanici.Personel.Adi.Contains(adi) && r.Kullanici.Personel.Soyadi.Contains(soyadi))).ToPagedList(pageIndex, pageSize);
            }
            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            //var detail = Entity.Kull_Rolleri.Where(t => t.IsActive == true && t.IsDelete == false).ToList();
            ViewBag.Kull_Rol_Name = await Entity.Kull_Rol_Adi.Where(r => r.IsActive == true && r.IsDelete == false).ToListAsync();
            List<Kullanici> kullanicilar = await Entity.Kullanicis.Where(r => r.IsActive == true && r.IsDelete == false && r.Kull_Rolleri.Count == 0).ToListAsync();
            ViewBag.Kullanici = kullanicilar;
            ViewBag.Roller = user.getAutorizeUser().Roller;
            return View(detail);
        }

        [HttpPost]
        public async Task<ActionResult> KullaniciRollerEkle(KullaniciRollerData model)
        {
            foreach (var item in model.User_Ref)
            {
                foreach (var itemRol in model.User_Role_Name_Ref)
                {
                    var rolKontrol = await Entity.Kull_Rolleri.Where(y => y.IsActive == true && y.IsDelete == false && y.User_Ref == item && y.User_Role_Name_Ref == itemRol).FirstOrDefaultAsync();
                    if (rolKontrol == null)
                    {
                        Kull_Rolleri tip = new Kull_Rolleri();
                        tip.IsActive = true;
                        tip.IsDelete = false;
                        tip.User_Ref = item;
                        tip.User_Role_Name_Ref = itemRol;
                        Entity.Kull_Rolleri.Add(tip);
                    }
                    else
                    {
                        rolKontrol.IsActive = true;
                        rolKontrol.IsDelete = false;
                    }
                    await Entity.SaveChangesAsync();
                }
            }

            return RedirectToAction("KullaniciRoller", "PersonelIslemler", new { area = "Panel" });
        }

        public async Task<JsonResult> KullRollerGuncelle(int User_Role_Name_RefUp, int User_RefUp, bool control)
        {
            bool result = false;
            try
            {
                var data = await Entity.Kull_Rolleri.Where(r => r.User_Ref == User_RefUp && r.User_Role_Name_Ref == User_Role_Name_RefUp).FirstOrDefaultAsync();
                if (data == null)
                {
                    Kull_Rolleri yeni = new Kull_Rolleri();
                    yeni.IsActive = true;
                    yeni.IsDelete = false;
                    yeni.User_Ref = User_RefUp;
                    yeni.User_Role_Name_Ref = User_Role_Name_RefUp;
                    Entity.Kull_Rolleri.Add(yeni);
                }
                else
                {
                    if (control)
                    {
                        data.IsActive = true;
                        data.IsDelete = false;
                    }
                    else
                    {
                        data.IsActive = false;
                        data.IsDelete = true;
                    }
                }
                await Entity.SaveChangesAsync();
                result = true;
            }
            catch
            {
                result = false;
            }
            return Json(result, "apllication/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Kullanicilar()
        {
            ViewBag.PersonelListe = await Entity.Personels.Where(r => r.IsActive == true && r.IsDelete == false && Entity.Kullanicis.Where(y => y.Personel_Ref == r.ID && y.IsActive == true && y.IsDelete == false).Count() == 0).Select(w => new PersonelListe { ID = w.ID, Adi = w.Adi, Soyadi = w.Soyadi, TCNo = w.TCNo }).ToListAsync();
            return View();
        }

        public async Task<ActionResult> _KullaniciListesi(int? page)
        {
            int pageSize = 24;
            int pageIndex = 1;
            int recordNo = 0;

            List<Sinif> siniflar = new List<Sinif>();
            IPagedList<Kullanici> data = null;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            int ogrAdet = 0;
            if (!user.getAutorizeUser().Roller.Contains(UserRoleName.PANEL_AdminGiris))
            {
                data = Entity.Kullanicis.OrderBy(m => m.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false && !r.Kull_Rolleri.Any(t => t.Kull_Rol_Adi.Name.Contains("ADMIN") || t.Kull_Rol_Adi.Name.Contains("admin"))).ToPagedList(pageIndex, pageSize);
            }
            else
            {
                data = Entity.Kullanicis.OrderBy(m => m.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false).ToPagedList(pageIndex, pageSize);
            }            
            ogrAdet = await Entity.Kullanicis.OrderBy(m => m.Personel.Adi).Where(r => r.IsActive == true && r.IsDelete == false).CountAsync();
            siniflar = await Entity.Sinifs.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();

            if (pageIndex != 1)
            {
                recordNo = ((pageIndex - 1) * pageSize);
            }
            ViewBag.RecordNo = recordNo;
            ViewBag.OgrenciAdet = ogrAdet;
            ViewBag.Siniflar = siniflar;
            return PartialView("_KullaniciListe", data);
        }


        public async Task<JsonResult> PersonelGetir(int sinifi, string padi, string psoyadi)
        {

            List<PersonelBilgi> data = new List<PersonelBilgi>();
            if (sinifi != 0)
            {
                IQueryable<Kullanici> ddp = (from dx in Entity.Kullanicis
                                             orderby dx.Personel.Adi
                                             where dx.IsActive == true && dx.IsDelete == false &&
                                                 dx.Personel.Sinif_Ogrenci.Where(e => e.Sinif_Ref == sinifi).Select(t => t.ID).Count() > 0 &&
                                                 ((dx.Personel.Adi.Contains(padi)) &&
                                                 (dx.Personel.Soyadi.Contains(psoyadi)) &&
                                                 (dx.Personel.Adi.Contains(padi) || dx.Personel.Soyadi.Contains(psoyadi)))
                                             select dx);
                List<Kullanici> datax = new List<Kullanici>();
                if (!user.getAutorizeUser().Roller.Contains(UserRoleName.PANEL_AdminGiris))
                {
                    var adminEksik = ddp.Where(u => !u.Kull_Rolleri.Any(t => t.Kull_Rol_Adi.Name.Contains("ADMIN") || t.Kull_Rol_Adi.Name.Contains("admin")));
                    datax = await adminEksik.ToListAsync();
                }

                List<Kullanici> datas = await ddp.ToListAsync();
                for (int i = 0; i < datas.Count; i++)
                {
                    PersonelBilgi psr = new PersonelBilgi();
                    psr.ID = datas[i].ID;
                    var perref = datas[i].Personel_Ref;
                    psr.Sinif = await Entity.Sinif_Ogrenci.Where(r => r.Personel_Ref == perref).Select(e => e.Sinif.SinifAdi).ToListAsync();
                    psr.Adi = datas[i].Personel.Adi;
                    psr.Soyadi = datas[i].Personel.Soyadi;
                    psr.Kullaniciadi = datas[i].KullaniciAdi;
                    psr.BaslangicTarihi = datas[i].BaslangicTarihi;
                    psr.BitisTarihi = datas[i].BitisTarihi.Value;
                    psr.Sifre = datas[i].Sifre;
                    psr.Personel_Ref = datas[i].Personel_Ref;
                    psr.Resim = datas[i].Personel.Resim;
                    data.Add(psr);
                }
            }
            else
            {
                IQueryable<Kullanici> ddp = (from dx in Entity.Kullanicis
                                             orderby dx.Personel.Adi
                                             where dx.IsActive == true && dx.IsDelete == false &&
                                                 ((dx.Personel.Adi.Contains(padi)) &&
                                                 (dx.Personel.Soyadi.Contains(psoyadi)) &&
                                                 (dx.Personel.Adi.Contains(padi) || dx.Personel.Soyadi.Contains(psoyadi)))
                                             select dx);
                List<Kullanici> datax = new List<Kullanici>();

                if (!user.getAutorizeUser().Roller.Contains(UserRoleName.PANEL_AdminGiris))
                {
                    var adminEksik = ddp.Where(u => !u.Kull_Rolleri.Any(t => t.Kull_Rol_Adi.Name.Contains("ADMIN") || t.Kull_Rol_Adi.Name.Contains("admin")));
                    datax = await adminEksik.ToListAsync();
                }



                for (int i = 0; i < datax.Count; i++)
                {
                    PersonelBilgi psr = new PersonelBilgi();
                    psr.ID = datax[i].ID;
                    var perref = datax[i].Personel_Ref;
                    psr.Sinif = await Entity.Sinif_Ogrenci.Where(r => r.Personel_Ref == perref).Select(e => e.Sinif.SinifAdi).ToListAsync();
                    psr.Adi = datax[i].Personel.Adi;
                    psr.Soyadi = datax[i].Personel.Soyadi;
                    psr.Kullaniciadi = datax[i].KullaniciAdi;
                    psr.BaslangicTarihi = datax[i].BaslangicTarihi;
                    psr.BitisTarihi = datax[i].BitisTarihi.Value;
                    psr.Sifre = datax[i].Sifre;
                    psr.Personel_Ref = datax[i].Personel_Ref;
                    psr.Resim = datax[i].Personel.Resim;
                    data.Add(psr);
                }




                //     data = Entity.Kullanicis.OrderBy(m => m.Personel.Adi).Where(r => r.IsActive == true
                //&& r.IsDelete == false &&
                //((r.Personel.Adi.Contains(padi)) &&
                //(r.Personel.Soyadi.Contains(psoyadi)) &&
                //(r.Personel.Adi.Contains(padi) || r.Personel.Soyadi.Contains(psoyadi)))
                //).Select(f => new PersonelBilgi
                //{
                //    ID = f.ID,
                //    Sinif =  Entity.Sinif_Ogrenci.Where(r=>r.Personel_Ref == r.Personel_Ref).Select(e=>e.Sinif.SinifAdi).ToList(),
                //    Adi = f.Personel.Adi,
                //    Soyadi = f.Personel.Soyadi,
                //    Kullaniciadi = f.KullaniciAdi,
                //    BaslangicTarihi = f.BaslangicTarihi,
                //    BitisTarihi = f.BitisTarihi.Value,
                //    Sifre = f.Sifre,
                //    Personel_Ref = f.Personel_Ref
                //})
                //.ToList();
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> SinifPersonelGetir(int id)
        {
            List<PersonelBilgi> dataBilgi = new List<PersonelBilgi>();
            List<Kullanici> ddp = await (from dx in Entity.Kullanicis
                                         orderby dx.Personel.Adi
                                         where dx.IsActive == true && dx.IsDelete == false &&
                                              dx.Personel.Sinif_Ogrenci.Where(q => q.Sinif_Ref == id).Count() > 0
                                         select dx).ToListAsync();
            for (int i = 0; i < ddp.Count; i++)
            {
                PersonelBilgi psr = new PersonelBilgi();
                psr.ID = ddp[i].ID;
                var perref = ddp[i].Personel_Ref;
                psr.Sinif = await Entity.Sinif_Ogrenci.Where(r => r.Personel_Ref == perref).Select(e => e.Sinif.SinifAdi).ToListAsync();
                psr.Adi = ddp[i].Personel.Adi;
                psr.Soyadi = ddp[i].Personel.Soyadi;
                psr.Kullaniciadi = ddp[i].KullaniciAdi;
                psr.BaslangicTarihi = ddp[i].BaslangicTarihi;
                psr.BitisTarihi = ddp[i].BitisTarihi.Value;
                psr.Sifre = ddp[i].Sifre;
                psr.Personel_Ref = ddp[i].Personel_Ref;
                psr.Resim = ddp[i].Personel.Resim;
                dataBilgi.Add(psr);
            }
            return Json(dataBilgi, "apllication/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> KullaniciKontrol(string KullaniciAdi)
        {
            bool kontrol = await Entity.Kullanicis.Where(t => t.KullaniciAdi == KullaniciAdi && t.IsActive == true && t.IsDelete == false).FirstOrDefaultAsync() == null ? false : true;
            return Json(kontrol, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> KullaniciEkle(string KullaniciAdi, string Sifre, DateTime BaslangicTarihi, DateTime BitisTarihi, int Personel_Ref)
        {
            var kontrol = await Entity.Kullanicis.Where(t => t.KullaniciAdi == KullaniciAdi && t.IsActive == false).FirstOrDefaultAsync();
            Kullanici tip = null;
            if (kontrol != null)
            {
                kontrol.IsActive = true;
                kontrol.IsDelete = false;
                kontrol.BaslangicTarihi = BaslangicTarihi;
                kontrol.BitisTarihi = BitisTarihi;
                kontrol.KullaniciAdi = KullaniciAdi;
                kontrol.Sifre = Sifre;
            }
            else
            {
                tip = new Kullanici();
                tip.IsActive = true;
                tip.IsDelete = false;
                tip.KullaniciAdi = KullaniciAdi;
                tip.Sifre = Sifre;
                tip.BaslangicTarihi = BaslangicTarihi;
                tip.BitisTarihi = BitisTarihi;
                tip.Personel_Ref = Personel_Ref;
                Entity.Kullanicis.Add(tip);
            }
            await Entity.SaveChangesAsync();
            return RedirectToAction("Kullanicilar", "PersonelIslemler", new { area = "Panel" });
        }

        public async Task<JsonResult> KullaniciGuncelle(int ID, string KullaniciAdi, string Sifre, DateTime BaslangicTarihi, DateTime BitisTarihi)
        {
            bool result = false;
            try
            {
                var data = await Entity.Kullanicis.Where(r => r.ID == ID).FirstOrDefaultAsync();
                data.KullaniciAdi = KullaniciAdi;
                data.Sifre = Sifre;
                data.BaslangicTarihi = BaslangicTarihi;
                data.BitisTarihi = BitisTarihi;
                await Entity.SaveChangesAsync();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return Json(result, "apllication/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> KullaniciSil(int ID)
        {
            bool result = false;
            try
            {
                var data = await Entity.Kullanicis.Where(r => r.ID == ID).FirstOrDefaultAsync();
                data.IsActive = false;
                data.IsDelete = true;
                await Entity.SaveChangesAsync();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }
            return Json(result, "apllication/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PersonelRapor(int? idx)
        {
            return View(idx);
        }

    }

    public class KullaniciRollerData
    {
        public int[] User_Role_Name_Ref { get; set; }
        public int[] User_Ref { get; set; }
    }

    public class PersonelListe : IDisposable
    {
        public int ID { get; set; }
        public string Adi { get; set; }
        public string Soyadi { get; set; }
        public string TCNo { get; set; }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public class PersonelBilgi : IDisposable
    {
        public int ID { get; set; }
        public List<string> Sinif { get; set; }
        public string Adi { get; set; }
        public string Soyadi { get; set; }
        public string Kullaniciadi { get; set; }
        public string Sifre { get; set; }
        public DateTime BaslangicTarihi { get; set; }
        public DateTime BitisTarihi { get; set; }
        public int Personel_Ref { get; set; }
        public string Resim { get; set; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
