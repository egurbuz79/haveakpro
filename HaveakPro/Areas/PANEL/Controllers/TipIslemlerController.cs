﻿using HaveakPro.Areas.PANEL.Models;
using HaveakPro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Web.UI;

namespace HaveakPro.Areas.PANEL.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.SINAVLAR_OgrenciGirisi + "," + UserRoleName.EGITIM_OgrenciGirisi + "," + UserRoleName.GUVENLIK_OgrenciGirisi + "," + UserRoleName.ADMIN_BagajTehlikelimadde + "," + UserRoleName.ADMIN_Sinav + "," + UserRoleName.ADMIN_PersonelKullanici + "," + UserRoleName.ADMIN_BagajTehlikelimadde + "," + UserRoleName.ADMIN_Egitim)]
    public class TipIslemlerController : Controller
    {
        //
        // GET: /PANEL/TipIslemler/
        HaveakEntities Entity;
        public TipIslemlerController(HaveakEntities _Entity)
        {
            Entity = _Entity;
        }

        public async Task<ActionResult> Index()
        {
            var data = await Entity.TipSinavlars.Where(r => r.IsActive == true && r.IsDelete == false).ToListAsync();
            return View(data);
        }

        public async Task<JsonResult> TipGuncelle(int id, string TipNo, string SoruSayisi, int Sure, string Aciklama, bool Yayin)
        {
            bool durum = false;
            try
            {
                var data = await Entity.TipSinavlars.Where(y => y.ID == id).FirstOrDefaultAsync();
                if (data != null)
                {
                    data.TipNo = TipNo;
                    data.SoruSayisi = int.Parse(SoruSayisi);
                    data.Sure = Sure;
                    data.Aciklama = Aciklama;
                    data.Yayinda = Yayin;
                    await Entity.SaveChangesAsync();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> TipSilme(int id)
        {
            bool durum = false;
            try
            {
                var durumKontrol = await Entity.TipSinavlars.Where(t => t.IsActive == true && t.IsDelete == false && t.ID == id).FirstOrDefaultAsync();
                if (durumKontrol != null)
                {
                    durumKontrol.IsActive = false;
                    durumKontrol.IsDelete = true;
                    await Entity.SaveChangesAsync();
                    durum = true;
                }
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> TipKaydet(string TipNo, int SoruSayisi, int Sure, string Aciklama, string Yayin)
        {
            var kontrol = await Entity.TipSinavlars.Where(y => y.TipNo == TipNo && y.IsDelete == false && y.IsActive == true).CountAsync();
            if (kontrol == 0)
            {
                TipSinavlar data = new TipSinavlar();
                data.IsActive = true;
                data.IsDelete = false;
                data.TipNo = TipNo;
                data.SoruSayisi = SoruSayisi;
                data.Sure = Sure;
                data.Aciklama = Aciklama;
                data.Yayinda = Yayin == "on" ? true : false;
                Entity.TipSinavlars.Add(data);
                await Entity.SaveChangesAsync();
            }
            return RedirectToAction("Index", "TipIslemler", new { area = "Panel" });
        }

        public async Task<ActionResult> TipSorular(int id)
        {
            //ViewData["Unite"] = Entity.Unites.Where(r => r.IsActive == true).ToList();
            //yukarıda kurs 3 ve kurs 11 ya otomatik seçilecek yada başka bi şekilde çağrılacak!
            ViewBag.TipSorular = await Entity.UniteSorulars.Where(t => t.SinavTipi_Ref == (int)Enumlar.SoruTipleri.TIP && t.IsActive == true && t.IsDelete == false).ToListAsync();
            var data = await Entity.TipSinavlars.Where(t => t.ID == id && t.IsActive == true && t.IsDelete == false).FirstOrDefaultAsync();
            return View(data);
        }

        //public JsonResult TestKursModulUniteler(int id)
        //{
        //    var data = Entity.Unites.Where(t => t.IsActive == true && t.IsDelete == false && t.Modul_Ref == id).Select(r => new { r.ID, r.UniteAdi }).ToList();
        //    return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        //}

        public async Task<JsonResult> TipSoruKaydet(int TipSinavlar_Ref, int UniteSorular_Ref)
        {
            bool durum = false;
            try
            {
                var kontrol = await Entity.Tip_Sorular.Where(r => r.TipSinavlar_Ref == TipSinavlar_Ref && r.UniteSorular_Ref == UniteSorular_Ref && r.IsActive == true && r.IsDelete == false).FirstOrDefaultAsync();
                if (kontrol == null)
                {
                    var data = new Tip_Sorular();
                    data.IsActive = true;
                    data.IsDelete = false;
                    data.TipSinavlar_Ref = TipSinavlar_Ref;
                    data.UniteSorular_Ref = UniteSorular_Ref;
                    Entity.Tip_Sorular.Add(data);
                    await Entity.SaveChangesAsync();
                }
                else
                {
                    kontrol.IsActive = true;
                    kontrol.IsDelete = false;
                    await Entity.SaveChangesAsync();
                }
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }


        public async Task<JsonResult> TanimliSoruGuncelleTip(int id)
        {
            bool durum = false;
            try
            {
                var data = await Entity.Tip_Sorular.Where(t => t.ID == id).FirstOrDefaultAsync();
                data.IsActive = false;
                data.IsDelete = true;
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }


    }
}
