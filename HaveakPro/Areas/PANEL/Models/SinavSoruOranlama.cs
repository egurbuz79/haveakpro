﻿using HaveakPro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HaveakPro.Areas.PANEL.Models
{
    public class SinavSoruOranlama
    {
        HaveakEntities db;
        public SinavSoruOranlama(HaveakEntities _db)
        {
            db = _db;
        }

        public SoruOranAdet SoruOranAdetGetir(int soruId)
        {
            SoruOranAdet oranadet = new SoruOranAdet();
            decimal oran = 0;
            IQueryable<SinavCevaplar> girilencevaplar = db.SinavCevaplars.Where(r => r.Sinav_Sorular.UniteSorular_Ref == soruId);
            oranadet.adet = db.Sinav_Sorular.Where(t => t.UniteSorular_Ref == soruId && t.IsActive == true && t.IsDelete == false).Select(t => t.ID).Count();
            decimal toplamCevapAdet = girilencevaplar.Count();
            decimal dogruCevaplar = girilencevaplar.Where(w => w.Netice == true).Count();
            try
            {
                oran = (dogruCevaplar / toplamCevapAdet) * 100;
            }
            catch
            {
                oran = 0;
            }
            oranadet.oran = Math.Round(oran, 2);
            return oranadet;
        }      

    }

    public class SoruOranAdet
    {
        public int adet { get; set; }
        public decimal oran { get; set; }
    }

}