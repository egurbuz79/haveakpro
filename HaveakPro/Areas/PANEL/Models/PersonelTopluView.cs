﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HaveakPro.Areas.PANEL.Models
{
    [Serializable]
    public class PersonelTopluView
    {
        public string TC_Kimlik_No { get; set; }
        public string Adi { get; set; }
        public string Soyadi { get; set; }
        public string Calistigi_Kurum { get; set; }
        public string Ev_Adresi { get; set; }
        public string Cep_Tel { get; set; }
        public string EPosta { get; set; }
        public string Sehir { get; set; }
        public string Calistigi_Birimler { get; set; }
    }
}