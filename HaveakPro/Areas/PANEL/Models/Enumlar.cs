﻿
namespace HaveakPro.Areas.PANEL.Models
{
    public class Enumlar
    {
        public enum SoruTipleri
        {
            UYGULAMA = 1,
            TEORIK = 2,
            TEST = 3,
            TIP = 4,
            TIP_EGITIM = 9
        }

        public enum SinavTipi
        {
            Hazırlik_Sinavi = 1,
            Sertifika_Sinavi = 2
        }

    }

    
}