﻿using HaveakPro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HaveakPro.Areas.PANEL.Models
{
    public class UniteSorularPage:IDisposable
    {
        public decimal bulunmaOran { get; set; }
        public int bulunmaAdet { get; set; }
        public UniteSorular UniteSoru { get; set; }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}