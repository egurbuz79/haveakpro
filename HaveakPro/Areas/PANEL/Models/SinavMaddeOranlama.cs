﻿using HaveakPro.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace HaveakPro.Areas.PANEL.Models
{
    public class SinavMaddeOranlama
    {
        HaveakEntities db;
        public SinavMaddeOranlama(HaveakEntities _db)
        {
            db = _db;
        }

        public decimal MaddeOranGetir(int resimID)
        {
            decimal oran = 0;

            IQueryable<SinavCevaplar> girilencevaplar = db.SinavCevaplars.Where(r => r.Sinav_Sorular.UniteSorular.STM_Ref == resimID);
            decimal toplamCevapAdet = girilencevaplar.Count();
            decimal dogruCevaplar = girilencevaplar.Where(w => w.Netice == true).Count();
            try
            {
                oran = (dogruCevaplar / toplamCevapAdet) * 100;
            }
            catch
            {
                oran = 0;
            }
            return Math.Round(oran, 2);
        }

    }
}