﻿using System.Collections.Generic;

namespace HaveakPro.Areas.PANEL.Models
{
    public class Dictionary
    {       

        public Dictionary<int, string> DuyuruTipi()
        {
            Dictionary<int, string> list = new Dictionary<int, string>();
            list.Add(1, "Genel Duyuru");
            list.Add(2, "Sınav Duyuru");       
            return list;
        }

        public Dictionary<string, string> RolAdi()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("PANEL/AdminGirisi", "ADMİN");
            list.Add("ADMIN/BagajTehlikelimadde", " - Bagaj Madde");
            list.Add("ADMIN/Egitim", " - Eğitim");
            list.Add("ADMIN/Sinav", " - Sınav İşlem");
            list.Add("ADMIN/PersonelKullanici", " - Personel Kullanıcı");
            list.Add("GUVENLIK/OgrenciGirisi", "GÜVENLİK");
            list.Add("EGITIM/OgrenciGirisi", "EĞİTİM");
            list.Add("SINAVLAR/OgrenciGirisi", "SINAV");
            return list;
        }

        public Dictionary<int, string> SoruKategori()
        {
            Dictionary<int, string> list = new Dictionary<int, string>();
            list.Add(0, "Soru Kategorisi Seçiniz");
            list.Add(1, "UYGULAMA");
            list.Add(2, "TEORİK");
            return list;
        }

        public Dictionary<int, string> DuyuruOncelik()
        {
            Dictionary<int, string> list = new Dictionary<int, string>();
            list.Add(1, "Normal");
            list.Add(2, "Önemli");
            list.Add(3, "Acil");
            return list;
        } 

    }
}