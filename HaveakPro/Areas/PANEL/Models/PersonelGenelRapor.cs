﻿using HaveakPro.Models;
using System;
using System.Collections.Generic;
using System.Linq;



namespace HaveakPro.Areas.PANEL.Models
{
    public class PersonelGenelRapor
    {
        public int perID { get; set; }
        public string TcNo { get; set; }
        public string AdSoyad { get; set; }
        public int? SinifAdet { get; set; }
        public int? GisSinavAdet { get; set; }
        public int? H_SinavAdet { get; set; }
        public int? S_SinavAdet { get; set; }
        public decimal? GisOrtalama { get; set; }
        public decimal? H_SinavUygOrtalama { get; set; }
        public decimal? S_SinavUygOrtalama { get; set; }
        public decimal? H_SinavTeoOrtalama { get; set; }
        public decimal? S_SinavTeoOrtalama { get; set; }

        public decimal? deliciYuzde { get; set; }
        public decimal? silahYuzde { get; set; }
        public decimal? duzenekYuzde { get; set; }
        public decimal? kargoYuzde { get; set; }
        public decimal? digerleriYuzde { get; set; }
    }

    public class GenelRaporlama
    {
        HaveakEntities _db;
        public GenelRaporlama(HaveakEntities db)
        {
            _db = db;
        }
        public List<PersonelGenelRapor> GenelDataCikti()
        {
            try
            {
                List<PersonelGenelRapor> datas = new List<PersonelGenelRapor>();
                var data = _db.sp_GenelPersonelDurumDegerlendirme();
                foreach (var item in data)
                {
                    PersonelGenelRapor per = new PersonelGenelRapor();
                    per.perID = item.ID;
                    per.AdSoyad = item.Adi + " " + item.Soyadi;
                    per.TcNo = item.TCNo;
                    per.SinifAdet = item.SinifAdet ?? 0;
                    per.GisSinavAdet = item.GisAdet ?? 0;
                    per.H_SinavAdet = item.PersonelSinavHazirlik ?? 0;
                    per.S_SinavAdet = item.PersonelSinavSertifika ?? 0;
                    per.GisOrtalama = item.GisNotOrtalama ?? 0;
                    per.H_SinavTeoOrtalama = item.HazirlikTeoOrtalama ?? 0;
                    per.H_SinavUygOrtalama = item.HazirlikUygOrtalama ?? 0;
                    per.S_SinavTeoOrtalama = item.SertifikaTeoOrtalama ?? 0;
                    per.S_SinavUygOrtalama = item.SertifikaUygOrtalama ?? 0;
                    per.deliciYuzde = item.Kesici_Delici ?? 0;
                    per.silahYuzde = item.Silah ?? 0;
                    per.duzenekYuzde = item.Duzenek ?? 0;
                    // per.kargoYuzde = item.Kargo_EDS;
                    per.digerleriYuzde = item.Digerleri ?? 0;
                    datas.Add(per);
                }
                return datas;
            }
            catch
            {
                return new List<PersonelGenelRapor>();
            }

        }

        public List<DuzenekOranlarView> SinavMaddeOranlar(int prs_Id)
        {

            List<DuzenekOranlarView> oranlar = new List<DuzenekOranlarView>();

            #region Uygulama Sınav ORanlar

            Nullable<int> genelSinavSilah = _db.PersonelToplamSinavSoruMaddeDagilim(prs_Id).Where(w => w.Isim == "Silah").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
            Nullable<int> genelSinavSilahDogru = _db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(prs_Id).Where(r => r.Isim == "Silah").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
            double oran = 0;
            try
            {
                oran = ((Convert.ToDouble(genelSinavSilahDogru) / Convert.ToDouble(genelSinavSilah)) * 100);
                if (double.IsNaN(oran))
                {
                    oran = 0;
                }
                oran = Math.Round(oran, 2);
            }
            catch
            {
                oran = 0;
            }
            oranlar.Add(new DuzenekOranlarView { madde = "Silah", oran = oran, sorusayisi = genelSinavSilah.Value, dogrusayisi = genelSinavSilahDogru.Value, sinavturu = "UYGULAMA Sınavlar" });

            Nullable<int> genelSinavDuzenek = _db.PersonelToplamSinavSoruMaddeDagilim(prs_Id).Where(w => w.Isim == "Düzenek").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
            Nullable<int> genelSinavDuzenekDogru = _db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(prs_Id).Where(r => r.Isim == "Düzenek").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
            double oranDuz = 0;
            try
            {
                oranDuz = ((Convert.ToDouble(genelSinavDuzenekDogru) / Convert.ToDouble(genelSinavDuzenek)) * 100);
                if (double.IsNaN(oranDuz))
                {
                    oranDuz = 0;
                }
                oranDuz = Math.Round(oranDuz, 2);
            }
            catch
            {
                oranDuz = 0;
            }
            oranlar.Add(new DuzenekOranlarView { madde = "Düzenek", oran = oranDuz, sorusayisi = genelSinavDuzenek.Value, dogrusayisi = genelSinavDuzenekDogru.Value, sinavturu = " " });

            Nullable<int> genelSinavKesici = _db.PersonelToplamSinavSoruMaddeDagilim(prs_Id).Where(w => w.Isim == "Kesici-Delici").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
            Nullable<int> genelSinavKesiciDogru = _db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(prs_Id).Where(r => r.Isim == "Kesici-Delici").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
            double oranKes = 0;
            try
            {
                oranKes = ((Convert.ToDouble(genelSinavKesiciDogru) / Convert.ToDouble(genelSinavKesici)) * 100);
                if (double.IsNaN(oranKes))
                {
                    oranKes = 0;
                }
                oranKes = Math.Round(oranKes, 2);
            }
            catch
            {
                oranKes = 0;
            }
            oranlar.Add(new DuzenekOranlarView { madde = "Kesici-Delici", oran = oranKes, sorusayisi = genelSinavKesici.Value, dogrusayisi = genelSinavKesiciDogru.Value, sinavturu = " " });

            Nullable<int> genelSinavKargo = _db.PersonelToplamSinavSoruMaddeDagilim(prs_Id).Where(w => w.Isim == "Düzenek (Kargo-EDS)").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
            Nullable<int> genelSinavKargoDogru = _db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(prs_Id).Where(r => r.Isim == "Düzenek (Kargo-EDS)").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
            double oranKar = 0;
            try
            {
                oranKar = ((Convert.ToDouble(genelSinavKargoDogru) / Convert.ToDouble(genelSinavKargo)) * 100);
                if (double.IsNaN(oranKar))
                {
                    oranKar = 0;
                }
                oranKar = Math.Round(oranKar, 2);
            }
            catch
            {
                oranKar = 0;
            }
            oranlar.Add(new DuzenekOranlarView { madde = "Düzenek (Kargo-EDS)", oran = oranKar, sorusayisi = genelSinavKargo.Value, dogrusayisi = genelSinavKargoDogru.Value, sinavturu = " " });

            Nullable<int> genelSinavDiger = _db.PersonelToplamSinavSoruMaddeDagilim(prs_Id).Where(w => w.Isim == "Diğerleri").Select(q => q.ToplamSoru).FirstOrDefault() ?? 0;
            Nullable<int> genelSinavDigerDogru = _db.PersonelToplamSinavSoruMaddeDagilimDogruAdet(prs_Id).Where(r => r.Isim == "Diğerleri").Select(q => q.DogruCevap).FirstOrDefault() ?? 0;
            double oranDig = 0;
            try
            {
                oranDig = ((Convert.ToDouble(genelSinavDigerDogru) / Convert.ToDouble(genelSinavDiger)) * 100);
                if (double.IsNaN(oranDig))
                {
                    oranDig = 0;
                }
                oranDig = Math.Round(oranDig, 2);
            }
            catch
            {
                oranDig = 0;
            }
            oranlar.Add(new DuzenekOranlarView { madde = "Diğerleri", oran = oranDig, sorusayisi = genelSinavDiger.Value, dogrusayisi = genelSinavDigerDogru.Value, sinavturu = " " });
            #endregion
            return oranlar;

        }
    }
}