﻿using System.Web.Mvc;

namespace HaveakPro.Areas.SINAV
{
    public class SINAVAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "SINAV";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "SINAV_default",
                "SINAV/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}