﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HaveakPro.Models;
using HaveakPro.Areas.SINAV.Models;
using System.Security.Claims;
using HaveakPro.Models.Interface;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.UI;

namespace HaveakPro.Areas.SINAV.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize]
    public class HomeController : Controller
    {

        HaveakEntities Entity;
        I_UserModel user;
        public HomeController(HaveakEntities _Entity, I_UserModel _user)
        {
            Entity = _Entity;
            user = _user;
        }

        #region Sınav İşlemler
        public async Task<ActionResult> Index()
        {
            var userId = user.getAutorizeUser().Id;
            ViewBag.Duyurular = await Entity.Duyurulars.Where(t => t.IsDelete == false && t.IsActive == true && t.Tipi == 2).ToListAsync();
            ViewBag.SinavSonucu = await Entity.Personel_Sinavlar.Where(r => r.Bitti == 1 && r.Durum != "AÇIKLANMADI").ToListAsync();
            var acikKalanSinav = await Entity.Personel_Sinavlar.Where(r => r.IsActive == true && r.Sinif_Ogrenci.Personel_Ref == userId && r.Bitti == 0).ToListAsync();
            foreach (var item in acikKalanSinav)
            {
                //DateTime pTarih = item.SinavTarihi.Value;
                //TimeSpan girisSaati = item.GirisSaati.HasValue == false ? TimeSpan.Zero : item.GirisSaati.Value;
                //DateTime girisZaman = pTarih.Add(girisSaati);
                //DateTime bitisGerekenSure = girisZaman.Add(TimeSpan.FromMinutes(item.SinavSuresi.Value));

                DateTime tarih = item.SinavTarihi.Value;
                TimeSpan bitisSaati = item.BitisSaati.Value;
                DateTime bugun = DateTime.Now.Date;
                TimeSpan aktifSaat = TimeSpan.Parse(DateTime.Now.ToShortTimeString());
                if (tarih <= bugun && bitisSaati < aktifSaat)
                {
                    var sinavBitir = await Entity.Personel_Sinavlar.Where(t => t.ID == item.ID).FirstOrDefaultAsync();
                    sinavBitir.Bitti = 1;
                    sinavBitir.CikisSaati = bitisSaati;
                    await Entity.SaveChangesAsync();
                }
            }
            ViewBag.Kullanici = user.getAutorizeUser();
            return View();
        }

        public async Task<ActionResult> _SSoruDetay(int Sinav_Ref, int idx, int SoruNo)
        {
            int userId = user.getAutorizeUser().Id;
            Sinav_Sorular data = await (from d in Entity.Sinav_Sorular
                                        where
                                            d.Sinavlar_Ref == Sinav_Ref &&
                                            d.ID == idx &&
                                            d.Sinavlar.Personel_Sinavlar.Where(r => r.Sinif_Ogrenci.Personel_Ref == userId).Select(t => t.Sinavlar_Ref).FirstOrDefault() == Sinav_Ref
                                        select d).FirstOrDefaultAsync();
            //Sinav_Sorular data = Entity.Sinav_Sorular.Where(t => t.Sinavlar_Ref == Sinav_Ref && t.ID == idx).First();
            ViewBag.userId = user.getAutorizeUser().Id;
            ViewBag.Soruno = SoruNo;
            return PartialView("_SoruIcerik", data);
        }

        public async Task<ActionResult> _SinavKontrolBir()
        {
            int userId = user.getAutorizeUser().Id;
            List<Personel_Sinavlar> data = await Entity.Personel_Sinavlar.Where(t => t.IsActive == true && t.IsDelete == false && t.Sinif_Ogrenci.Personel_Ref == userId).ToListAsync();
            return PartialView("_SinavKontrol", data);
        }

        public async Task<ActionResult> _SinavDuyuruBir()
        {
            int userId = user.getAutorizeUser().Id;
            List<Personel_Sinavlar> data = await Entity.Personel_Sinavlar.OrderByDescending(w => w.ID).Where(t => t.IsActive == true && t.IsDelete == false && t.Sinif_Ogrenci.Personel_Ref == userId).ToListAsync();
            return PartialView("_SinavDuyurular", data);
        }

        public async Task<ActionResult> _SinavCevaplar(int id)
        {
            var pcevap = await Entity.SinavCevaplars.Where(y => y.Personel_Sinavlar_Ref == id).ToListAsync();
            return PartialView("_SinavDetay", pcevap);
        }

        public async Task<JsonResult> AltSiklarGetir(int id)
        {
            var data = await Entity.AltSiklars.Where(t => t.IsActive == true && t.UniteSorular_Ref == id).Select(r => new { r.ID, r.AltSoruSikki, r.AltCevapIcerik }).ToListAsync();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }


        public async Task<JsonResult> SinavCevap(int Personel_SinavRef, int SinavSoruRef, int? Cevap, int? AltCevap, int? xkordinat, int? ykordinat, bool? koordinatSecim)
        {
            string durum = "";
            try
            {
                var control = await Entity.SinavCevaplars.Where(t => t.Personel_Sinavlar_Ref == Personel_SinavRef && t.Sınav_Sorular_Ref == SinavSoruRef).FirstOrDefaultAsync();
                if (control != null)
                {
                    control.Personel_Sinavlar_Ref = Personel_SinavRef;
                    control.Sınav_Sorular_Ref = SinavSoruRef;
                    control.GirilenCevap = Cevap;
                    control.AltCevap = AltCevap;
                    control.CevapResimX = xkordinat;
                    control.CevapResimY = ykordinat;
                    control.DogruCevap = Entity.SoruSiklaris.Where(r => r.ID == Cevap).Select(e => e.DogruSecenek).FirstOrDefault();
                    control.DogruAltCevap = Entity.AltSiklars.Where(y => y.ID == AltCevap).Select(r => r.AltDogruSecenek).FirstOrDefault();
                    control.KoordinatSecim = koordinatSecim;
                    //var koordinatlar = Entity.UniteSorulars.Where(t => t.ID == control.Sinav_Sorular.UniteSorular_Ref).Select(e => new { e.XKordinat, e.YKordinat }).FirstOrDefault();
                    //if (koordinatlar.XKordinat != null)
                    //{
                    //    //int xbas = koordinatlar.XBaslangic.Value;
                    //    //int xbit = koordinatlar.XBitis.Value;
                    //    //int ybas = koordinatlar.YBaslangic.Value;
                    //    //int ybit = koordinatlar.YBitis.Value;

                    //    //if ((xbas <= xkordinat.Value && xbit >= xkordinat) && (ybas <= ykordinat && ybit >= ykordinat))
                    //    //{
                    //    //    control.KoordinatSecim = true;
                    //    //}
                    //    //else
                    //    //{
                    //    //    control.KoordinatSecim = false;
                    //    //}
                    //}
                    var unite = await Entity.Sinav_Sorular.Where(r => r.ID == SinavSoruRef).Select(n => new { n.UniteSorular_Ref, n.SinavTipi_Ref }).FirstOrDefaultAsync();
                    int altsik = await Entity.AltSiklars.Where(t => t.UniteSorular_Ref == unite.UniteSorular_Ref).CountAsync();
                    decimal? resim = await Entity.UniteSorulars.Where(t => t.ID == unite.UniteSorular_Ref).Select(u => u.XKordinat).FirstOrDefaultAsync();

                    bool sonuc = false;
                    if (altsik != 0 && resim != null && unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Uygulama)
                    {
                        if (control.DogruCevap == true && control.DogruAltCevap == true && control.KoordinatSecim == true)
                        {
                            sonuc = true;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                    else if (altsik == 0 && unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Uygulama)
                    {
                        var secenek = await Entity.SoruSiklaris.Where(r => r.ID == Cevap && r.DogruSecenek == true).Select(e => e.SoruSikki).FirstOrDefaultAsync();
                        if (secenek == "a)")
                        {
                            if (control.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }
                        }
                        else
                        {

                            if (control.DogruCevap == true && control.KoordinatSecim == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }

                        }
                    }
                    else if (unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Teorik)
                    {
                        if (control.DogruCevap == true)
                        {
                            sonuc = true;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                    control.Netice = sonuc;
                    Entity.SaveChanges();
                    durum = "Guncellendi";
                }
                else
                {
                    int unite_Ref = await Entity.Sinav_Sorular.Where(t => t.ID == SinavSoruRef).Select(r => r.UniteSorular_Ref).FirstOrDefaultAsync();
                    var data = new SinavCevaplar();
                    data.Personel_Sinavlar_Ref = Personel_SinavRef;
                    data.Sınav_Sorular_Ref = SinavSoruRef;
                    data.GirilenCevap = Cevap;
                    data.AltCevap = AltCevap;
                    data.CevapResimX = xkordinat;
                    data.CevapResimY = ykordinat;
                    data.DogruCevap = await Entity.SoruSiklaris.Where(r => r.ID == Cevap).Select(e => e.DogruSecenek).FirstOrDefaultAsync();
                    data.DogruAltCevap = await Entity.AltSiklars.Where(y => y.ID == AltCevap).Select(r => r.AltDogruSecenek).FirstOrDefaultAsync();
                    data.KoordinatSecim = koordinatSecim;
                    //try
                    //{
                    //var koordinatlar = Entity.UniteSorulars.Where(t => t.ID == unite_Ref).Select(e => new { e.XKordinat, e.YKordinat }).FirstOrDefault();
                    //if (koordinatlar.XKordinat != null)
                    //{
                    //int xbas = koordinatlar.XBaslangic.Value;
                    //int xbit = koordinatlar.XBitis.Value;
                    //int ybas = koordinatlar.YBaslangic.Value;
                    //int ybit = koordinatlar.YBitis.Value;

                    //if ((xbas <= xkordinat.Value && xbit >= xkordinat) && (ybas <= ykordinat && ybit >= ykordinat))
                    //{
                    //    data.KoordinatSecim = true;
                    //}
                    //else
                    //{
                    //    data.KoordinatSecim = false;
                    //}
                    //    }
                    //}
                    //catch
                    //{
                    //}
                    var unite = await Entity.Sinav_Sorular.Where(r => r.ID == SinavSoruRef).Select(n => new { n.UniteSorular_Ref, n.SinavTipi_Ref }).FirstOrDefaultAsync();
                    int altsik = await Entity.AltSiklars.Where(t => t.UniteSorular_Ref == unite.UniteSorular_Ref).CountAsync();
                    int? resim = await Entity.UniteSorulars.Where(t => t.ID == unite.UniteSorular_Ref).Select(u => u.SBagajlar_Ref).FirstOrDefaultAsync();

                    bool sonuc = false;
                    if (altsik != 0 && resim != null && unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Uygulama)
                    {
                        if (data.DogruCevap == true && data.DogruAltCevap == true && data.KoordinatSecim == true)
                        {
                            sonuc = true;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                    else if (altsik == 0 && unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Uygulama)
                    {
                        var secenek = await Entity.SoruSiklaris.Where(r => r.ID == Cevap && r.DogruSecenek == true).Select(e => e.SoruSikki).FirstOrDefaultAsync();
                        if (secenek == "a)")
                        {
                            if (data.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }
                        }
                        else
                        {

                            if (data.DogruCevap == true && data.KoordinatSecim == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }

                        }
                    }
                    else if (unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Teorik)
                    {
                        if (data.DogruCevap == true)
                        {
                            sonuc = true;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                    data.Netice = sonuc;
                    Entity.SinavCevaplars.Add(data);
                    await Entity.SaveChangesAsync();

                    durum = "Tamam";
                }

            }
            catch
            {
                durum = "İşlem sırasında hata meydana geldi tekrar deneyiniz!";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> SinavaBasla(int id)
        {
            var users = user.getAutorizeUser();
            List<Sinav_Sorular> data = null;
            Personel_Sinavlar aktifSinav = await Entity.Personel_Sinavlar.Where(t => t.ID == id).FirstOrDefaultAsync();
            if (aktifSinav != null)
            {
                ViewBag.AktifSinav = aktifSinav.Sinavlar_Ref;
                ViewBag.PerSinavID = id;
                ViewBag.userId = users.Id;
                data = await Entity.Sinav_Sorular.Where(r => r.Sinavlar_Ref == aktifSinav.Sinavlar_Ref && r.IsActive == true).ToListAsync();
                ViewBag.zamanlar = await Entity.Personel_Sinavlar.Where(t => t.Sinif_Ogrenci.Personel_Ref == users.Id && t.ID == id).FirstOrDefaultAsync();
                ViewBag.personel_sinav =await Entity.Personel_Sinavlar.Where(r => r.Sinif_Ogrenci.Personel_Ref == users.Id && r.Sinavlar_Ref == aktifSinav.Sinavlar_Ref).Select(t => t.ID).FirstOrDefaultAsync();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            return View(data);
        }

        public async Task<JsonResult> SinavStart(int id)
        {
            string durum = "";
            Personel_Sinavlar aktifSinav = await Entity.Personel_Sinavlar.Where(t => t.ID == id).FirstOrDefaultAsync();
            if (aktifSinav.Basladi == 0)
            {
                aktifSinav.Basladi = 1;
                aktifSinav.GirisSaati = TimeSpan.Parse(DateTime.Now.ToLongTimeString());
                await Entity.SaveChangesAsync();
                durum = "tamam";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SinavBitir(int aktifSinavID)
        {
            string durum = "";
            try
            {
                Personel_Sinavlar aktifSinav = await Entity.Personel_Sinavlar.Where(t => t.ID == aktifSinavID).FirstOrDefaultAsync();
                aktifSinav.Bitti = 1;
                aktifSinav.CikisSaati = TimeSpan.Parse(DateTime.Now.ToLongTimeString());
                await Entity.SaveChangesAsync();
                durum = "tamam";
            }
            catch
            {
                durum = "hata";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Test İşlemler
        public async Task<ActionResult> TesteBasla(int id)
        {
            int userId = user.getAutorizeUser().Id;
            List<Test_Sorular> data = await Entity.Test_Sorular.Where(r => r.Testler_Ref == id && r.IsActive == true).ToListAsync();
            Personel_Testler aktifTest = await Entity.Personel_Testler.Where(t => t.Sinif_Ogrenci.Personel_Ref == userId && t.Testler_Ref == id && t.Bitti == 0).FirstOrDefaultAsync();
            int prsID = 0;
            int aktifTst = 0;
            if (aktifTest != null)
            {
                aktifTst = aktifTest.Testler_Ref;
                prsID = aktifTest.ID;
            }
            ViewBag.AktifTest = aktifTst;
            ViewBag.PerTestID = prsID;
            ViewBag.users = user.getAutorizeUser();
            //else
            //{
            //    return RedirectToAction("Index", "Home");
            //}

            return View(data);
        }

        public async Task<ActionResult> _TSoruDetay(int Test_Ref, int idx, int SoruNo)
        {
            int userId = user.getAutorizeUser().Id;
            Test_Sorular data = await (from d in Entity.Test_Sorular
                                 where
                                     d.Testler_Ref == Test_Ref &&
                                     d.ID == idx &&
                                     d.Testler.Personel_Testler.Where(r => r.Sinif_Ogrenci.Personel_Ref == userId).Select(t => t.Testler_Ref).FirstOrDefault() == Test_Ref
                                       select d).FirstOrDefaultAsync();
            //Sinav_Sorular data = Entity.Sinav_Sorular.Where(t => t.Sinavlar_Ref == Sinav_Ref && t.ID == idx).First();

            ViewBag.Soruno = SoruNo;
            ViewBag.TestRef = Test_Ref;
            ViewBag.userId = user.getAutorizeUser().Id;
            return PartialView("_SoruIcerikTest", data);
        }

        public async Task<ActionResult> _TestCevaplar(int id)
        {
            var pcevap = await Entity.TestCevaplars.Where(y => y.Personel_Testler_Ref == id).ToListAsync();
            return PartialView("_TestDetay", pcevap);
        }

        public async Task<JsonResult> TestStart(int id, int sinif_Ogrenci_ref)
        {
            string durum = "";
            Personel_Testler aktifTest = new Personel_Testler();
            aktifTest.IsActive = true;
            aktifTest.IsDelete = false;
            aktifTest.Sinif_Ogrenci_Ref = sinif_Ogrenci_ref;
            aktifTest.Testler_Ref = id;
            aktifTest.TestAciklama = false;
            aktifTest.TestNotu = 0;
            aktifTest.Durum = "AÇIKLANMADI";
            aktifTest.DogruSayisi = 0;
            aktifTest.YanlisSayisi = 0;
            aktifTest.Bos = 0;
            aktifTest.TestTarihi = DateTime.Now.Date;
            aktifTest.GirisSaati = TimeSpan.Parse(DateTime.Now.ToLongTimeString());
            aktifTest.Basladi = 1;
            aktifTest.Bitti = 0;
            Entity.Personel_Testler.Add(aktifTest);
            await Entity.SaveChangesAsync();
            durum = "tamam";

            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> TestBitir(int aktifTestID)
        {
            string durum = "";
            try
            {
                Personel_Testler aktifTest = await Entity.Personel_Testler.Where(t => t.ID == aktifTestID).FirstOrDefaultAsync();
                aktifTest.Bitti = 1;
                aktifTest.CikisSaati = TimeSpan.Parse(DateTime.Now.ToLongTimeString());
                TimeSpan dakika = aktifTest.CikisSaati.Value - aktifTest.GirisSaati.Value;
                aktifTest.TestSuresi = dakika.Minutes == 0 ? 1 : dakika.Minutes;
                Entity.SaveChanges();
                bool sonucu = await TestSonuclandirma(aktifTest.ID, aktifTest.Testler_Ref);
                if (sonucu)
                {
                    durum = "tamam";
                }
            }
            catch
            {
                durum = "hata";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }


        public async Task<bool> TestSonuclandirma(int id, int testRef)
        {
            bool durum = false;
            try
            {
                List<TestSonuclari> cevaplar = new List<TestSonuclari>();
                var testSorular = await Entity.Test_Sorular.Where(r => r.Testler_Ref == testRef && r.IsActive == true && r.IsDelete == false).ToListAsync();
                foreach (var item2 in testSorular)
                {
                    var cevapkontrol = item2.TestCevaplars.Where(r => r.Personel_Testler_Ref == id).Select(t => new { t.Netice, t.Test_Sorular_Ref }).FirstOrDefault();
                    string cevabi = "";
                    if (cevapkontrol == null)
                    {
                        cevabi = "YOK";
                    }
                    else
                    {
                        if (cevapkontrol.Netice == true)
                        {
                            cevabi = "True";
                        }
                        else
                        {
                            cevabi = "False";
                        }
                    }
                    TestSonuclari datax = new TestSonuclari();
                    datax.PerID = id;
                    datax.SoruID = item2.ID;
                    datax.Cevap = cevabi;
                    cevaplar.Add(datax);
                }

                int DogruSayisi = cevaplar.Where(t => t.Cevap == "True").Count();
                int YanlisSayisi = cevaplar.Where(t => t.Cevap == "False").Count();
                int Bos = cevaplar.Where(t => t.Cevap == "YOK").Count();

                decimal TamPuan = 100;

                int ToplamSoru = testSorular.Count();

                decimal soruPuan = 0;
                if (ToplamSoru != 0)
                {
                    soruPuan = TamPuan / ToplamSoru;
                }

                decimal TestNotu = soruPuan * DogruSayisi;

                string Netice = "";
                if (TestNotu >= 70)
                {
                    Netice = "GEÇTİ";
                }
                else
                {
                    Netice = "KALDI";
                }

                var perTestIsleme = await Entity.Personel_Testler.Where(t => t.ID == id).FirstOrDefaultAsync();
                perTestIsleme.DogruSayisi = DogruSayisi;
                perTestIsleme.YanlisSayisi = YanlisSayisi;
                perTestIsleme.TestNotu = TestNotu;
                perTestIsleme.Bos = Bos;
                perTestIsleme.Durum = Netice;
                perTestIsleme.TestAciklama = true;
                await Entity.SaveChangesAsync();
                durum = true;

            }
            catch
            {
                durum = false;
            }
            return durum;
        }

        public async Task<ActionResult> Modul_Testler(int id, int? sonkonu)
        {
            var users = user.getAutorizeUser();
            var testler = await Entity.Testlers.Where(t => t.UniteModul_Ref == id && t.IsActive == true && t.IsDelete == false).ToListAsync();
            ViewBag.Testler = await Entity.Personel_Testler.Where(t => t.Sinif_Ogrenci.Personel_Ref == users.Id && t.Testler.UniteModul_Ref == id).ToListAsync();
            ViewBag.Idx = id;
            List<Personel_Testler> data = await Entity.Personel_Testler.Where(t => t.IsActive == true && t.IsDelete == false && t.Sinif_Ogrenci.Personel_Ref == users.Id).ToListAsync();
            ViewBag.TestCevaplar = data;
            var bittiKontrol = await Entity.Personel_Testler.Where(e => e.Bitti == 1 && e.TestAciklama == false).ToListAsync();
            foreach (var itembt in bittiKontrol)
            {
                if (itembt != null)
                {
                    await TestBitir(itembt.ID);
                }
            }
            ViewBag.Sonkonu = sonkonu ?? 0;
            ViewBag.users = users;
            return View(testler);
        }


        public async Task<JsonResult> TestCevap(int Personel_TestRef, int TestSoruRef, int? Cevap, int? AltCevap)
        {
            string durum = "";
            try
            {
                var control = await Entity.TestCevaplars.Where(t => t.Personel_Testler_Ref == Personel_TestRef && t.Test_Sorular_Ref == TestSoruRef).FirstOrDefaultAsync();
                if (control != null)
                {
                    control.Personel_Testler_Ref = Personel_TestRef;
                    control.Test_Sorular_Ref = TestSoruRef;
                    control.GirilenCevap = Cevap;
                    control.AltCevap = AltCevap;
                    control.DogruCevap = await Entity.SoruSiklaris.Where(r => r.ID == Cevap).Select(e => e.DogruSecenek).FirstOrDefaultAsync();
                    control.DogruAltCevap =await Entity.AltSiklars.Where(y => y.ID == AltCevap).Select(r => r.AltDogruSecenek).FirstOrDefaultAsync();

                    var unite = await Entity.Test_Sorular.Where(r => r.ID == TestSoruRef).Select(n => new { n.UniteSorular_Ref }).FirstOrDefaultAsync();
                    int altsik = await Entity.AltSiklars.Where(t => t.UniteSorular_Ref == unite.UniteSorular_Ref).CountAsync();


                    bool sonuc = false;

                    if (altsik != 0)
                    {
                        if (control.DogruCevap == true && control.DogruAltCevap == true)
                        {
                            sonuc = true;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                    else if (altsik == 0)
                    {
                        var secenek = await Entity.SoruSiklaris.Where(r => r.ID == Cevap && r.DogruSecenek == true).Select(e => e.SoruSikki).FirstOrDefaultAsync();
                        if (secenek == "a)")
                        {
                            if (control.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }
                        }
                        else
                        {

                            if (control.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }
                        }
                    }

                    control.Netice = sonuc;
                    Entity.SaveChanges();
                    durum = "Guncellendi";
                }
                else
                {
                    int unite_Ref = await Entity.Test_Sorular.Where(t => t.ID == TestSoruRef).Select(r => r.UniteSorular_Ref).FirstOrDefaultAsync();
                    var data = new TestCevaplar();
                    data.Personel_Testler_Ref = Personel_TestRef;
                    data.Test_Sorular_Ref = TestSoruRef;
                    data.GirilenCevap = Cevap;
                    data.AltCevap = AltCevap;
                    data.DogruCevap = await Entity.SoruSiklaris.Where(r => r.ID == Cevap).Select(e => e.DogruSecenek).FirstOrDefaultAsync();
                    data.DogruAltCevap = await Entity.AltSiklars.Where(y => y.ID == AltCevap).Select(r => r.AltDogruSecenek).FirstOrDefaultAsync();

                    var unite = await Entity.Test_Sorular.Where(r => r.ID == TestSoruRef).Select(n => new { n.UniteSorular_Ref }).FirstOrDefaultAsync();
                    int altsik = await Entity.AltSiklars.Where(t => t.UniteSorular_Ref == unite.UniteSorular_Ref).CountAsync();

                    bool sonuc = false;
                    if (altsik != 0)
                    {
                        if (data.DogruCevap == true && data.DogruAltCevap == true)
                        {
                            sonuc = true;
                        }
                        else
                        {
                            sonuc = false;
                        }
                    }
                    else if (altsik == 0)
                    {
                        var secenek =await Entity.SoruSiklaris.Where(r => r.ID == Cevap && r.DogruSecenek == true).Select(e => e.SoruSikki).FirstOrDefaultAsync();
                        if (secenek == "a)")
                        {
                            if (data.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }
                        }
                        else
                        {

                            if (data.DogruCevap == true)
                            {
                                sonuc = true;
                            }
                            else
                            {
                                sonuc = false;
                            }

                        }
                    }

                    data.Netice = sonuc;
                    Entity.TestCevaplars.Add(data);
                    await Entity.SaveChangesAsync();

                    durum = "Tamam";
                }

            }
            catch
            {
                durum = "İşlem sırasında hata meydana geldi tekrar deneyiniz!";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        #endregion



    }
    public class TestSonuclari : IDisposable
    {
        public int PerID { get; set; }
        public int SoruID { get; set; }
        public string Cevap { get; set; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public class SoruSiklariLocal : IDisposable
    {

        public int ID { get; set; }
        public string SoruSikki { get; set; }
        public string CevapIcerik { get; set; }
        public bool DogruSecenek { get; set; }


        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
