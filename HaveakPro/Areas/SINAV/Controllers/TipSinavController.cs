﻿using HaveakPro.Models;
using HaveakPro.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Data.Entity;
using HaveakPro.Models.Methods;
using System.Web.UI;

namespace HaveakPro.Areas.SINAV.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize]
    public class TipSinavController : Controller
    {
        //
        // GET: /SINAV/TipSinav/
        HaveakEntities entity;
        I_UserModel user;
        I_UserLog log;
        public TipSinavController(HaveakEntities _entity, I_UserModel _user, I_UserLog _log)
        {
            entity = _entity;
            user = _user;
            log = _log;
        }

        public async Task<ActionResult> Index(bool? tst)
        {
            int userId = user.getAutorizeUser().Id;
            var tiptest = await entity.Personel_TIP.Where(r => r.Personel_Ref == userId && r.IsActive == true && r.IsDelete == false).ToListAsync();
            var data = await entity.TipSinavlars.Where(r => r.IsActive == true && r.IsDelete == false && r.Yayinda == true).ToListAsync();
            ViewBag.aktifKontrol = await entity.Personel_TIP.Where(e => e.Bitti == 0 && e.Personel_Ref == userId && e.IsDelete == false && e.IsActive == true).Select(w => w.TipSinavlar_Ref).FirstOrDefaultAsync();
            ViewBag.PersonelTIP = tiptest;
            ViewBag.TipEgitimTest = await entity.TipEgitimSinavlars.Where(r => r.IsActive == true && r.IsDelete == false && r.Yayinda == true).OrderBy(x => Guid.NewGuid()).ToListAsync();
            foreach (var item in tiptest)
            {
                if (item.Basladi == 1 && item.Bitti == 0)
                {
                    DateTime tipTarih = item.TipTarihi.Value;
                    TimeSpan girisSaati = item.GirisSaati.Value;
                    DateTime girisZaman = tipTarih.Add(girisSaati);
                    DateTime bitisGerekenSure = girisZaman.Add(TimeSpan.FromMinutes(item.TipSinavlar.Sure));
                    DateTime suan = DateTime.Now;
                    if (suan > bitisGerekenSure)
                    {
                        JsonResult drm = await TipTestBitir(item.ID);
                    }
                }
            }
            ViewBag.Test = tst;
            //var girisBilgi = item.Personel_TIP.Where(e => e.Basladi == 1 && e.Bitti == 0).Select(e => new { e.GirisSaati, e.Basladi, e.Bitti }).FirstOrDefault();
            //TimeSpan sure = TimeSpan.FromMinutes(item.Sure);
            //TimeSpan bitisSuresi = girisBilgi == null ? TimeSpan.Zero : girisBilgi.GirisSaati.Value.Add(sure);
            //TimeSpan Suan = DateTime.Now.TimeOfDay;

            //string baslik = "Basla";
            //if (bitisSuresi > Suan && girisBilgi.Basladi == 1 && girisBilgi.Bitti == 0)
            //{
            //    baslik = "Devam";
            //}
            ViewBag.userId = userId;
            return View(data);
        }

        public ActionResult _tipDetay(int id)
        {
            //var data = entity.t
            return PartialView("_TipDetayListe", id);
        }

        public async Task<ActionResult> TipTesteBasla(int id)
        {
            int userId = user.getAutorizeUser().Id;
            List<Tip_Sorular> data = null;
            Personel_TIP aktifSinav = await entity.Personel_TIP.Where(t => t.TipSinavlar_Ref == id && t.Personel_Ref == userId && t.IsActive == true && t.Bitti == 0).FirstOrDefaultAsync();
            ViewBag.Per_TIP = aktifSinav;
            ViewBag.TipSinavID = id;
            ViewBag.userId = user.getAutorizeUser().Id;
            data = await entity.Tip_Sorular.Where(r => r.TipSinavlar_Ref == id && r.IsActive == true).ToListAsync();
            return View(data);
        }

        public async Task<ActionResult> _TSoruDetay(int Tip_Ref, int idx)
        {           
            int sonrakiSoru = 0;
            int index = 0;
            int userId = user.getAutorizeUser().Id;
            ICollection<TipCevaplar> ckontrol = await entity.Personel_TIP.Where(r => r.TipSinavlar_Ref == Tip_Ref && r.Personel_Ref == userId && r.Bitti == 0).Select(e => e.TipCevaplars).FirstOrDefaultAsync();
            if (ckontrol != null)
            {
                int cevapson = ckontrol.OrderByDescending(q => q.ID).Select(r => r.Tip_Sorular_Ref).FirstOrDefault();
                var sorular = (from d in entity.Tip_Sorular
                               where
                                   d.TipSinavlar_Ref == Tip_Ref &&
                                   d.TipSinavlar.Personel_TIP.Where(r => r.Personel_Ref == userId && r.Bitti == 0).Select(t => t.TipSinavlar_Ref).FirstOrDefault() == Tip_Ref
                               select d).ToList();

                Dictionary<int, int> SoruListe = new Dictionary<int, int>();
                foreach (var item in sorular)
                {
                    index++;
                    SoruListe.Add(index, item.ID);
                }
                int soruKey = SoruListe.Where(r => r.Value == cevapson).Select(w => w.Key).FirstOrDefault();
                soruKey++;
                ViewBag.SoruIndex = soruKey;

                sonrakiSoru = SoruListe.Where(q => q.Key == soruKey).Select(t => t.Value).FirstOrDefault();
                idx = sonrakiSoru;
            }
            else
            {
                sonrakiSoru = 0;
                ViewBag.SoruIndex = 0;
                //var sorular = (from d in entity.Tip_Sorular
                //               where
                //                   d.TipSinavlar_Ref == Tip_Ref &&
                //                   d.TipSinavlar.Personel_TIP.Where(r => r.Personel_Ref == userRef && r.Bitti == 0).Select(t => t.TipSinavlar_Ref).FirstOrDefault() == Tip_Ref
                //               select d).ToList();

                //Dictionary<int, int> SoruListe = new Dictionary<int, int>();
                //foreach (var item in sorular)
                //{
                //    index++;
                //    SoruListe.Add(index, item.ID);
                //}
                //int soruKey = SoruListe.Where(r => r.Value == idx).Select(w => w.Key).FirstOrDefault();
                //ViewBag.SoruIndex = soruKey;
                //soruKey++;
                //sonrakiSoru = SoruListe.Where(q => q.Key == soruKey).Select(t => t.Value).FirstOrDefault();
            }
            Tip_Sorular data = await (from d in entity.Tip_Sorular
                                where
                                    d.TipSinavlar_Ref == Tip_Ref &&
                                    d.ID == idx &&
                                    d.TipSinavlar.Personel_TIP.Where(r => r.Personel_Ref == userId && r.Bitti == 0).Select(t => t.TipSinavlar_Ref).FirstOrDefault() == Tip_Ref
                                select d).FirstOrDefaultAsync();

            ViewBag.Soruno = sonrakiSoru;

            ViewBag.TipRef = Tip_Ref;
            ViewBag.entity = entity;
            ViewBag.userId = user.getAutorizeUser().Id;
            return PartialView("_SoruIcerikTip", data);
        }

        public async Task<ActionResult> TipCevaplar(int id, int tipref)
        {
            var tipsorular = await entity.Tip_Sorular.Where(t => t.TipSinavlar_Ref == tipref).ToListAsync();
            var pcevap = await entity.TipCevaplars.Where(y => y.Personel_Tip_Ref == id && y.Tip_Sorular.IsActive== true && y.Tip_Sorular.IsDelete == false).ToListAsync();
            ViewData["tipcevaplar"] = pcevap;
            return View(tipsorular);
        }

        public async Task<JsonResult> TipTestStart(int id, int personel_ref)
        {
            string tipNo = Guid.NewGuid().ToString().Split('-')[0];
            bool durum = false;
            try
            {
                Personel_TIP aktifTest = new Personel_TIP();
                aktifTest.IsActive = true;
                aktifTest.IsDelete = false;
                aktifTest.Personel_Ref = personel_ref;
                aktifTest.TipSinavlar_Ref = id;
                //aktifTest.TipAciklama = false;
                aktifTest.TipNotu = 0;
                aktifTest.TipNo = tipNo.ToUpper();
                aktifTest.DogruSayisi = 0;
                aktifTest.YanlisSayisi = 0;
                //aktifTest.Bos = 0;
                aktifTest.TipTarihi = DateTime.Now.Date;
                aktifTest.GirisSaati = TimeSpan.Parse(DateTime.Now.ToLongTimeString());
                aktifTest.Basladi = 1;
                aktifTest.Bitti = 0;
                entity.Personel_TIP.Add(aktifTest);
                await entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }

            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> TipTestBitir(int aktifTipTestID)
        {
            string durum = "";
            try
            {
                //int user = Authorizing..User_Ref;
                Personel_TIP aktifTest = await entity.Personel_TIP.Where(t => t.ID == aktifTipTestID).FirstOrDefaultAsync();
                DateTime tipTarih = aktifTest.TipTarihi.Value;
                TimeSpan girisSaati = aktifTest.GirisSaati.Value;
                DateTime girisZaman = tipTarih.Add(girisSaati);
                DateTime bitisGerekenSure = girisZaman.Add(TimeSpan.FromMinutes(aktifTest.TipSinavlar.Sure));
                aktifTest.Bitti = 1;
                DateTime suan = DateTime.Now;
                if (suan > bitisGerekenSure)
                {
                    aktifTest.CikisSaati = TimeSpan.Parse(bitisGerekenSure.ToLongTimeString());
                }
                else
                {
                    aktifTest.CikisSaati = TimeSpan.Parse(DateTime.Now.ToLongTimeString());
                }
                TimeSpan dakika = aktifTest.CikisSaati.Value - aktifTest.GirisSaati.Value;
                aktifTest.TipSuresi = dakika.Minutes == 0 ? 1 : dakika.Minutes;
                entity.SaveChanges();
                bool sonucu = await TipSonuclandirma(aktifTest.ID, aktifTest.TipSinavlar_Ref);
                if (sonucu)
                {
                    durum = "tamam";
                }
            }
            catch
            {
                durum = "hata";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> TipSinavCevap(int Personel_TipRef, int TipSoruRef, int? Cevap, int? AltCevap, int? xkordinat, int? ykordinat, bool? koordinatSecim)
        {
            string durum = "";
            try
            {
                //var control = entity.TipCevaplars.Where(t => t.Personel_Tip_Ref == Personel_TipRef && t.Tip_Sorular_Ref == TipSoruRef).FirstOrDefault();
                //if (control != null)
                //{
                //    control.Personel_Tip_Ref = Personel_TipRef;
                //    control.Tip_Sorular_Ref = TipSoruRef;
                //    control.GirilenCevap = Cevap;
                //    //control.AltCevap = AltCevap;
                //    control.CevapResimX = xkordinat;
                //    control.CevapResimY = ykordinat;
                //    control.DogruCevap = entity.SoruSiklaris.Where(r => r.ID == Cevap).Select(e => e.DogruSecenek).FirstOrDefault();
                //    //control.DogruAltCevap = Entity.AltSiklars.Where(y => y.ID == AltCevap).Select(r => r.AltDogruSecenek).FirstOrDefault();
                //    control.KoordinatSecim = koordinatSecim;
                //    //var koordinatlar = Entity.UniteSorulars.Where(t => t.ID == control.Sinav_Sorular.UniteSorular_Ref).Select(e => new { e.XKordinat, e.YKordinat }).FirstOrDefault();
                //    //if (koordinatlar.XKordinat != null)
                //    //{
                //    //    //int xbas = koordinatlar.XBaslangic.Value;
                //    //    //int xbit = koordinatlar.XBitis.Value;
                //    //    //int ybas = koordinatlar.YBaslangic.Value;
                //    //    //int ybit = koordinatlar.YBitis.Value;

                //    //    //if ((xbas <= xkordinat.Value && xbit >= xkordinat) && (ybas <= ykordinat && ybit >= ykordinat))
                //    //    //{
                //    //    //    control.KoordinatSecim = true;
                //    //    //}
                //    //    //else
                //    //    //{
                //    //    //    control.KoordinatSecim = false;
                //    //    //}
                //    //}
                //    var unite = entity.Sinav_Sorular.Where(r => r.ID == SinavSoruRef).Select(n => new { n.UniteSorular_Ref, n.SinavTipi_Ref }).FirstOrDefault();
                //    //int altsik = Entity.AltSiklars.Where(t => t.UniteSorular_Ref == unite.UniteSorular_Ref).Count();
                //    decimal? resim = entity.UniteSorulars.Where(t => t.ID == unite.UniteSorular_Ref).Select(u => u.XKordinat).FirstOrDefault();

                //    bool sonuc = false;
                //    if (altsik != 0 && resim != null && unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Uygulama)
                //    {
                //        if (control.DogruCevap == true && control.DogruAltCevap == true && control.KoordinatSecim == true)
                //        {
                //            sonuc = true;
                //        }
                //        else
                //        {
                //            sonuc = false;
                //        }
                //    }
                //    else if (altsik == 0 && unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Uygulama)
                //    {
                //        var secenek = entity.SoruSiklaris.Where(r => r.ID == Cevap && r.DogruSecenek == true).Select(e => e.SoruSikki).FirstOrDefault();
                //        if (secenek == "a)")
                //        {
                //            if (control.DogruCevap == true)
                //            {
                //                sonuc = true;
                //            }
                //            else
                //            {
                //                sonuc = false;
                //            }
                //        }
                //        else
                //        {

                //            if (control.DogruCevap == true && control.KoordinatSecim == true)
                //            {
                //                sonuc = true;
                //            }
                //            else
                //            {
                //                sonuc = false;
                //            }

                //        }
                //    }
                //    else if (unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Teorik)
                //    {
                //        if (control.DogruCevap == true)
                //        {
                //            sonuc = true;
                //        }
                //        else
                //        {
                //            sonuc = false;
                //        }
                //    }
                //    control.Netice = sonuc;
                //    entity.SaveChanges();
                //    durum = "Guncellendi";
                //}
                //else
                //{
                int unite_Ref = await entity.Tip_Sorular.Where(t => t.ID == TipSoruRef).Select(r => r.UniteSorular_Ref).FirstOrDefaultAsync();
                var data = new TipCevaplar();
                data.Personel_Tip_Ref = Personel_TipRef;
                data.Tip_Sorular_Ref = TipSoruRef;
                data.GirilenCevap = Cevap;
                //data.AltCevap = AltCevap;
                data.CevapResimX = xkordinat;
                data.CevapResimY = ykordinat;
                data.DogruCevap = await entity.SoruSiklaris.Where(r => r.ID == Cevap).Select(e => e.DogruSecenek).FirstOrDefaultAsync();
                //data.DogruAltCevap = entity.AltSiklars.Where(y => y.ID == AltCevap).Select(r => r.AltDogruSecenek).FirstOrDefault();
                data.KoordinatSecim = koordinatSecim;
                //try
                //{
                //var koordinatlar = Entity.UniteSorulars.Where(t => t.ID == unite_Ref).Select(e => new { e.XKordinat, e.YKordinat }).FirstOrDefault();
                //if (koordinatlar.XKordinat != null)
                //{
                //int xbas = koordinatlar.XBaslangic.Value;
                //int xbit = koordinatlar.XBitis.Value;
                //int ybas = koordinatlar.YBaslangic.Value;
                //int ybit = koordinatlar.YBitis.Value;

                //if ((xbas <= xkordinat.Value && xbit >= xkordinat) && (ybas <= ykordinat && ybit >= ykordinat))
                //{
                //    data.KoordinatSecim = true;
                //}
                //else
                //{
                //    data.KoordinatSecim = false;
                //}
                //    }
                //}
                //catch
                //{
                //}
                var unite = await entity.Tip_Sorular.Where(r => r.ID == TipSoruRef).Select(n => new { n.UniteSorular_Ref }).FirstOrDefaultAsync();
                //int altsik = entity.AltSiklars.Where(t => t.UniteSorular_Ref == unite.UniteSorular_Ref).Count();
                int? resim = await entity.UniteSorulars.Where(t => t.ID == unite.UniteSorular_Ref).Select(u => u.SBagajlar_Ref).FirstOrDefaultAsync();

                bool sonuc = false;
                //if (altsik != 0 && resim != null && unite.SinavTipi_Ref == (int)EnumSinav.SinavTipi.Uygulama)
                //{
                //    if (data.DogruCevap == true && data.DogruAltCevap == true && data.KoordinatSecim == true)
                //    {
                //        sonuc = true;
                //    }
                //    else
                //    {
                //        sonuc = false;
                //    }
                //}

                var secenek = await entity.SoruSiklaris.Where(r => r.ID == Cevap && r.DogruSecenek == true).Select(e => e.SoruSikki).FirstOrDefaultAsync();
                if (secenek == "a)")
                {
                    if (data.DogruCevap == true)
                    {
                        sonuc = true;
                    }
                    else
                    {
                        sonuc = false;
                    }
                }
                else
                {

                    if (data.DogruCevap == true && data.KoordinatSecim == true)
                    {
                        sonuc = true;
                    }
                    else
                    {
                        sonuc = false;
                    }

                }

                data.Netice = sonuc;
                entity.TipCevaplars.Add(data);
                await entity.SaveChangesAsync();

                durum = "Tamam";
                //}

            }
            catch
            {
                durum = "İşlem sırasında hata meydana geldi tekrar deneyiniz!";
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<bool> TipSonuclandirma(int id, int testRef)
        {
            bool durum = false;
            try
            {
                List<TestSonuclari> cevaplar = new List<TestSonuclari>();
                var testSorular = await entity.Tip_Sorular.Where(r => r.TipSinavlar_Ref == testRef && r.IsActive == true && r.IsDelete == false).ToListAsync();
                foreach (var item2 in testSorular)
                {
                    var cevapkontrol = item2.TipCevaplars.Where(r => r.Personel_Tip_Ref == id).Select(t => new { t.Netice, t.Tip_Sorular_Ref }).FirstOrDefault();
                    string cevabi = "";
                    if (cevapkontrol == null)
                    {
                        cevabi = "YOK";
                    }
                    else
                    {
                        if (cevapkontrol.Netice == true)
                        {
                            cevabi = "True";
                        }
                        else
                        {
                            cevabi = "False";
                        }
                    }
                    TestSonuclari datax = new TestSonuclari();
                    datax.PerID = id;
                    datax.SoruID = item2.ID;
                    datax.Cevap = cevabi;
                    cevaplar.Add(datax);
                }

                int DogruSayisi = cevaplar.Where(t => t.Cevap == "True").Count();
                int YanlisSayisi = cevaplar.Where(t => t.Cevap == "False" || t.Cevap == "" || t.Cevap == null).Count();
                //int Bos = cevaplar.Where(t => t.Cevap == "YOK").Count();

                decimal TamPuan = 100;

                int ToplamSoru = testSorular.Count();

                decimal soruPuan = 0;
                if (ToplamSoru != 0)
                {
                    soruPuan = TamPuan / ToplamSoru;
                }

                decimal TestNotu = soruPuan * DogruSayisi;

                string Netice = "";
                if (TestNotu >= 70)
                {
                    Netice = "GEÇTİ";
                }
                else
                {
                    Netice = "KALDI";
                }

                var perTestIsleme = await entity.Personel_TIP.Where(t => t.ID == id).FirstOrDefaultAsync();
                perTestIsleme.DogruSayisi = DogruSayisi;
                perTestIsleme.YanlisSayisi = YanlisSayisi;
                perTestIsleme.TipNotu = TestNotu;
                //perTestIsleme.Bos = Bos;
                //perTestIsleme.Durum = Netice;
                //perTestIsleme.TipAciklama = true;
                perTestIsleme.Netice = Netice;
                await entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return durum;
        }

        public JsonResult LogYaz(int islemTur, int simulatorRef)
        {
            LogDetail dtl = new LogDetail();
            dtl.user_Ref = user.getAutorizeUser().User_Ref;
            dtl.IslemTuru = islemTur;
            dtl.Simulator_Kategori_Ref = simulatorRef;
            log.Logwrite(dtl);
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LogBagaj()
        {
            int userRef = user.getAutorizeUser().User_Ref;
            log.LogBaggage(userRef);
            return Json(null, JsonRequestBehavior.AllowGet);
        }
    }
}
