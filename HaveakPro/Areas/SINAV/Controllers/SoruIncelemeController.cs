﻿
using HaveakPro.Models;
using HaveakPro.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.UI;

namespace HaveakPro.Areas.SINAV.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize]
    public class SoruIncelemeController : Controller
    {
        HaveakEntities Entity;
        I_UserModel user;
        public SoruIncelemeController(HaveakEntities _Entity, I_UserModel _user)
        {
            Entity = _Entity;
            user = _user;
        }

        public ActionResult Soru_Detay(int Sinav_Ref, int idx, int SoruNo)
        {
            var users = user.getAutorizeUser();
            int userRef = 0;
            IQueryable<Sinav_Sorular> icerik = (from d in Entity.Sinav_Sorular
                                                where
                                                    d.Sinavlar_Ref == Sinav_Ref &&
                                                    d.ID == idx
                                                select d);
            Sinav_Sorular data = null;
            if (!users.Roller.Contains(UserRoleName.ADMIN_Egitim) && !users.Roller.Contains(UserRoleName.ADMIN_Sinav))
            {
                data = icerik.Where(d => d.Sinavlar.Personel_Sinavlar.Where(r => r.Sinif_Ogrenci.Personel_Ref == userRef).Select(t => t.Sinavlar_Ref).FirstOrDefault() == Sinav_Ref).FirstOrDefault();
            }
            else
            {
                data = icerik.FirstOrDefault();
                userRef = data.Sinavlar.Personel_Sinavlar.Select(q => q.Sinif_Ogrenci.Personel.ID).FirstOrDefault();
            }

            ViewBag.Soruno = SoruNo;
            ViewBag.PersonelRef = userRef;

            return View(data);
        }

    }
}
