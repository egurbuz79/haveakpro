﻿using System.Web.Mvc;

namespace HaveakPro.Areas.SIMULATOR
{
    public class SIMULATORAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "SIMULATOR";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "SIMULATOR_default",
                "SIMULATOR/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}