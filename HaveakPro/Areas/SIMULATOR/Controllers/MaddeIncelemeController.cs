﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HaveakPro.Models;
using HaveakPro.Models.Interface;
using HaveakPro.Models.Methods;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.UI;

namespace HaveakPro.Areas.SIMULATOR.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize]
    public class MaddeIncelemeController : Controller
    {
        HaveakEntities Entity;
        I_UserModel user;
        I_UserLog log;
        public MaddeIncelemeController(HaveakEntities _Entity, I_UserModel _user, I_UserLog _log)
        {
            Entity = _Entity;
            user = _user;
            log = _log;
        }

        public ActionResult MADDEINCELEME()
        {
            ViewBag.Kategori = Entity.TehlikeliMaddeKategoris.Where(r => r.IsActive == true && r.IsDelete == false).ToList();
            //LogDetail dty = new LogDetail();
            //dty.IslemTuru = 1;
            //dty.Simulator_Kategori_Ref = 6;
            //dty.user_Ref = user.getAutorizeUser().Id;
            //    UserLog.Logwrite(dty);
            return View();
        }

        public JsonResult KategoriMaddeler(int id)
        {
            var data = Entity.Maddelers.OrderBy(s => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false && t.TehlikeliMadde_Ref == id)
                .Select(r => new { r.ID, r.ResimAd, r.Resim, AnaResim = r.TehlikeliMaddeKategori.Resim }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LogYaz(int islemTur, int simulatorRef)
        {
            LogDetail dtl = new LogDetail();
            dtl.user_Ref = user.getAutorizeUser().Id;
            dtl.IslemTuru = islemTur;
            dtl.Simulator_Kategori_Ref = simulatorRef;
            log.Logwrite(dtl);
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LogBagaj()
        {
            int userRef = user.getAutorizeUser().Id;
            log.LogBaggage(userRef);
            return Json(null, JsonRequestBehavior.AllowGet);
        }
    }
}
