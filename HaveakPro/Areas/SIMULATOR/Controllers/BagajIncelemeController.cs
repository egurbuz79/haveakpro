﻿using HaveakPro.Models;
using HaveakPro.Models.Interface;
using HaveakPro.Models.Methods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Web.UI;

namespace HaveakPro.Areas.SIMULATOR.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    //[Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.ADMIN_Sinav)]
    [Authorize]
    public class BagajIncelemeController : Controller
    {
        
        HaveakEntities Entity;
        I_UserModel user;
        I_UserLog log;
        public BagajIncelemeController(HaveakEntities _Entity, I_UserModel _user, I_UserLog _log)
        {
            Entity = _Entity;
            user = _user;
            log = _log;
           // Authorizing.SecilenModul = "SIMULATOR";
        }
           
        public ActionResult HEIMANN()
        {
            //ViewBag.Id = user.getAutorizeUser().Id;
            return View();
        }

        public ActionResult NUCTECH()
        {
            ViewBag.Bagaj = "page-navigation-top";
            ViewBag.Menu = "x-navigation-horizontal";
            ViewBag.SideBar = "";
            return View();
        }

        public ActionResult NUCTECH_ONE()
        {
            return View();
        }

        //public ActionResult RAPISCAN()
        //{

        //    return View();
        //}

        public async Task<JsonResult> NuctechBagajActiveList()
        {
            var data = await Entity.Bagajlars.OrderBy(r => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false).Select(r => new { r.ID, r.Resim }).ToListAsync();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> Nuctech_One_BagajActiveList()
        {
            var data = await Entity.Bagajlars.OrderBy(r => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false).Select(r => new { r.ID, r.Resim }).ToListAsync();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> HiemanBagajActiveList()
        {
            var data = await Entity.HBagajlars.OrderBy(r => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false).Select(r => new { r.ID, r.Resim }).ToListAsync();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> NuctechBagajResimleriGetir(int id)
        {
            var data = await Entity.Bagajlars.OrderBy(r => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false && t.ID == id).Select(r => new { r.ID, r.Resim }).ToListAsync();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> Nuctech_One_BagajResimleriGetir(int id)
        {
            var data = await Entity.Bagajlars.OrderBy(r => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false && t.ID == id).Select(r => new { r.ID, r.Resim }).ToListAsync();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> HiemanBagajResimleriGetir(int id)
        {
            var data = await Entity.HBagajlars.OrderBy(r => Guid.NewGuid()).Where(t => t.IsActive == true && t.IsDelete == false && t.ID == id).Select(r => new { r.ID, r.Resim }).ToListAsync();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LogYaz(int islemTur, int simulatorRef)
        {
            LogDetail dtl = new LogDetail();
            dtl.user_Ref = user.getAutorizeUser().Id;
            dtl.IslemTuru = islemTur;
            dtl.Simulator_Kategori_Ref = simulatorRef;
            log.Logwrite(dtl);
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LogBagaj()
        {         
            int userRef = user.getAutorizeUser().Id;
            log.LogBaggage(userRef);
            return Json(null, JsonRequestBehavior.AllowGet);
        }
    }
}
