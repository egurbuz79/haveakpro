﻿using HaveakPro.Models;
using HaveakPro.Models.Interface;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.UI;

namespace HaveakPro.Areas.EGITIM.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]    
    [Authorize(Roles = UserRoleName.EGITIM_OgrenciGirisi)]
    //[Authorize]
    public class HomeController : Controller
    {
       
        HaveakEntities Entity;
        I_UserModel user;
        public HomeController(HaveakEntities _Entity, I_UserModel _user)
        {
            Entity = _Entity;
            user = _user;
        }
       
        public async Task<ActionResult> Index()
        {
            var users = user.getAutorizeUser();
            ViewBag.Aciklama = await Entity.EgitimGirisSayfasis.FirstOrDefaultAsync();
            DateTime tarih = DateTime.Now.Date;
            var sinif_Ogrenci = await Entity.Sinif_Ogrenci.Where(r => r.Basladi == true && r.Bitti == false && r.Personel_Ref == users.Id && r.BitisTarihi >= tarih).FirstOrDefaultAsync();
            ViewBag.Roller = users.Roller;
            return View(sinif_Ogrenci);
        }
        
        public async Task<JsonResult> OkunduBilgisi(int atananBolum_Ref, int unite_Ref)
        {
            bool durum = false;
            var kontrol = await Entity.OkunanUnitelers.Where(r => r.Unite_Ref == unite_Ref && r.AtananBolumler_Ref == atananBolum_Ref).CountAsync();
            if (kontrol > 0)
            {
                durum = true;
            }
            else
            {
                try
                {
                    OkunanUniteler unt = new OkunanUniteler();
                    unt.Unite_Ref = unite_Ref;
                    unt.AtananBolumler_Ref = atananBolum_Ref;
                    Entity.OkunanUnitelers.Add(unt);
                    await Entity.SaveChangesAsync();
                    durum = true;
                }
                catch
                {
                    durum = false;
                }
            }

            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

    }
}
