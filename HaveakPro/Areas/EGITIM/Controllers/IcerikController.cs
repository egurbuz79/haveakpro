﻿using HaveakPro.Models;
using HaveakPro.Models.Interface;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;

namespace HaveakPro.Areas.EGITIM.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    [Authorize(Roles = UserRoleName.EGITIM_OgrenciGirisi)]
    public class IcerikController : Controller
    {

        HaveakEntities Entity;
        I_UserModel user;
        public IcerikController(HaveakEntities _Entity, I_UserModel _user)
        {
            Entity = _Entity;
            user = _user;
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> BolumBasliklari(int id)
        {            
            int kullaniciID = user.getAutorizeUser().Id;
            var data = await Entity.Unites.OrderBy(t => t.Sira).Where(t => t.Modul_Ref == id && t.IsActive == true && t.IsDelete == false).Select(e => new
            {
                e.ID, e.UniteAdi,
                abRef = e.OkunanUnitelers.Where(r => r.AtananBolumler.Sinif_Ogrenci.Personel_Ref == kullaniciID).Select(q => q.AtananBolumler_Ref).Count(),
                obref = e.OkunanUnitelers.Where(r => r.AtananBolumler.Sinif_Ogrenci.Personel_Ref == kullaniciID).Select(q => q.AtananBolumler_Ref).FirstOrDefault(),
                unite_modul_ref = e.Modul_Ref,
                aktifTestAdet = e.UniteModul.Testlers.Where(r => r.Personel_Testler.Any(t => t.Sinif_Ogrenci.Personel_Ref == kullaniciID)).Count()                
            }).ToListAsync();
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Konu(int id, int? atananblm)
        {
            List<Egitim_Arsivi> Uniteler = await Entity.Egitim_Arsivi.Where(r => r.IsActive == true && r.IsDelete == false && r.Unite_Ref == id).ToListAsync();
            int? modulID = await Entity.Unites.Where(t => t.ID == id && t.IsDelete == false).Select(r => r.Modul_Ref).FirstOrDefaultAsync();
            ViewBag.UniteID = await Entity.Unites.OrderBy(r => r.Sira).Where(t => t.Modul_Ref == modulID.Value && t.IsActive== true && t.IsDelete == false).Select(y => y.ID).ToListAsync();
            //int atanaBolumRef = Entity.KursModullers.Where(e => e.).First();
            ViewBag.AtananBolum = atananblm ?? 0;
            var users = user.getAutorizeUser();
            ViewBag.Roller = users.Roller;
            ViewBag.Id = users.Id;
            return View(Uniteler);
        }

        //public JsonResult AtananBolumGetir(int SinifOgrenci_Ref)
        //{ 
           
        //}
        //public JsonResult Testler(int modulRef)
        //{
        //    var data = Entity.Testlers.Where(t => t.UniteModul_Ref == modulRef && t.IsActive == true && t.IsDelete == false).Select(r => new { r.ID, r.TestNo, r.UniteModul_Ref}).ToList();
        //    return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        //}

    }
}
