﻿using System.Web.Mvc;

namespace HaveakPro.Areas.EGITIM
{
    public class EGITIMAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "EGITIM";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "EGITIM_default",
                "EGITIM/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}