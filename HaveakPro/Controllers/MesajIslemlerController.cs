﻿using HaveakPro.Models;
using HaveakPro.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Web.UI;

namespace HaveakPro.Controllers
{

    [Authorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    public class MesajIslemlerController : Controller
    {
        HaveakEntities Entity;
        I_UserModel user;
        public MesajIslemlerController(HaveakEntities _Entity, I_UserModel _user)
        {
            Entity = _Entity;
            user = _user;
        }
             

        public async Task<ActionResult> Mesajlar()
        {
            var users = user.getAutorizeUser();
            DateTime bugun = DateTime.Now.Date;           
            if (!users.Roller.Contains(UserRoleName.ADMIN_Egitim) && !users.Roller.Contains(UserRoleName.PANEL_AdminGiris))
            {
                ViewBag.MesajKisiler = await Entity.Kull_Rolleri.Where(t => (t.Kull_Rol_Adi.Name == UserRoleName.PANEL_AdminGiris || t.Kull_Rol_Adi.Name == UserRoleName.ADMIN_Egitim)  && t.Kullanici.Personel_Ref != users.Id && t.Kullanici.Personel.IsActive == true).Select(e => e.Kullanici).ToListAsync();
            }
            else
            {
                ViewBag.MesajKisiler = await Entity.Mesajlars.Where(t => t.IsActive == true && t.IsDelete == false && t.Alici_Ref == users.Id && t.Personel.IsActive == true).Select(w => w.Personel.Kullanicis.FirstOrDefault()).Distinct().ToListAsync();
            }
            ViewBag.Roller = users.Roller;
            return View();
        }

        public async Task<ActionResult> _MesajIcerikGnl(int aliciRef)
        {
            var users = user.getAutorizeUser();          
            var data =await Entity.Mesajlars.OrderByDescending(y => y.ID).Where(t => t.IsActive == true && t.IsDelete == false && (t.Gonderen_Ref == users.Id || t.Alici_Ref == users.Id)).ToListAsync();
            ViewBag.AliciRef = aliciRef;
            ViewBag.Kullanici = users.Id;         
            return PartialView("_MesajDetayGenel", data);
        }

        public ActionResult PostaGelen()
        { 
            return View(); 
        }
        public ActionResult PostaGonder()
        {
            return View();
        }
       

        public async Task<JsonResult> UpdateMesaj(int id, bool IsActive, bool IsDelete)
        {
            bool result = false;
            try
            {
                var data =await Entity.Mesajlars.Where(r => r.ID == id).FirstOrDefaultAsync();               
                data.IsActive = IsActive;
                data.IsDelete = IsDelete;
                Entity.SaveChanges();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }            
            return Json(result, JsonRequestBehavior.AllowGet);
        }
       
    }
}
