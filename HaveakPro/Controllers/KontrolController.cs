﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using HaveakPro.Models;
using System.Drawing;
using System.IO;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.Security.Claims;
using HaveakPro.Models.Interface;
using System.Threading.Tasks;
using System.Data.Entity;
using HaveakPro.Models.Methods;
using Microsoft.AspNet.Identity;
using System.Security.Principal;
using System.Web.UI;

namespace HaveakPro.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]   
    public class KontrolController : Controller
    {
        HaveakEntities db;
        I_UserModel user;       
        I_MailHelper mail;
        public KontrolController(HaveakEntities _db, I_UserModel _user, I_MailHelper _mail)
        {
            db = _db;
            user = _user;           
            mail = _mail;
        }
        [AllowAnonymous]
        public ActionResult Giris()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            return View();
        }
        private DateTime bugun = DateTime.Now;
        private int gun = DateTime.Now.Day;
        private int ay = DateTime.Now.Month;
        private int yil = DateTime.Now.Year;

        public async Task<JsonResult> SifreKontrol(string userName, string password)
        {
            int durum = 0;
            DateTime zaman = DateTime.Now.Date;
            Kullanici accountControl = await db.Kullanicis.Where(t => t.IsActive == true && t.KullaniciAdi == userName && t.Sifre == password && t.BitisTarihi >= zaman).FirstOrDefaultAsync();
            if (accountControl != null)            
            {                
                    durum = 2; // Kayıt Doğrulandı               
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Giris(string userName, string password, string Secure)
        {
            try
            {
                if (userName != null)
                {
                    if (Session["Captcha"] == null)
                    {
                        return null;
                    }
                    int total = Convert.ToInt32(Session["Captcha"]);
                    if (Secure == "")
                    {
                        Secure = "0";
                    }
                    if (int.Parse(Secure) == total)
                    {
                        Kullanici accountControl = await db.Kullanicis.Where(t => t.IsActive == true && t.KullaniciAdi == userName && t.Sifre == password).FirstOrDefaultAsync();
                        if (accountControl != null)
                        {
                            List<Claim> claims = new Authorizing(db).GetClaims(accountControl);
                            if (null != claims)
                            {
                                new Authorizing(db).SignIn(claims);
                                return RedirectToAction("Index", "Home", new { area = "" });
                                //KullaniciLoglama(accountControl.ID);
                                //    HttpContext.Session.Add("sifre", password);
                               
                            }
                        }
                    }
                    else
                    {
                       await Logoff();
                    }
                }
            }
            catch
            {
                await Logoff();
            }
            return View();
        }
        //public bool KullaniciLoglama(int kullaniciRef)
        //{           
        //    bool durum = false;
        //    try
        //    {
        //        Loglama logdetay = db.Loglamas.Where(w =>
        //            w.Kullanici_Ref == kullaniciRef &&
        //            w.GirisTarihi.Day == gun &&
        //            w.GirisTarihi.Month == ay &&
        //            w.GirisTarihi.Year == yil).FirstOrDefault();
        //        if (logdetay == null)
        //        {
        //            logdetay = new Loglama();
        //            logdetay.IsActive = true;
        //            logdetay.IsDelete = false;
        //            logdetay.Kullanici_Ref = kullaniciRef;
        //            logdetay.GirisTarihi = bugun;
        //            logdetay.YapilanIs = "Kullanicı Girişi";
        //            db.Loglamas.Add(logdetay);
        //            db.SaveChanges();
        //            durum = true;
        //        }
        //    }
        //    catch
        //    {
        //        durum = false;
        //    }
        //    return durum;
        //}
        public async Task<ActionResult> Logoff()
        {
            int userRef = user.getAutorizeUser().Id;
            Loglama log = await db.Loglamas.Where(w =>
                w.Kullanici_Ref == userRef &&
                w.GirisTarihi.Day == gun &&
                w.GirisTarihi.Month == ay &&
                w.GirisTarihi.Year == yil &&
                w.CikisSaati == null
                ).FirstOrDefaultAsync();
            if (log != null)
            {
                log.CikisSaati = bugun;
                TimeSpan fark = bugun - log.GirisTarihi;
                log.ToplamSure = fark;
                await db.SaveChangesAsync();
            }
            Session.Abandon();
            //FormsAuthentication.SignOut();
            HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            HttpContext.User =new GenericPrincipal(new GenericIdentity(string.Empty), null);
            //DeleteCookie(this, )
            return RedirectToAction("Giris", "Kontrol");
        }

        public async Task<JsonResult> SendMail(string adress)
        {
            bool durum = false;
            try
            {
                List<string> adres = new List<string>();
                adres.Add(adress);
                var data = await db.Personels.Where(t => t.Email == adress).FirstOrDefaultAsync();
                string kisi = data.Adi + " " + data.Soyadi;
                string body = "Sayın <I>" + kisi + "; </I> <br />";
                body += "Proje Kullanıcı adı: <b>[" + data.Kullanicis.Select(t => t.KullaniciAdi).FirstOrDefault() + "] Şifre: [" + data.Kullanicis.Select(t => t.Sifre).FirstOrDefault() + "]</b> dir. <br />";
                body += "Sorun halinde temes kurunuz.";
                durum = mail.SendMail(adres, "Kullanıcı adı,Şifre Bilgisi", body);    
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> ResimGetir(string user)
        {
            ResimData data = await db.Kullanicis.Where(t => t.IsDelete == false && t.IsActive == true && t.KullaniciAdi == user).Select(q => new ResimData { Resim = q.Personel.Resim, Adi = q.Personel.Adi, Soyadi = q.Personel.Soyadi }).FirstOrDefaultAsync();
            if (data == null)
            {
                data = new ResimData();
                data.Adi = "Kullanıcı";
                data.Resim = "RYok.jpg";
                data.Soyadi = "Bulunamadı!";
            }
            if (data.Resim == null)
            {
                data.Resim = "RYok.jpg";
            }
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        //public void DeleteCookie(this Controller controller, string cookieName)
        //{
        //    if (controller.HttpContext.Request.Cookies[cookieName] == null)
        //        return; //cookie doesn't exist

        //    var c = new HttpCookie(cookieName)
        //    {
        //        Expires = DateTime.Now.AddDays(-1)
        //    };
        //    controller.HttpContext.Response.Cookies.Add(c);
        //}


        //public JsonResult ReadData()
        //{
        //    ManagementScope scope = new ManagementScope("\\\\" + Environment.MachineName + "\\root\\cimv2");

        //    scope.Connect();

        //    ManagementObject wmiClass = new ManagementObject(scope, new ManagementPath("Win32_BaseBoard.Tag=\"Base Board\""), new ObjectGetOptions());

        //    foreach (PropertyData propData in wmiClass.Properties)
        //    {
        //        SerialNumber = scope.Path.ToString();
        //    }
        //}
        public ActionResult CaptchaImage(string prefix, bool noisy = true)
        {
            var rand = new Random((int)DateTime.Now.Ticks);
            //yeni soru üret
            int a = rand.Next(10, 99);
            int b = rand.Next(0, 9);
            var captcha = string.Format("{0} + {1} = ?", a, b);

            //cevabı sakla
            Session["Captcha" + prefix] = a + b;

            //resim oluştur
            FileContentResult img = null;

            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(130, 30))
            using (var gfx = Graphics.FromImage((Image)bmp))
            {
                gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.FillRectangle(Brushes.White, new Rectangle(0, 0, bmp.Width, bmp.Height));

                //karmaşa ekle
                if (noisy)
                {
                    int i, r, x, y;
                    var pen = new Pen(Color.Yellow);
                    for (i = 1; i < 10; i++)
                    {
                        pen.Color = Color.FromArgb(
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)));

                        r = rand.Next(0, (130 / 3));
                        x = rand.Next(0, 130);
                        y = rand.Next(0, 30);

                        gfx.DrawEllipse(pen, x - r, y - r, r, r);
                    }
                }

                //soruyu ekle
                gfx.DrawString(captcha, new Font("Tahoma", 15), Brushes.Gray, 2, 3);

                //resim olarak çiz
                bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
                img = this.File(mem.GetBuffer(), "image/Jpeg");
            }

            return img;
        }       
    }
    public class ResimData : IDisposable
    {
        public string Resim { get; set; }
        public string Adi { get; set; }
        public string Soyadi { get; set; }


        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}