﻿using HaveakPro.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Data.Entity;
using System.Web.UI;

namespace HaveakPro.Controllers
{
    [Authorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    public class DuyurularController : Controller
    {    
        HaveakEntities Entity;
        public DuyurularController(HaveakEntities _Entity)
        {
            Entity = _Entity;           
        }
        
        public async Task<ActionResult> Index()
        {           
            IQueryable<Duyurular> duyurular = Entity.Duyurulars;
            List<Duyurular> data = data = await duyurular.Where(t => t.IsDelete == false && t.IsActive == true).ToListAsync();  
            return View(data);
        }
        
    }
}
