﻿using HaveakPro.Models;
using HaveakPro.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Data.Entity;
using System.Web.UI;

namespace HaveakPro.Controllers
{
    //[Authorize(Roles = UserRoleName.PANEL_AdminGiris + "," + UserRoleName.SINAVLAR_OgrenciGirisi + "," + UserRoleName.EGITIM_OgrenciGirisi + "," + UserRoleName.GUVENLIK_OgrenciGirisi + "," + UserRoleName.ADMIN_BagajTehlikelimadde + "," + UserRoleName.ADMIN_Sinav + "," + UserRoleName.ADMIN_PersonelKullanici + "," + UserRoleName.ADMIN_BagajTehlikelimadde + "," + UserRoleName.ADMIN_Egitim)]
    [Authorize]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*", Location = OutputCacheLocation.None)]
    public class HomeController : Controller
    {

        HaveakEntities Entity;
        I_UserModel user;
        public HomeController(HaveakEntities _Entity, I_UserModel _user)
        {
            Entity = _Entity;
            user = _user;
        }

        public async Task<ActionResult> Index()
        {
            try
            {
                var users = user.getAutorizeUser();
                ViewBag.Roller = users.Roller;
                ViewBag.MesajIcerik = await Entity.Mesajlars.Where(t => t.Alici_Ref == users.Id && t.IsActive == true && t.IsDelete == false).ToListAsync();
                TimeSpan zaman = DateTime.Now.TimeOfDay;
                DateTime bugun = DateTime.Now.Date;
                ViewBag.AdminDetay = await Entity.Kullanicis.Where(t => t.Kull_Rolleri.Any(m => m.User_Role_Name_Ref == 1)).ToListAsync();
                ViewBag.MesajBaslik = await (from d in Entity.Mesajlars group d by new { d.Personel.ID, d.Personel.Adi, d.Personel.Soyadi } into grp select new ProjectMessage { ID = grp.Key.ID, Ad = grp.Key.Adi, Soyad = grp.Key.Soyadi }).ToListAsync();
                IQueryable<Duyurular> qduyuru = Entity.Duyurulars;
                List<Duyurular> duyuruData = await qduyuru.Where(t => t.IsDelete == false && t.IsActive == true).ToListAsync();
                ViewBag.Duyurular = duyuruData;
                ViewBag.Sinavlar = await Entity.Personel_Sinavlar.Where(t => t.IsActive == true && t.IsDelete == false && t.Sinif_Ogrenci.Personel_Ref == users.Id && t.SinavTarihi.Value >= bugun && t.BitisSaati >= zaman).ToListAsync();
                ViewBag.SinavAciklama = await Entity.Personel_Sinavlar.Where(t => t.IsActive == true && t.IsDelete == false && t.Sinif_Ogrenci.Personel_Ref == users.Id && t.DuyuruTarihi.Value == bugun).ToListAsync();
                if (!users.Roller.Contains(UserRoleName.ADMIN_Egitim))
                {
                    ViewBag.Adminler = await Entity.Kull_Rolleri.Where(t => t.Kull_Rol_Adi.Name == UserRoleName.ADMIN_Egitim && t.Kullanici.Personel_Ref != users.Id).Select(e => e.Kullanici).ToListAsync();
                }
                else
                {
                    ViewBag.Adminler = await Entity.Mesajlars.Where(t => t.IsActive == true && t.IsDelete == false && t.Alici_Ref == users.Id && t.Tarih == bugun).Select(w => w.Personel.Kullanicis.FirstOrDefault()).Distinct().ToListAsync();
                }
            }
            catch
            {
                RedirectToAction("Logoff", "Kontrol", new { area = "" });
            }
            return View();
        }

        public async Task<JsonResult> VitrinMesajlar()
        {
            List<ProjectMessage> data = new List<ProjectMessage>();
            try
            {
                var userId = user.getAutorizeUser().Id;
                DateTime bugun = DateTime.Now.Date;
                data = await Entity.Mesajlars.OrderByDescending(y => y.ID).Where(t => t.Alici_Ref == userId && t.Tarih == bugun).Select(t => new ProjectMessage
                {
                    Ad = t.Personel.Adi,
                    Soyad = t.Personel.Soyadi,
                    Resim = (t.Personel.Resim ?? "RYok.jpg"),
                    Icerik = t.Icerik,
                    Gonderen_Ref = t.Gonderen_Ref
                }).Take(1).ToListAsync();
            }
            catch
            {
                RedirectToAction("Logoff", "Account", new { area = "" });
            }
            return Json(data, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> Mesajlar()
        {
            var kimlik = user.getAutorizeUser().Id;
            if (kimlik != 0)
            {
                var userRole = user.getAutorizeUser().Roller;
                if (userRole.Contains("PANEL/AdminGirisi"))
                {
                    ViewBag.MesajIcerik = await Entity.Mesajlars.Where(t => t.IsActive == true && t.IsDelete == false).ToListAsync();
                }
                else
                {
                    ViewBag.MesajIcerik = await Entity.Mesajlars.Where(r => r.Gonderen_Ref == kimlik && r.IsActive == true && r.IsDelete == false).ToListAsync();
                }
                ViewBag.AdminDetay = await Entity.Kullanicis.Where(t => t.Kull_Rolleri.Any(m => m.User_Role_Name_Ref == 1)).ToListAsync();
                ViewBag.MesajBaslik = await (from d in Entity.Mesajlars group d by new { d.Personel.ID, d.Personel.Adi, d.Personel.Soyadi } into grp select new ProjectMessage { ID = grp.Key.ID, Ad = grp.Key.Adi, Soyad = grp.Key.Soyadi }).ToListAsync();
            }
            var data2 = await Entity.Mesajlars.Where(t => t.IsActive == true && t.IsDelete == false).Select(y =>
                               new { y.ID, y.Alici_Ref, y.Icerik, y.Personel.Adi, y.Personel.Resim, y.Personel.Soyadi, y.Tarih }).ToListAsync();

            return Json(data2, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> _MesajIcerikVt(int aliciRef)
        {
            int gonderen = user.getAutorizeUser().Id;
            DateTime bugun = DateTime.Now.Date;
            var data = await Entity.Mesajlars.OrderByDescending(y => y.ID).Where(t => t.IsActive == true && t.IsDelete == false && (t.Gonderen_Ref == gonderen || t.Alici_Ref == gonderen) && t.Tarih == bugun).ToListAsync();
            ViewBag.AliciRef = aliciRef;
            ViewBag.userId = user.getAutorizeUser().Id;
            return PartialView("_MesajDetay", data);
        }

        public async Task<JsonResult> MesajGonder(int aliciRef, string mesaj)
        {
            bool durum = false;
            try
            {
                int gonderenRef = user.getAutorizeUser().Id;
                DateTime tarih = DateTime.Now.Date;
                TimeSpan saat = DateTime.Now.TimeOfDay;

                Mesajlar msj = new Mesajlar();
                msj.IsActive = true;
                msj.IsDelete = false;
                msj.Gonderen_Ref = gonderenRef;
                msj.Alici_Ref = aliciRef;
                msj.Tarih = tarih;
                msj.Icerik = mesaj;
                msj.Saat = saat;
                Entity.Mesajlars.Add(msj);
                await Entity.SaveChangesAsync();
                durum = true;
            }
            catch
            {
                durum = false;
            }
            return Json(durum, "application/json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }

    }


    public class ProjectMessage
    {
        public int ID { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Resim { get; set; }
        public string Icerik { get; set; }
        public int Gonderen_Ref { get; set; }
    }
}
