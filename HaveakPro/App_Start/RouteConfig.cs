﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HaveakPro
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               "Default",
               "{controller}/{action}/{id}",
               new { controller = "Kontrol", action = "Giris", id = UrlParameter.Optional },
               new[] { "HaveakPro.Controllers" }
            );
        }
    }
}
