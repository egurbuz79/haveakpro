using HaveakPro.Models.Interface;
using HaveakPro.Models.Methods;
using System;

using Unity;

namespace HaveakPro
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

     
        public static IUnityContainer Container => container.Value;
        #endregion

      
        public static void RegisterTypes(IUnityContainer container)
        {                 
            container.RegisterType<I_MailHelper, MailHelper>();
            container.RegisterType<I_UserModel, UserModel>();
            container.RegisterType<I_UserLog, UserLog>();
        }
    }
}