﻿using HaveakPro.Models;
using HaveakPro.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HaveakPro
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            Application["OnlineZiyaretci"] = 0;
        }

        protected void Application_End(object sender, EventArgs e)
        {
            var _hlp = DependencyResolver.Current.GetService<I_UserModel>();
            Application.Remove("OnlineZiyaretci");
            var sifre = HttpContext.Current.Session["sifre"];
            int? accountId = _hlp.getAutorizeUser().User_Ref;
            if (accountId != null)
            {
                using (HaveakEntities db = new HaveakEntities())
                {
                    var userq = db.Kullanicis.Where(t => t.ID == accountId).FirstOrDefault();
                    userq.Sistemde = false;
                    db.SaveChanges();
                }
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Application.Lock();
            Application["OnlineZiyaretci"] = (int)Application["OnlineZiyaretci"] + 1;
            Application.UnLock();
        }

        protected void Session_End(object sender, EventArgs e)
        {
            Application.Lock();
            Application["OnlineZiyaretci"] = (int)Application["OnlineZiyaretci"] - 1;          
            Application.UnLock();         
        }
    }
}
