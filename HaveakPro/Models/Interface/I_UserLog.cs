﻿
using HaveakPro.Models.Methods;


namespace HaveakPro.Models.Interface
{
    public interface I_UserLog
    {
        void Logwrite(LogDetail detail);
        void LogBaggage(int userRef);
    }
}
