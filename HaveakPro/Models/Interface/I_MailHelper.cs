﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HaveakPro.Models.Interface
{
    public interface I_MailHelper
    {
        bool SendMail(List<string> Address, string subject, string body);
    }
}
