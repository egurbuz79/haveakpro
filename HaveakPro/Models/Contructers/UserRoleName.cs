﻿

namespace HaveakPro.Models
{
    public static class UserRoleName
    {
        public const string PANEL_AdminGiris = "PANEL/AdminGirisi";
        public const string GUVENLIK_OgrenciGirisi = "GUVENLIK/OgrenciGirisi";
        public const string EGITIM_OgrenciGirisi = "EGITIM/OgrenciGirisi";
        public const string SINAVLAR_OgrenciGirisi = "SINAVLAR/OgrenciGirisi";
        public const string ADMIN_BagajTehlikelimadde = "ADMIN/BagajTehlikelimadde";
        public const string ADMIN_Egitim = "ADMIN/Egitim";
        public const string ADMIN_Sinav = "ADMIN/Sinav";
        public const string ADMIN_PersonelKullanici = "ADMIN/PersonelKullanici";
        public static string SecilenModul { get; set; }
    }

}