﻿using HaveakPro.Models.Interface;
using System;
using System.Linq;


namespace HaveakPro.Models.Methods
{
    public class UserLog:I_UserLog
    {
        public void Logwrite(LogDetail detail)
        {
            using (HaveakEntities db = new HaveakEntities())
            {
                int ogrenci_ref = db.Sinif_Ogrenci.OrderByDescending(r=>r.ID).OrderByDescending(e=>e.ID).Where(w => w.Personel_Ref == detail.user_Ref && w.Bitti == false).Select(q => q.ID).FirstOrDefault();
                SimulatorLog logIcerik = db.SimulatorLogs.OrderByDescending(r=>r.ID).Where(r => r.Sinif_Ogrenci_Ref == ogrenci_ref && r.Bitti == null).FirstOrDefault();              
                if (logIcerik == null && detail.IslemTuru == Convert.ToInt32(IslemTuru.Giris_Islemi))
                {
                    SimulatorLog logdata = new SimulatorLog();
                    logdata.Sinif_Ogrenci_Ref = ogrenci_ref;
                    logdata.Simulator_Kategori_Ref = detail.Simulator_Kategori_Ref;
                    logdata.GirisZamani = DateTime.Now;                    
                    logdata.Basladi = Convert.ToInt32(IslemTuru.Giris_Islemi);
                    db.SimulatorLogs.Add(logdata);
                    db.SaveChanges();
                }
                else if (logIcerik != null && detail.IslemTuru == Convert.ToInt32(IslemTuru.Cikis_Islemi))
                {
                    logIcerik.CikisZamani = DateTime.Now;
                    logIcerik.Bitti = Convert.ToInt32(IslemTuru.Cikis_Islemi);
                    db.SaveChanges();
                }                
                else
                {
                    try
                    {
                        logIcerik.CikisZamani = logIcerik.GirisZamani.AddMinutes(1);
                        logIcerik.Bitti = Convert.ToInt32(IslemTuru.Cikis_Islemi);
                        db.SaveChanges();
                    }
                    catch 
                    {                        
                    }                 
                  
                    SimulatorLog logdatayeni = new SimulatorLog();
                    logdatayeni.Sinif_Ogrenci_Ref = ogrenci_ref;
                    logdatayeni.GirisZamani = DateTime.Now;
                    logdatayeni.Basladi = Convert.ToInt32(IslemTuru.Giris_Islemi);
                    logdatayeni.Simulator_Kategori_Ref = detail.Simulator_Kategori_Ref;
                    db.SimulatorLogs.Add(logdatayeni);
                    db.SaveChanges();
                }
               
            }
        }
                
        public void LogBaggage(int userRef)
        {
            using (HaveakEntities db = new HaveakEntities())
            {
                int ogrenci_ref = db.Sinif_Ogrenci.OrderByDescending(e=>e.ID).Where(w => w.Personel_Ref == userRef && w.Bitti == false).Select(q => q.ID).FirstOrDefault();
                SimulatorLog logIcerik = db.SimulatorLogs.OrderByDescending(r=>r.ID).Where(r => r.Sinif_Ogrenci_Ref == ogrenci_ref && r.Bitti == null).FirstOrDefault();
                int bagajAdet = 0;
                if (logIcerik != null)
                {
                    bagajAdet = logIcerik.IncelenenBagaj == null ? 0 : logIcerik.IncelenenBagaj.Value;
                    int yenideger = bagajAdet + 1;
                    logIcerik.IncelenenBagaj = yenideger;
                    db.SaveChanges();
                }                  
            }
        }
               
    }

    public class LogDetail 
    {
        public int user_Ref { get; set; }
        public int Simulator_Kategori_Ref { get; set; }
        public int IslemTuru { get; set; }        
    }

    public enum IslemTuru : int
    {
        Giris_Islemi = 1,
        Cikis_Islemi = 2
    }
}