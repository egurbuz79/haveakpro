﻿using HaveakPro.Models.Interface;
using System.Collections.Generic;
using System.Net.Mail;

namespace HaveakPro.Models.Methods
{
    public class MailHelper : I_MailHelper
    {
        public bool SendMail(List<string> Address, string subject, string body)
        {
            bool save = false;
            try
            {
                MailMessage mail = new MailMessage();
                foreach (var itemFrom in Address)
                {
                    mail.To.Add(itemFrom);
                }
                mail.From = new MailAddress("info@haveak.com", "Havacılık Eğitim Kurumu");
                mail.Subject = subject;
                string Body = body;
                mail.Body = Body;
                mail.Priority = MailPriority.High;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "mail.haveak.com";
                smtp.EnableSsl = false;
                smtp.Credentials = new System.Net.NetworkCredential("info@haveak.com", "Eg2014");
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Port = 587;
                smtp.Send(mail);
                save = true;
            }
            catch
            {
                save = false;
            }
            return save;
        }
    }
}