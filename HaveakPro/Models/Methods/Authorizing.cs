﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using HaveakPro.Models;
using HaveakPro.Models.Interface;

namespace HaveakPro.Models.Methods
{
    public class Authorizing
    {
        private HaveakEntities db = null;
        public Authorizing(HaveakEntities _db)
        {
            db = _db;
        }

        public List<Claim> GetClaims(Kullanici data)
        {

            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.NameIdentifier, data.KullaniciAdi));
            claims.Add(new Claim(ClaimTypes.Sid, data.ID.ToString()));
            List<string> roller = db.Kull_Rolleri.Where(e => e.User_Ref == data.ID).Select(t => t.Kull_Rol_Adi.Name).ToList();
            //List<string> modeller = db.Modullers.Where(t => t.IsActive && !t.IsDelete).Select(r => r.Name).ToList();
            claims.Add(new Claim(ClaimTypes.PrimarySid, data.Personel.ID.ToString()));
            claims.Add(new Claim(ClaimTypes.Name, data.Personel.Adi));
            claims.Add(new Claim(ClaimTypes.Surname, data.Personel.Soyadi));
            claims.Add(new Claim(ClaimTypes.MobilePhone, data.Personel.CepTelefon ?? ""));
            claims.Add(new Claim(ClaimTypes.Email, data.Personel.Email ?? ""));
            claims.Add(new Claim(ClaimTypes.Expiration, data.Personel.Kullanicis.OrderByDescending(r => r.ID).Select(r => r.BaslangicTarihi).FirstOrDefault().ToString() ?? ""));
            claims.Add(new Claim(ClaimTypes.Expired, data.Personel.Kullanicis.OrderByDescending(r => r.ID).Select(r => r.BitisTarihi).FirstOrDefault().ToString() ?? ""));



            string resim = string.IsNullOrEmpty(data.Personel.Resim) == true ? "/DocumentFiles/Personel/RYok.jpg" : data.Personel.Resim;
            claims.Add(new Claim(ClaimTypes.Upn, resim));
            foreach (var rol in roller)
            {
                claims.Add(new Claim(ClaimTypes.Role, rol));
            }


            return claims;
        }
        public void SignIn(List<Claim> claims)
        {
            var claimsIdentity = new ClaimsIdentity(claims,
            DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, claimsIdentity);
            HttpContext.Current.User = new ClaimsPrincipal(AuthenticationManager.AuthenticationResponseGrant.Principal);
        }

        public IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication;
            }
        }
        public static string SecilenModul { get; set; }

    }
}