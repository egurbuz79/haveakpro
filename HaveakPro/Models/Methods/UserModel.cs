﻿using HaveakPro.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace HaveakPro.Models.Methods
{
    public class UserModel:I_UserModel
    {
        public int Id { get; set; }
        public string EName { get; set; }
        public string ESurName { get; set; }
        public string UserName { get; set; }
        public string EMail { get; set; }
        public string Telephone { get; set; }
        public string Picture { get; set; }
        public bool? Notification { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int UserType { get; set; }
        public int User_Ref { get; set; }
        public string SecilenModul { get; set; }
        public List<string> Roller { get; set; }
        public List<string> Moduller { get; set; }


        public UserModel getAutorizeUser()
        {

            var identity = (ClaimsIdentity)HttpContext.Current.User.Identity;
            UserModel user = new UserModel();
            if (identity.IsAuthenticated)
            {              
                user.User_Ref = Convert.ToInt32(identity.Claims.Where(r => r.Type == ClaimTypes.Sid).FirstOrDefault().Value);
                user.UserName = identity.FindFirst(ClaimTypes.NameIdentifier).Value;
                user.Id = Convert.ToInt32(identity.FindFirst(ClaimTypes.PrimarySid).Value);
                user.EName = identity.FindFirst(ClaimTypes.Name).Value;
                user.ESurName = identity.FindFirst(ClaimTypes.Surname).Value;
                user.Roller = identity.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).ToList();
                user.Picture = identity.FindFirst(ClaimTypes.Upn).Value;
                user.EMail = identity.FindFirst(ClaimTypes.Email).Value;
                user.Telephone = identity.FindFirst(ClaimTypes.MobilePhone).Value;
                user.StartDate = Convert.ToDateTime(identity.FindFirst(ClaimTypes.Expiration).Value);
                user.EndDate = Convert.ToDateTime(identity.FindFirst(ClaimTypes.Expired).Value);
            }
            return user;
        }
    }
}