//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HaveakPro.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SBagajlar
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SBagajlar()
        {
            this.UniteSorulars = new HashSet<UniteSorular>();
        }
    
        public int ID { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public string Resim { get; set; }
        public string Isim { get; set; }
        public int SKategori_Ref { get; set; }
    
        public virtual SBagajKategori SBagajKategori { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UniteSorular> UniteSorulars { get; set; }
    }
}
